'use strict';
angular.module('qeApp')
    .controller('StationCategoryFormController', stationCategory);

stationCategory.$inject = ['StationCategory', 'notify', '$stateParams', '$location'];

function stationCategory(StationCategory, notify, $stateParams, $location) {
    var vm = this;
    vm.stationCategory = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';

    if ($stateParams.id) {
        vm.stationCategory = StationCategory.findById({
            id: $stateParams.id
        });
    }
    vm.save = function() {
        vm.saving = true;
        var saveSuccess = function() {
            vm.saving = false;
            notify.success('Station Category saved!');
        };
        if (!vm.stationCategory.id) {
            StationCategory.create(vm.stationCategory, function() {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.stationCategory.$save(function() {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
