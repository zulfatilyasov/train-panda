'use strict';
angular.module('qeApp')
    .controller('StationsController', stations);

stations.$inject = ['Station', 'datatables', '$timeout', 'csvParser', '$scope', '$rootScope'];

function stations(Station, dataTables, $timeout, csvParser, $scope, $rootScope) {
    var vm = this;
    vm.uploading = false;

    $rootScope.loading = true;
    vm.stations = Station.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.stations-datatable');
        });
    });

    $scope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $scope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    csvParser.attachInputWatcher('.upload-select', Station);

    vm.delete = function (customerId) {
        Station.deleteById({
            id: customerId
        }).$promise
            .then(function () {
                dataTables.removeRow(customerId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}
