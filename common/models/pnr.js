'use strict';
var q = require('q');
module.exports = function (PNR) {
    PNR.getByCustomerId = function (customerId, cb) {
        var ObjectID = PNR.app.datasources.mongodb.ObjectID;
        var id = ObjectID.createFromHexString(customerId);
        var collection = PNR.getDataSource().connector.collection("PNR");
        collection.find({'customer.id': id}).toArray(function (err, pnrs) {
            if (err) {
                cb(err);
            }
            else {
                cb(null, pnrs);
            }
        });
    };

    PNR.remoteMethod('getByCustomerId',
        {
            accepts: {arg: 'id', type: 'string'},
            returns: {arg: 'pnrs', type: 'array'},
            http: {path: '/getByCustomerId', verb: 'get'}
        });

    PNR.insertMany = function (data, cb) {
        var Customer = PNR.app.models.Customer;
        var Station = PNR.app.models.Station;

        var promises = [];

        for (var i = 0, len = data.length; i < len; i++) {
            var pnr = data[i];

            if (!pnr.startDate) {
                pnr.startDate = null;
            }

            if (!pnr.endDate) {
                pnr.endDate = null;
            }

            var createPnrFn = getCreatePnrFnc(pnr);

            var p = q.all([
                setPNRCustomerReference(pnr),
                setPNRStartStation(pnr),
                setPNREndStation(pnr)
            ]).then(createPnrFn);

            promises.push(p);
        }

        function getCreatePnrFnc(pnr) {
            return function () {
                var def = q.defer();

                PNR.create(pnr, function (err, pnr) {
                    if (err) {
                        def.reject(err);
                    }
                    else {

                        def.resolve(pnr);
                    }
                });
                return def.promise;
            };
        }

        q.all(promises)
            .then(function () {
                cb(null);
            })
            .catch(function (err) {
                cb(err, null);
            });

        function setPNRCustomerReference(pnr) {
            var def = q.defer();

            if (pnr.customerCode) {
                Customer.findOne({
                    where: {
                        code: pnr.customerCode
                    }
                }, setCustomer);
            } else {
                def.resolve();
            }

            function setCustomer(err, customer) {
                if (err) {
                    def.reject(err);
                }

                if (!customer) {
                    def.reject("PNR must have customer");
                    return;
                }

                pnr.customer = {
                    id: customer.id,
                    name: customer.name
                };
                def.resolve();
            }

            return def.promise;
        }

        function setPNRStartStation(pnr) {
            var def = q.defer();

            if (pnr.startStationCode) {
                Station.findOne({
                    where: {
                        code: pnr.startStationCode
                    }
                }, setStartStation);
            } else {
                def.resolve();
            }

            function setStartStation(err, station) {
                if (err) {
                    def.reject(err);
                }

                if (!station) {
                    def.reject("PNR must have start station");
                    return;
                }

                pnr.startingStation = {
                    id: station.id,
                    name: station.name,
                    code: station.code
                };
                def.resolve();
            }

            return def.promise;
        }

        function setPNREndStation(pnr) {
            var def = q.defer();

            if (pnr.endStationCode) {
                Station.findOne({
                    where: {
                        code: pnr.endStationCode
                    }
                }, setEndStation);
            } else {
                def.resolve();
            }

            function setEndStation(err, station) {
                if (err) {
                    def.reject(err);
                }

                if (!station) {
                    def.reject("PNR must have end station");
                    return;
                }

                pnr.endStation = {
                    id: station.id,
                    name: station.name,
                    code: station.code
                };
                def.resolve();
            }

            return def.promise;
        }
    };

    PNR.remoteMethod('insertMany',
        {
            accepts: {arg: 'data', type: 'array'}
        });
};
