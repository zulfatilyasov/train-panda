"use strict";

module.exports = function (TrainRating) {
    TrainRating.observe('after save', function (ctx, next) {
        var Train = TrainRating.app.models.Train;
        var rate = ctx.instance;
        if (rate) {
            Train.findById(rate.trainId, function (err, train) {
                if (!train.ratesCount) {
                    train.ratesCount = 0;
                }
                var getAvg = function (field) {
                    if (!train[field]) {
                        train[field] = 0;
                    }
                    return (train[field] * train.ratesCount + rate[field]) / (train.ratesCount + 1);
                };
                if (train) {
                    if(rate.foodQuality){
                        train.foodQuality = getAvg('foodQuality');
                    }
                    if(rate.clean){
                        train.clean = getAvg('clean');
                    }
                    if(rate.crowded){
                        train.crowded = getAvg('crowded');
                    }
                    if(rate.delay){
                        train.delay = getAvg('delay');
                    }
                    train.ratesCount++;
                    train.save(function (err) {
                        if (err) {
                            next(err);
                        }
                        else {
                            next();
                        }
                    });
                } else {
                    next();
                }
            });

        }
    });
};
