'use strict';
angular.module('qeApp')
  .controller('RegisterFormController', register);

register.$inject = ['$state', 'Users'];

function register($state, Users) {
  var vm = this;

  // bind here all data from the form
  vm.account = {};
  // place the message if something goes wrong
  vm.authMsg = '';

  var logInUser = function(user) {
    Users.login({
      email: user.email,
      password: user.password2 || user.passowrd
    }, loginSucess, loginFail);
  };

  var loginFail = function(resp) {
    console.log(resp);
    vm.authMsg = 'Authentication failed';
  };

  var loginSucess = function(accessToken) {
    console.log(accessToken);
    if (accessToken.id) {
      $state.go('app.dashboard');
    }
  };

  var createSuccess = function(user) {
    if (user.id) {
      logInUser(user);
    }
  };

  var createFail = function(response) {
    console.log(response);
    vm.authMsg = 'Registration failed';
  };

  vm.register = function() {
    vm.authMsg = '';
    vm.account.username = vm.account.email;
    //Users.create(vm.account, createSuccess, createFail);
  };

}
