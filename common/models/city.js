'use strict';
module.exports = function (City) {

    City.findByStateId = function (stateId, cb) {
        var ObjectID = City.app.datasources.mongodb.ObjectID;
        City.find({
            where: {
                'state.id': ObjectID.createFromHexString(stateId)
            }
        }, function (err, cities) {
            if (err) {
                throw err;
            }

            cb(null, cities);
        });
        City.findOne();
    };

    City.remoteMethod(
        'findByStateId',
        {
            accepts: {arg: 'stateId', type: 'string'},
            returns: {arg: 'cities', type: 'array'}
        }
    );

    City.observe('after save', function (ctx, next) {
        if (ctx.instance) {
            var Customer = City.app.models.Customer;
            var Station = City.app.models.Station;

            Customer.find({
                where: {
                    'city.id': ctx.instance.id
                }
            }, function (err, customers) {
                updateCustomersCity(customers, ctx.instance);
            });
        }
        next();
    });

    function updateCustomersCity(customers, city) {
        for (var i = customers.length - 1; i >= 0; i--) {
            customers[i].updateAttribute('state', {
                id: city.id,
                name: city.name
            });
        }
    }

    City.insertMany = function (data, cb) {
        var State = City.app.models.State;
        var createdCount = 0;

        for (var i = 0, len = data.length; i < len; i++) {
            var city = data[i];
            city.isUrban = city.isUrban.toLowerCase() === 'true';
            if (city.stateCode) {
                setCityStateReference(city);
            }
            else {
                createCity(city);
            }
        }

        function setCityStateReference(city) {
            State.findOne({
                    where: {
                        code: city.stateCode
                    }
                }, function (err, state) {
                    if (err || !state) {
                        cb(err, null);
                    }

                    if (state) {
                        city.state = {
                            id: state.id,
                            name: state.name
                        };
                        createCity(city);
                    }
                }
            );
        }

        function createCity(city) {
            City.create(city, function (err) {
                if (err){
                    cb(err, null);
                }

                createdCount++;
                if (createdCount === data.length) {
                    cb(null);
                }
            });
        }
    };

    City.remoteMethod('insertMany',
        {
            accepts: {arg: 'data', type: 'array'}
        });

};
