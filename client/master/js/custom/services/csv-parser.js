'use strict';
angular.module('qeApp')
    .factory('csvParser', csvParser);

csvParser.$inject = ['notify', 'datatables', '$state', '$rootScope'];

function csvParser(notify, dataTables, $state, $rootScope) {
    function attachInputWatcher(inputSelector, Model) {
        $(inputSelector).on("change", function () {
            var file = $(this)[0].files[0];
            if (file)
                onFileSelected(file);
        });

        function onFileSelected(file) {
            $rootScope.$broadcast('uploadStart');
            Papa.parse(file, {
                header: true,
                complete: parseCompleted,
                error: function (err, file, inputElem, reason) {
                    notify.error("File couldn't be parsed. Only csv files accepted");
                    console.log(err + '\n' + reason);
                }
            });
        }

        function parseCompleted(results) {
            if (results.errors && results.errors.length) {
                notify.error("File couldn't be parsed. Only csv files accepted");
                return;
            }

            if (!results.data)
                throw new Error('no data in parsed results');

            Model.insertMany({data: results.data})
                .$promise
                .then(function () {
                    dataTables.destroy();
                    $state.go($state.current, {}, {reload: true});
                }, function (err) {
                    if (err.data && err.data.error && err.data.error.message) {
                        notify.error(err.data.error.message);
                    }
                    else {
                        notify.error("Data import failed. Please check you csv file format is valid");
                    }
                })
                .finally(function () {
                    $rootScope.$broadcast('uploadFinish');
                });
        }
    }

    return {
        attachInputWatcher: attachInputWatcher
    }
}
