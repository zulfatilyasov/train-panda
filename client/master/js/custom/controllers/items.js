'use strict';
angular.module('qeApp')
    .controller('ItemsController', items);

items.$inject = ['Item', 'datatables', '$timeout', '$state', '$stateParams', 'csvParser', '$rootScope'];

function items(Item, dataTables, $timeout, $state, $statePrams, csvParser,  $rootScope) {
    var vm = this;

    var filter = {
        'filter[include]': ['category', 'subcategory', 'restaurant']
    };

    if($statePrams.restaurantId) {
        filter['filter[where][restaurantId]'] = $statePrams.restaurantId;
    }

    $rootScope.loading = true;
    Item.query(filter, function (items) {
        vm.items = items;
        $timeout(function () {
            dataTables.initDataTable('.items-datatable');
        })
    });

    csvParser.attachInputWatcher('.upload-select', Item);

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    vm.delete = function (itemId) {
        Item.deleteById({
            id: itemId
        }).$promise
            .then(function () {
                dataTables.removeRow(itemId);
            });
    };

    vm.showAlarms = function (itemId) {
        $state.go('app.item-alarms', {itemId: itemId});
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}
