'use strict';
module.exports = function (Customer) {

    function getNextSequence(name, callback) {
        var connector = Customer.getDataSource().connector;
        var codes = connector.collection("Codes");
        codes.findAndModify({_id: name}, null, {$inc: {seq: 1}}, {new: true}, callback);
    }

    Customer.observe('before save', function (ctx, next) {
            if (ctx.instance && !ctx.instance.code) {
                getNextSequence('customers', function (err, resp) {
                    ctx.instance.code = resp.value.seq;
                    next();
                });
            } else if (!ctx.data.code) {
                getNextSequence('customers', function (err, resp) {
                    ctx.data.code = resp.value.seq;
                    next();
                });
            }
            else {
                next();
            }
        }
    );

    Customer.insertMany = function (data, cb) {
        var State = Customer.app.models.State;
        var City = Customer.app.models.City;

        var createdCount = 0;

        for (var i = 0, len = data.length; i < len; i++) {
            var customer = data[i];
            if (customer.stateCode) {
                setCustomerStateReference(customer);
            }
            else if (customer.cityCode) {
                setCustomerCityReference(customer);
            }
            else {
                createCustomer(customer);
            }
        }

        function setCustomerStateReference(customer) {
            State.findOne({
                    where: {
                        code: customer.stateCode
                    }
                }, function (err, state) {
                    if (err || !state) {
                        cb(err, null);
                    }

                    if (state) {
                        customer.state = {
                            id: state.id,
                            name: state.name
                        };
                    }

                    if (customer.cityCode) {
                        setCustomerCityReference(customer);
                    }
                    else {
                        createCustomer(customer);
                    }
                }
            );
        }

        function setCustomerCityReference(customer) {
            City.findOne({
                    where: {
                        code: customer.cityCode
                    }
                }, function (err, city) {
                    if (err || !city) {
                        cb(err, null);
                    }

                    if (city) {
                        customer.city = {
                            id: city.id,
                            name: city.name
                        };
                        createCustomer(customer);
                    }
                }
            );
        }

        function createCustomer(customer) {
            getNextSequence('customers', function (err, resp) {
                customer.code = resp.value.seq;
                Customer.create(customer, function (err) {
                    if (err) {
                        cb(err, null);
                    }

                    createdCount++;
                    if (createdCount === data.length) {
                        cb(null);
                    }
                });
            });
        }
    };


    Customer.remoteMethod('insertMany',
        {
            accepts: {arg: 'data', type: 'array'}
        });
}
;
