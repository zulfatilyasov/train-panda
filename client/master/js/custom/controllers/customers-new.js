'use strict';
angular.module('qeApp')
    .controller('AddCustomersController', customer);

customer.$inject = ['Customer', 'City', 'State', 'notify', '$stateParams', '$location'];

function customer(Customer, City, State, notify, $stateParams, $location) {
    var vm = this;
    vm.customer = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';
    console.log(vm.action);


    if ($stateParams.id) {
        vm.customer = Customer.findById({
            id: $stateParams.id
        }, function() {
            if (vm.customer.city)
                vm.cityId = vm.customer.city.id;
            if (vm.customer.state)
                vm.stateId = vm.customer.state.id;
        });
    }
    var hello = {
        world:"world"
    }
    if ($stateParams.id) {
        console.log(hello.world);
    }

    var cities = City.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function() {
        vm.cities = cities;
    });

    var states = State.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function() {
      vm.states = states;
    });

    function beforeSave() {
        if (vm.cityId) {
            var cities = vm.cities.filter(function(c) {
                return c.id === vm.cityId;
            });
            if (cities.length)
                vm.customer.city = cities[0];
        }
        if (vm.stateId) {
            var states = vm.states.filter(function(s) {
                return s.id === vm.stateId;
            });
            if (states.length)
                vm.customer.state = states[0];
        }
    }

    vm.save = function() {
        vm.saving = true;
        beforeSave();
        var saveSuccess = function() {
            vm.saving = false;
            notify.success('Customer saved!');
        };
        if (!vm.customer.id) {
            Customer.create(vm.customer, function() {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.customer.$save(function() {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
