'use strict';
angular.module('qeApp')
    .controller('HotelsController', hotels);

hotels.$inject = ['Hotel', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function hotels(Hotel, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;
    $rootScope.loading = true;
    vm.hotels = Hotel.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.hotels-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', Hotel);

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    vm.delete = function (cityId) {
        Hotel.deleteById({
            id: cityId
        }).$promise
            .then(function () {
                dataTables.removeRow(cityId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}
