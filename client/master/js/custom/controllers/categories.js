'use strict';
angular.module('qeApp')
    .controller('CategoriesController', categories);

categories.$inject = ['Category', 'datatables', '$timeout', 'csvParser'];

function categories(category, dataTables, $timeout, csvParser) {
    var vm = this;
    vm.categories = category.find(function() {
        $timeout(function() {
            dataTables.initDataTable('.categories-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', category);

    vm.delete = function(categoryId) {
        category.deleteById({
            id: categoryId
        }).$promise
            .then(function() {
                dataTables.removeRow(categoryId);
            });
    };

    vm.search = function() {
        dataTables.searchTable(vm.searchQuery);
    };
}
