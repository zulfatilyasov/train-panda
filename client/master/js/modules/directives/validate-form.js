/**=========================================================
 * Module: validate-form.js
 * Initializes the validation plugin Parsley
 =========================================================*/
'use strict';
App.directive('validateForm', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {
            var $elem = $($element);
            if ($.fn.parsley) {
                $elem.parsley();
            }
        }
    };
});
