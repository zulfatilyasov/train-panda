'use strict';
module.exports = function (State) {
    State.observe('after save', function (ctx, next) {
        if (ctx.instance) {
            var Customer = State.app.models.Customer;
            var City = State.app.models.City;
            updateModelsStateProperty(City, ctx.instance);
            updateModelsStateProperty(Customer, ctx.instance);
        }
        next();
    });

    function updateModelsStateProperty(Model, state) {
        Model.find({
            where: {
                'state.id': state.id
            }
        }, function (err, entities) {
            updateState(entities, state);
        });
    }

    function updateState(entities, state) {
        for (var i = entities.length - 1; i >= 0; i--) {
            entities[i].updateAttribute('state', {
                id: state.id,
                name: state.name
            });
        }
    }
};
