/**=========================================================
 * Module: config.js
 * App routes and resources configuration
 * Created with Angular UI Router
 * https://github.com/angular-ui/ui-router
 =========================================================*/
'use strict';
App.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'APP_REQUIRES',
    function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, appRequires) {

        App.controller = $controllerProvider.register;
        App.directive = $compileProvider.directive;
        App.filter = $filterProvider.register;
        App.factory = $provide.factory;
        App.service = $provide.service;
        App.constant = $provide.constant;
        App.value = $provide.value;

        // LAZY MODULES
        // -----------------------------------

        $ocLazyLoadProvider.config({
            debug: false,
            events: true,
            modules: appRequires.modules
        });


        // defaults to dashboard
        $urlRouterProvider.otherwise('/app/dashboard');

        //
        // Application Routes
        // -----------------------------------
        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: basepath('app.html'),
                controller: 'AppController',
                resolve: resolveFor('fastclick', 'modernizr', 'icons', 'screenfull', 'animo', 'sparklines', 'slimscroll', 'classyloader', 'toaster', 'papaparse', 'angularSpinner')
            })
            .state('page', {
                url: '/page',
                templateUrl: 'app/pages/page.html',
                resolve: resolveFor('modernizr', 'icons', 'parsley')
            })
            .state('page.login', {
                url: '/login',
                title: 'Login',
                templateUrl: 'app/pages/login.html'
            })
            .state('page.register', {
                url: '/register',
                title: 'Register',
                templateUrl: 'app/pages/register.html'
            })
            .state('app.dashboard', {
                url: '/dashboard',
                title: 'Dashboard',
                controller: 'DashboardController',
                controllerAs: 'vm',
                templateUrl: basepath('dashboard.html'),
                resolve: resolveFor('flot-chart', 'flot-chart-plugins')
            })

            .state('app.customers', {
                url: '/customers',
                title: 'Customers',
                controller: 'CustomersController',
                controllerAs: 'vm',
                templateUrl: basepath('customers.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.customers-new', {
                url: '/customers/new',
                title: 'Add Customer',
                controller: 'AddCustomersController',
                controllerAs: 'vm',
                templateUrl: basepath('customers-new.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.customers-edit', {
                url: '/customers/:id/edit',
                title: 'Add Customer',
                controller: 'AddCustomersController',
                controllerAs: 'vm',
                templateUrl: basepath('customers-new.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.customers-view', {
                url: '/customers/:id/view',
                title: 'Add Customer',
                controller: 'AddCustomersController',
                controllerAs: 'vm',
                templateUrl: basepath('customers-new.html')
            })

            .state('app.cities', {
                url: '/cities',
                title: 'Cities',
                controller: 'CitiesController',
                controllerAs: 'vm',
                templateUrl: basepath('cities.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.cities-new', {
                url: '/cities/new',
                title: 'City form',
                controller: 'CityFormController',
                controllerAs: 'vm',
                templateUrl: basepath('cities-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.cities-edit', {
                url: '/cities/:id/edit',
                title: 'City form',
                controller: 'CityFormController',
                controllerAs: 'vm',
                templateUrl: basepath('cities-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.cities-view', {
                url: '/cities/:id/view',
                title: 'City form',
                controller: 'CityFormController',
                controllerAs: 'vm',
                templateUrl: basepath('cities-form.html')
            })

            .state('app.states', {
                url: '/states',
                title: 'states',
                controller: 'StatesController',
                controllerAs: 'vm',
                templateUrl: basepath('states.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.states-new', {
                url: '/states/new',
                title: 'State form',
                controller: 'StateFormController',
                controllerAs: 'vm',
                templateUrl: basepath('state-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.states-edit', {
                url: '/states/:id/edit',
                title: 'State form',
                controller: 'StateFormController',
                controllerAs: 'vm',
                templateUrl: basepath('state-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.states-view', {
                url: '/states/:id/view',
                title: 'State form',
                controller: 'StateFormController',
                controllerAs: 'vm',
                templateUrl: basepath('state-form.html')
            })

            .state('app.stationCategories', {
                url: '/stationCategories',
                title: 'stationCategories',
                controller: 'StationCategoriesController',
                controllerAs: 'vm',
                templateUrl: basepath('station-categories.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.stationCategories-new', {
                url: '/stationCategories/new',
                title: 'StationCategory form',
                controller: 'StationCategoryFormController',
                controllerAs: 'vm',
                templateUrl: basepath('station-category-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.stationCategories-edit', {
                url: '/stationCategories/:id/edit',
                title: 'StationCategory form',
                controller: 'StationCategoryFormController',
                controllerAs: 'vm',
                templateUrl: basepath('station-category-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.stationCategories-view', {
                url: '/stationCategories/:id/view',
                title: 'StationCategory form',
                controller: 'StationCategoryFormController',
                controllerAs: 'vm',
                templateUrl: basepath('station-category-form.html')
            })

            .state('app.stations', {
                url: '/stations',
                title: 'stations',
                controller: 'StationsController',
                controllerAs: 'vm',
                templateUrl: basepath('stations.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.stations-new', {
                url: '/stations/new',
                title: 'Station form',
                controller: 'StationFormController',
                controllerAs: 'vm',
                templateUrl: basepath('station-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley', 'angularFileUpload')
            })

            .state('app.stations-edit', {
                url: '/stations/:id/edit',
                title: 'Station form',
                controller: 'StationFormController',
                controllerAs: 'vm',
                templateUrl: basepath('station-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley', 'angularFileUpload')
            })

            .state('app.stations-view', {
                url: '/stations/:id/view',
                title: 'Station form',
                controller: 'StationFormController',
                controllerAs: 'vm',
                templateUrl: basepath('station-form.html'),
                resolve: resolveFor('angularFileUpload')
            })

            .state('app.trains', {
                url: '/trains',
                title: 'trains',
                controller: 'TrainsController',
                controllerAs: 'vm',
                templateUrl: basepath('trains.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.trains-new', {
                url: '/trains/new',
                title: 'Train form',
                controller: 'TrainFormController',
                controllerAs: 'vm',
                templateUrl: basepath('train-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley', 'angularFileUpload')
            })

            .state('app.trains-edit', {
                url: '/trains/:id/edit',
                title: 'Train form',
                controller: 'TrainFormController',
                controllerAs: 'vm',
                templateUrl: basepath('train-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley', 'angularFileUpload')
            })

            .state('app.trains-view', {
                url: '/trains/:id/view',
                title: 'Train form',
                controller: 'TrainFormController',
                controllerAs: 'vm',
                templateUrl: basepath('train-form.html'),
                resolve: resolveFor('angularFileUpload')
            })

            .state('app.pnrs', {
                url: '/pnrs',
                title: 'pnrs',
                controller: 'PNRsController',
                controllerAs: 'vm',
                templateUrl: basepath('pnrs.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.pnrs-new', {
                url: '/pnrs/new',
                title: 'PNR form',
                controller: 'PNRFormController',
                controllerAs: 'vm',
                templateUrl: basepath('pnr-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.pnrs-edit', {
                url: '/pnrs/:id/edit',
                title: 'PNR form',
                controller: 'PNRFormController',
                controllerAs: 'vm',
                templateUrl: basepath('pnr-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.pnrs-view', {
                url: '/pnrs/:id/view',
                title: 'PNR form',
                controller: 'PNRFormController',
                controllerAs: 'vm',
                templateUrl: basepath('pnr-form.html')
            })

            .state('app.trips', {
                url: '/trips',
                title: 'trips',
                controller: 'TripsController',
                controllerAs: 'vm',
                templateUrl: basepath('trips.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.trips-new', {
                url: '/trips/new',
                title: 'Trip form',
                controller: 'TripFormController',
                controllerAs: 'vm',
                templateUrl: basepath('trip-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.trips-edit', {
                url: '/trips/:id/edit',
                title: 'Trip form',
                controller: 'TripFormController',
                controllerAs: 'vm',
                templateUrl: basepath('trip-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.trips-view', {
                url: '/trips/:id/view',
                title: 'Trip form',
                controller: 'TripFormController',
                controllerAs: 'vm',
                templateUrl: basepath('trip-form.html')
            })

            .state('app.alarms', {
                url: '/alarms',
                title: 'alarms',
                controller: 'AlarmsController',
                controllerAs: 'vm',
                templateUrl: basepath('alarms.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.trip-alarms', {
                url: '/trip/{tripId}/alarms',
                title: 'alarms',
                controller: 'AlarmsController',
                controllerAs: 'vm',
                templateUrl: basepath('alarms.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.alarms-new', {
                url: '/alarms/new',
                title: 'Alarm form',
                controller: 'AlarmFormController',
                controllerAs: 'vm',
                templateUrl: basepath('alarm-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.alarms-edit', {
                url: '/alarms/:id/edit',
                title: 'Alarm form',
                controller: 'AlarmFormController',
                controllerAs: 'vm',
                templateUrl: basepath('alarm-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.alarms-view', {
                url: '/alarms/:id/view',
                title: 'Alarm form',
                controller: 'AlarmFormController',
                controllerAs: 'vm',
                templateUrl: basepath('alarm-form.html')
            })

            .state('app.trainRatings', {
                url: '/trainRates',
                title: 'trainRatings',
                controller: 'TrainRatingsController',
                controllerAs: 'vm',
                templateUrl: basepath('train-raitings.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.trainRatings-new', {
                url: '/trainRatings/new',
                title: 'TrainRating form',
                controller: 'TrainRatingFormController',
                controllerAs: 'vm',
                templateUrl: basepath('train-raiting-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.trainRatings-edit', {
                url: '/trainRatings/:id/edit',
                title: 'TrainRating form',
                templateUrl: basepath('train-raiting-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.trainRatings-view', {
                url: '/trainRatings/:id/view',
                title: 'TrainRating form',
                controller: 'TrainRatingFormController',
                controllerAs: 'vm',
                templateUrl: basepath('train-raiting-form.html')
            })

            .state('app.cabs', {
                url: '/cabs',
                title: 'cabss',
                controller: 'CabsController',
                controllerAs: 'vm',
                templateUrl: basepath('cabs.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.cabs-new', {
                url: '/cabs/new',
                title: 'Cabs form',
                controller: 'CabsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('cab-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.cabs-edit', {
                url: '/cabs/:id/edit',
                title: 'Cabs form',
                controller: 'CabsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('cab-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.cabs-view', {
                url: '/cabs/:id/view',
                title: 'Cabs form',
                controller: 'CabsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('cab-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.restaurants', {
                url: '/restaurants',
                title: 'restaurants',
                controller: 'RestaurantsController',
                controllerAs: 'vm',
                templateUrl: basepath('restaurants.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.restaurants-items', {
                url: '/restaurant/{restaurantId}/items',
                title: 'restaurant items',
                controller: 'ItemsController',
                controllerAs: 'vm',
                templateUrl: basepath('items.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.restaurants-new', {
                url: '/restaurants/new',
                title: 'Restaurants form',
                controller: 'RestaurantsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('restaurant-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.restaurants-edit', {
                url: '/restaurants/:id/edit',
                title: 'Restaurants form',
                controller: 'RestaurantsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('restaurant-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.restaurants-view', {
                url: '/restaurants/:id/view',
                title: 'Restaurants form',
                controller: 'RestaurantsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('restaurant-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.hotels', {
                url: '/hotels',
                title: 'hotels',
                controller: 'HotelsController',
                controllerAs: 'vm',
                templateUrl: basepath('hotels.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularFileUpload')
            })

            .state('app.hotels-new', {
                url: '/hotels/new',
                title: 'Hotels form',
                controller: 'HotelsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('hotel-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.hotels-edit', {
                url: '/hotels/:id/edit',
                title: 'Hotels form',
                controller: 'HotelsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('hotel-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.hotels-view', {
                url: '/hotels/:id/view',
                title: 'Hotels form',
                controller: 'HotelsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('hotel-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.items', {
                url: '/items',
                title: 'items',
                controller: 'ItemsController',
                controllerAs: 'vm',
                templateUrl: basepath('items.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.items-new', {
                url: '/items/new',
                title: 'Items form',
                controller: 'ItemsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('item-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.items-edit', {
                url: '/items/:id/edit',
                title: 'Items form',
                controller: 'ItemsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('item-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.items-view', {
                url: '/items/:id/view',
                title: 'Items form',
                controller: 'ItemsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('item-form.html')
            })

            .state('app.categories', {
                url: '/categories',
                title: 'categories',
                controller: 'CategoriesController',
                controllerAs: 'vm',
                templateUrl: basepath('categories.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.categories-new', {
                url: '/categories/new',
                title: 'Categories form',
                controller: 'CategoriesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('category-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.categories-edit', {
                url: '/categories/:id/edit',
                title: 'Categories form',
                controller: 'CategoriesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('category-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.categories-view', {
                url: '/categories/:id/view',
                title: 'Categories form',
                controller: 'CategoriesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('category-form.html')
            })

            .state('app.subCategories', {
                url: '/subCategories',
                title: 'subCategories',
                controller: 'SubCategoriesController',
                controllerAs: 'vm',
                templateUrl: basepath('subcategories.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.subCategories-new', {
                url: '/subCategories/new',
                title: 'SubCategories form',
                controller: 'SubCategoriesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('subcategory-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.subCategories-edit', {
                url: '/subCategories/:id/edit',
                title: 'SubCategories form',
                controller: 'SubCategoriesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('subcategory-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.subCategories-view', {
                url: '/subCategories/:id/view',
                title: 'SubCategories form',
                controller: 'SubCategoriesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('subcategory-form.html')
            })

            .state('app.orders', {
                url: '/orders',
                title: 'orders',
                controller: 'OrdersController',
                controllerAs: 'vm',
                templateUrl: basepath('orders.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.orders-new', {
                url: '/orders/new',
                title: 'Orders form',
                controller: 'OrdersFormController',
                controllerAs: 'vm',
                templateUrl: basepath('order-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.orders-edit', {
                url: '/orders/:id/edit',
                title: 'Orders form',
                controller: 'OrdersFormController',
                controllerAs: 'vm',
                templateUrl: basepath('order-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.orders-view', {
                url: '/orders/:id/view',
                title: 'TrainRoutes form',
                controller: 'TrainRoutesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('order-form.html')
            })

            .state('app.trainRoutes', {
                url: '/trainRoutes',
                title: 'trainRoutes',
                controller: 'TrainRoutesController',
                controllerAs: 'vm',
                templateUrl: basepath('trainRoutes.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.trainRoutes-new', {
                url: '/trainRoutes/new',
                title: 'TrainRoutes form',
                controller: 'TrainRoutesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('trainRoute-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.trainRoutes-edit', {
                url: '/trainRoutes/:id/edit',
                title: 'TrainRoutes form',
                controller: 'TrainRoutesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('trainRoute-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.trainRoutes-view', {
                url: '/trainRoutes/:id/view',
                title: 'TrainRoutes form',
                controller: 'TrainRoutesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('trainRoute-form.html')
            })

            .state('app.train-routes', {
                url: '/train/{trainNo}/routes',
                title: 'train routes',
                controller: 'TrainRoutesController',
                controllerAs: 'vm',
                templateUrl: basepath('trainRoutes.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            });
        // Set here the base of the relative path
        // for all app views
        function basepath(uri) {
            return 'app/views/' + uri;
        }

        // Generates a resolve object by passing script names
        // previously configured in constant.APP_REQUIRES
        function resolveFor() {
            var _args = arguments;
            return {
                deps: ['$ocLazyLoad', '$q', function ($ocLL, $q) {
                    // Creates a promise chain for each argument
                    var promise = $q.when(1); // empty promise
                    for (var i = 0, len = _args.length; i < len; i++) {
                        promise = andThen(_args[i]);
                    }
                    return promise;

                    // creates promise to chain dynamically
                    function andThen(_arg) {
                        // also support a function that returns a promise
                        if (typeof _arg === 'function')
                            return promise.then(_arg);
                        else
                            return promise.then(function () {
                                // if is a module, pass the name. If not, pass the array
                                var whatToLoad = getRequired(_arg);
                                // simple error check
                                if (!whatToLoad) return $.error('Route resolve: Bad resource name [' + _arg + ']');
                                // finally, return a promise
                                return $ocLL.load(whatToLoad);
                            });
                    }

                    // check and returns required data
                    // analyze module items with the form [name: '', files: []]
                    // and also simple array of script files (for not angular js)
                    function getRequired(name) {
                        if (appRequires.modules)
                            for (var m in appRequires.modules)
                                if (appRequires.modules[m].name && appRequires.modules[m].name === name) {
                                    return appRequires.modules[m];
                                }
                        return appRequires.scripts && appRequires.scripts[name];
                    }

                }]
            };
        }

    }
]).config(['$translateProvider', function ($translateProvider) {

    $translateProvider.useStaticFilesLoader({
        prefix: 'app/i18n/',
        suffix: '.json'
    });
    $translateProvider.preferredLanguage('en');
    $translateProvider.useLocalStorage();

}]).config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeBar = true;
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.latencyThreshold = 500;
    cfpLoadingBarProvider.parentSelector = '.wrapper > section';
}])
    .controller('NullController', function () {
    });
