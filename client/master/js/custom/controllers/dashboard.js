'use strict';
angular.module('qeApp')
    .controller('DashboardController', dashboard);

dashboard.$inject = ['Customer', 'Order', 'Restaurant', 'Cab', 'Hotel', 'PNR', 'State', 'Train', 'City', 'Station', 'StationCategory', 'Trip', 'Alarm',
    'TrainRating', 'Item', 'TrainRoute', 'Category', '$timeout', '$state'];


function dashboard(Customer, Order, Restaurant, Cab, Hotel, PNR, State, Train, City, Station, StationCategory, Trip, Alarm, TrainRating, Item, TrainRoute, Category, $timeout, $state) {
    var vm = this;
    vm.customers = vm.orders = vm.cabs = vm.pnrs = vm.hotels = vm.states = vm.restaurants = 'loading..';
    vm.go = function(state) {
       $state.go(state);
    };
    Customer.count(function (data) {
        vm.customers = data.count;
    });

    Order.count(function (data) {
        vm.orders = data.count;
    });

    Restaurant.count(function (data) {
        vm.restaurants = data.count;
    });

    Cab.count(function (data) {
        vm.cabs = data.count;
    });

    Hotel.count(function (data) {
        vm.hotels = data.count;
    });

    PNR.count(function (data) {
        vm.pnrs = data.count;
    });

    State.count(function (data) {
        vm.states = data.count;
    });

    Train.count(function (data) {
        vm.trains = data.count;
    });

    City.count(function (data) {
        vm.cities = data.count;
    });

    Station.count(function (data) {
        vm.stations = data.count;
    });

    Trip.count(function (data) {
        vm.trips = data.count;
    });

    StationCategory.count(function (data) {
        vm.stationCategories = data.count;
    });

    Alarm.count(function (data) {
        vm.alarms = data.count;
    });

    TrainRating.count(function (data) {
        vm.trainRatings = data.count;
    });

    Category.count(function (data) {
        vm.itemCategories = data.count;
    });

    TrainRoute.count(function (data) {
        vm.routes = data.count;
    });

    Item.count(function (data) {
        vm.items = data.count;
    });
}
