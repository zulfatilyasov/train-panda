'use strict';

module.exports = {
    addBeforeSaveHook: addBeforeSaveHook
};

function addBeforeSaveHook(model) {
    model.observe('before save', function(ctx, next) {
        if (ctx.instance) {
            if (!ctx.instance.id) {
                ctx.instance.addedOn = new Date();
            }
            ctx.instance.updatedOn = new Date();
        } else {
            if (!ctx.data.id) {
                ctx.data.addedOn = new Date();
            }
            ctx.data.updatedOn = new Date();
        }
        next();
    });
}
