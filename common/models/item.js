"use strict";
var q = require('q');
module.exports = function (Item) {
    Item.insertMany = function (data, cb) {
        var Restaurant = Item.app.models.Restaurant;
        var Category = Item.app.models.Category;
        var SubCategory = Item.app.models.SubCategory;

        var promises = [];

        for (var i = 0, len = data.length; i < len; i++) {
            var item = data[i];
            var createItem = getCreateItemFn(item);

            var p = q.all([
                setItemRestaurantReference(item),
                setItemCategoryReference(item),
                setItemSubCategory(item)
            ]).then(createItem);

            promises.push(p);
        }

        q.all(promises)
            .then(function () {
                cb(null);
            }).catch(function (err) {
                cb(err, null);
            });

        function getCreateItemFn(item) {
            return function () {
                var def = q.defer();
                Item.create(item, function (err) {
                    if (err) {
                        def.reject(err);
                    }
                    else {
                        def.resolve();
                    }
                });
                return def.promise;
            };
        }

        function setItemSubCategory(item) {
            var def = q.defer();

            if (item.categoryCode) {
                SubCategory.findOne({
                    where: {
                        code: item.subcategoryCode
                    }
                }, setSubCategory);
            }
            else {
                def.resolve();
            }

            function setSubCategory(err, category) {
                if (err) {
                    def.reject(err);
                    return;
                }

                if (category) {
                    item.subCategoryId = category.id;
                }
                def.resolve();
            }

            return def.promise;
        }


        function setItemRestaurantReference(item) {
            var def = q.defer();

            if (item.restaurantCode) {
                Restaurant.findOne({
                    where: {
                        code: item.restaurantCode
                    }
                }, setItemRestaurant);
            }
            else {
                def.resolve();
            }

            function setItemRestaurant(err, restaurant) {
                if (err || !restaurant) {
                    def.reject(err);
                    return;
                }

                if (restaurant) {
                    item.restaurantId = restaurant.id;
                }
                def.resolve();
            }

            return def.promise;
        }

        function setItemCategoryReference(item) {
            var def = q.defer();

            if (item.categoryCode) {
                Category.findOne({
                    where: {
                        code: item.categoryCode
                    }
                }, setItemCategory);
            } else {
                def.resolve();
            }

            function setItemCategory(err, category) {
                if (err || !category) {
                    def.reject(err);
                    return;
                }

                if (category) {
                    item.categoryId = category.id;
                }
                def.resolve();
            }

            return def.promise;
        }
    };


    Item.remoteMethod('insertMany',
        {
            accepts: {arg: 'data', type: 'array'}
        });
};
