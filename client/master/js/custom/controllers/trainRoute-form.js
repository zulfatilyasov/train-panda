'use strict';
angular.module('qeApp')
    .controller('TrainRoutesFormController', trainRoute);

trainRoute.$inject = ['TrainRoute', 'Train', 'Station', 'notify', '$stateParams', '$location', '$state'];

function trainRoute(TrainRoute, Train, Station, notify, $stateParams, $location, $state) {
    var vm = this;
    vm.trainRoute = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        vm.trainRoute = TrainRoute.findById({id: $stateParams.id});
    }

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                code: true,
                name: true
            },
            order: 'code'
        }
    }, function () {
        vm.stations = stations;
    });

    var trains = Train.find({
        filter: {
            fields: {
                id: true,
                number: true
            },
            order: 'number'
        }
    }, function () {
        vm.trains = trains;
    });

    vm.deleteFile = function (index, id) {
        uploader.remove(id)
            .success(function () {
                vm.files.splice(index, 1);
            });
    };

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('TrainRoute saved!');
        };
        if (!vm.trainRoute.id) {
            vm.trainRoute = TrainRoute.create(vm.trainRoute, function () {
                saveSuccess(true);
                $state.go('app.trainRoutes-edit', {id: vm.trainRoute.id})
            }, saveFailed);
        } else {
            vm.trainRoute.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
