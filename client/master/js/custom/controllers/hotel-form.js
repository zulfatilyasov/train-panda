'use strict';
angular.module('qeApp')
    .controller('HotelsFormController', hotel);

hotel.$inject = ['Hotel', 'Station', 'notify', '$stateParams', '$location', 'uploader'];

function hotel(Hotel, Station, notify, $stateParams, $location, uploader) {
    var vm = this;
    vm.hotel = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        vm.hotel = Hotel.findById({
            id: $stateParams.id
        });
    }

    vm.uploader = null;
    vm.halfLength = 0;

    if ($stateParams.id) {
        vm.containerId = $stateParams.id;
        vm.uploader = uploader.create($stateParams.id, 'Hotel');
        vm.uploader.onCompleteAll = function () {
            loadFiles();
        };

        uploader.getContainer()
            .success(function (container) {
                if (container) {
                    loadFiles();
                }
            })
            .error(function (data) {
                if (data.error.status === 404) {
                    uploader.createContainer();
                }
            });
    }
    else {
        vm.noFiles = true;
    }

    function loadFiles() {
        uploader.loadFilesList()
            .success(function (data) {
                vm.files = data;
                vm.halfLength = Math.ceil(vm.files.length / 2);
            });
    }

    vm.deleteFile = function (index, id) {
        uploader.remove(id)
            .success(function () {
                vm.files.splice(index, 1);
            });
    };

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                name: true,
                code:true
            },
            order: 'code'
        }
    }, function () {
        vm.stations = stations;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('hotel saved!');
        };
        if (!vm.hotel.id) {
            Hotel.create(vm.hotel, function () {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.hotel.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
