'use strict';
angular.module('qeApp')
    .controller('AlarmFormController', alarm);

alarm.$inject = ['Alarm', 'Trip', 'Station', 'Customer', 'notify', '$stateParams', '$location', '$state'];

function alarm(Alarm, Trip, Station, Customer, notify, $stateParams, $location, $state) {
    var vm = this;
    vm.alarm = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    var filter = {
        'filter[include]': ['trip', 'customer'],
        'filter[where][id]': $stateParams.id
    };

    if ($stateParams.id) {
        Alarm.query(filter, function (alarms) {
            vm.alarm = alarms[0];
        });
    }

    var trips = Trip.find({
        filter: {
            fields: {
                id: true,
                name:true
            }
        }
    }, function () {
        vm.trips = trips;
    });

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                name: true,
                code:true
            }
        }
    }, function () {
        vm.stations = stations;
    });
    var customers = Customer.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.customers = customers;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('Alarm saved!');
        };
        if (!vm.alarm.id) {
            vm.alarm = Alarm.create(vm.alarm, function () {
                saveSuccess(true);
                $state.go('app.alarms-edit', {id: vm.alarm.id})
            }, saveFailed);
        } else {
            vm.alarm.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
