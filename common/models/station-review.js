"use strict";

module.exports = function (StationReview) {
    StationReview.observe('after save', function (ctx, next) {
        var Station = StationReview.app.models.Station;
        var rate = ctx.instance;
        if (rate && rate.stationCode) {
            Station.findOne({code: rate.stationCode}, function (err, station) {
                if (!station.ratesCount) {
                    station.ratesCount = 0;
                }
                var getAvg = function (field) {
                    if (!station[field]) {
                        station[field] = 0;
                    }
                    return (station[field] * station.ratesCount + rate[field]) / (station.ratesCount + 1);
                };
                if (station) {
                    var properies = [
                        'accomodationStandard',
                        'snacksEatable',
                        'teaDrinkable',
                        'waitingHallCondition'
                    ];
                    for (var i = 0, len = properies.length; i < len; i++) {
                        var prop = properies[i];
                        if (rate[prop]) {
                            station[prop] = getAvg(prop);
                        }
                    }
                    station.ratesCount++;
                    station.save(function (err) {
                        if (err) {
                            next(err);
                        }
                        else {
                            next();
                        }
                    });
                } else {
                    next();
                }
            });

        }
    });
};
