'use strict';
angular.module('qeApp')
    .controller('TrainRatingFormController', trainRating);

trainRating.$inject = ['TrainRating', 'PNR', 'Train', 'Trip', 'notify', '$stateParams', '$location', '$state'];

function trainRating(TrainRating, PNR, Train, Trip, notify, $stateParams, $location, $state) {
    var vm = this;
    vm.trainRating = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    var filter = {
        'filter[include]': ['train', 'trip', 'PNR'],
        'filter[where][id]': $stateParams.id
    };

    if ($stateParams.id) {
        TrainRating.query(filter, function (trainRatings) {
            vm.trainRating = trainRatings[0];
        });
    }

    if (vm.action === 'upsert') {
        var pnrs = PNR.find({
            filter: {
                fields: {
                    id: true,
                    pnr: true
                }
            }
        }, function () {
            vm.pnrs = pnrs;
        });

        var trips = Trip.find({
            filter: {
                fields: {
                    id: true,
                    name: true
                }
            }
        }, function () {
            vm.trips = trips;
        });

        var trains = Train.find({
            filter: {
                fields: {
                    id: true,
                    number: true
                }
            }
        }, function () {
            vm.trains = trains;
        });
    }

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('TrainRating saved!');
        };
        if (!vm.trainRating.id) {
            vm.trainRating = TrainRating.create(vm.trainRating, function () {
                saveSuccess(true);
                $state.go('app.trainRatings-edit', {id: vm.trainRating.id})
            }, saveFailed);
        } else {
            vm.trainRating.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
