'use strict';
angular.module('qeApp')
    .controller('CustomersController', customers);

customers.$inject = ['Customer', 'datatables', '$timeout', '$state', 'csvParser', '$rootScope'];

function customers(Customer, dataTables, $timeout, $state, csvParser, $rootScope) {
    var vm = this;
    $rootScope.loading = true;
    vm.customers = Customer.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.customers-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', Customer);

    vm.delete = function (customerId) {
        Customer.deleteById({
            id: customerId
        }).$promise
            .then(function () {
                dataTables.removeRow(customerId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}
