'use strict';
angular.module('qeApp')
    .controller('PNRsController', pnrs);

pnrs.$inject = ['PNR', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function pnrs(PNR, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;
    $rootScope.loading = true;
    vm.pnrs = PNR.find(function() {
        $timeout(function() {
            dataTables.initDataTable('.pnrs-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', PNR);

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    vm.delete = function(pnrId) {
        PNR.deleteById({
                id: pnrId
            }).$promise
            .then(function() {
                dataTables.removeRow(pnrId);
            });
    };

    vm.search = function() {
        dataTables.searchTable(vm.searchQuery);
    };
}
