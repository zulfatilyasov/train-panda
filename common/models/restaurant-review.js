"use strict";

module.exports = function (RestaurantReview) {
    RestaurantReview.observe('after save', function (ctx, next) {
        var Restaurant = RestaurantReview.app.models.Restaurant;
        var rate = ctx.instance;
        if (rate && rate.restaurantId) {
            Restaurant.findById(rate.restaurantId, function (err, restaurant) {
                if (!restaurant.ratesCount) {
                    restaurant.ratesCount = 0;
                }
                var getAvg = function (field) {
                    if (!restaurant[field]) {
                        restaurant[field] = 0;
                    }
                    return (restaurant[field] * restaurant.ratesCount + rate[field]) / (restaurant.ratesCount + 1);
                };
                if (restaurant) {
                    var properies = [
                        'rating'
                    ];
                    for (var i = 0, len = properies.length; i < len; i++) {
                        var prop = properies[i];
                        if (rate[prop]) {
                            restaurant[prop] = getAvg(prop);
                        }
                    }
                    restaurant.ratesCount++;
                    restaurant.save(function (err) {
                        if (err) {
                            next(err);
                        }
                        else {
                            next();
                        }
                    });
                } else {
                    next();
                }
            });

        }
    });
};

