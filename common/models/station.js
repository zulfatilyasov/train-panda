'use strict';
var q = require('q');

module.exports = function (Station) {
    Station.observe('after save', function (ctx, next) {

        var PNR = Station.app.models.PNR;

        function updatePnrs(stationProp) {
            var where = {};
            where[stationProp + '.id'] = ctx.instance.id;

            PNR.find({
                where: where
            }, function (err, pnrs) {
                if (err) {
                    throw err;
                }
                updatePnrsStation(pnrs, stationProp);
            });
        }

        function updatePnrsStation(pnrs, stationProp) {
            for (var i = 0, len = pnrs.length; i < len; i++) {
                pnrs[i].updateAttribute(stationProp, {
                    id: ctx.instance.id,
                    name: ctx.instance.name
                });
            }
        }

        if (ctx.instance) {
            updatePnrs('startingStation');
            updatePnrs('endStation');
        }
        next();
    });

    Station.insertMany = function (data, cb) {
        var State = Station.app.models.State;
        var City = Station.app.models.City;
        var Category = Station.app.models.StationCategory;

        var promises = [];

        for (var i = 0, len = data.length; i < len; i++) {
            var station = data[i];
            station.ATM = (station.ATM.toLowerCase() === 'true');
            station.bookShop = (station.bookShop.toLowerCase() === 'true');
            station.trainPandaDelivery = (station.trainPandaDelivery.toLowerCase() === 'true');
            var createStation = getCreateStationFn(station);

            var p = q.all([
                setStationStateReference(station),
                setStationCityReference(station),
                setStationCategory(station)
            ]).then(createStation);

            promises.push(p);
        }

        q.all(promises)
            .then(function () {
                cb(null);
            }).catch(function (err) {
                cb(err, null);
            });

        function getCreateStationFn(station) {
            return function () {
                var def = q.defer();
                Station.create(station, function (err) {
                    if (err) {
                        def.reject(err);
                    }
                    else {
                        def.resolve();
                    }
                });
                return def.promise;
            };
        }

        function setStationCategory(station) {
            var def = q.defer();

            if (station.categoryCode) {
                Category.findOne({
                    where: {
                        code: station.categoryCode
                    }
                }, setCategory);
            }
            else {
                def.resolve();
            }

            function setCategory(err, category) {
                if (err) {
                    def.reject(err);
                    return;
                }

                if (category) {
                    station.category = {
                        id: category.id,
                        name: category.name
                    };
                }
                def.resolve();
            }

            return def.promise;
        }


        function setStationStateReference(station) {
            var def = q.defer();

            if (station.stateCode) {
                State.findOne({
                    where: {
                        code: station.stateCode
                    }
                }, setStationState);
            }
            else {
                def.resolve();
            }

            function setStationState(err, state) {
                if (err || !state) {
                    def.reject(err);
                    return;
                }

                if (state) {
                    station.stateId = state.id;
                }
                def.resolve();
            }

            return def.promise;
        }

        function setStationCityReference(station) {
            var def = q.defer();

            if (station.cityCode) {
                City.findOne({
                    where: {
                        code: station.cityCode
                    }
                }, setStationCity);
            } else {
                def.resolve();
            }

            function setStationCity(err, city) {
                if (err || !city) {
                    def.reject(err);
                    return;
                }

                if (city) {
                    station.cityId = city.id;
                }
                def.resolve();
            }

            return def.promise;
        }
    };


    Station.remoteMethod('insertMany',
        {
            accepts: {arg: 'data', type: 'array'}
        });

};
