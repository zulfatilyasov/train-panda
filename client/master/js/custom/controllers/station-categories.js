'use strict';
angular.module('qeApp')
    .controller('StationCategoriesController', stationCategories);

stationCategories.$inject = ['StationCategory', 'datatables', '$timeout', 'csvParser'];

function stationCategories(StationCategory, dataTables, $timeout, csvParser) {
    var vm = this;
    vm.stationCategories = StationCategory.find(function() {
        $timeout(function() {
            dataTables.initDataTable('.stationCategories-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', StationCategory);

    vm.delete = function(stationCategoryId) {
        StationCategory.deleteById({
                id: stationCategoryId
            }).$promise
            .then(function() {
                dataTables.removeRow(stationCategoryId);
            });
    };

    vm.search = function() {
        dataTables.searchTable(vm.searchQuery);
    };
}
