'use strict';
angular.module('qeApp')
    .controller('CabsFormController', cab);

cab.$inject = ['Cab','Station', 'notify', '$stateParams', '$location', '$state','uploader'];

function cab(Cab, Station, notify, $stateParams, $location, $state, uploader) {
    var vm = this;
    vm.cab = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        vm.cab = Cab.findById({id: $stateParams.id});
    }

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                code: true,
                name: true
            },
            order: 'code'
        }
    }, function () {
        vm.stations = stations;
    });

    vm.uploader = null;
    vm.halfLength = 0;

    if ($stateParams.id) {
        vm.containerId = $stateParams.id;
        vm.uploader = uploader.create($stateParams.id, 'Cab');
        vm.uploader.onCompleteAll = function () {
            loadFiles();
        };

        uploader.getContainer()
            .success(function (container) {
                if (container) {
                    loadFiles();
                }
            })
            .error(function (data) {
                if (data.error.status === 404) {
                    uploader.createContainer();
                }
            });
    }
    else {
        vm.noFiles = true;
    }

    function loadFiles() {
        uploader.loadFilesList()
            .success(function (data) {
                vm.files = data;
                vm.halfLength = Math.ceil(vm.files.length / 2);
            });
    }

    vm.deleteFile = function (index, id) {
        uploader.remove(id)
            .success(function () {
                vm.files.splice(index, 1);
            });
    };

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('Cab saved!');
        };
        if (!vm.cab.id) {
            vm.cab = Cab.create(vm.cab, function () {
                saveSuccess(true);
                $state.go('app.cabs-edit', {id: vm.cab.id})
            }, saveFailed);
        } else {
            vm.cab.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
