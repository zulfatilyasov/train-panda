'use strict';
angular.module('qeApp')
    .controller('PNRFormController', pnr);

pnr.$inject = ['PNR', 'Station', 'Customer', 'Train', 'notify', '$stateParams', '$location', '$filter'];

function pnr(PNR, Station, Customer, Train, notify, $stateParams, $location, $filter) {
    var vm = this;
    vm.pnr = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';

    if ($stateParams.id) {
        vm.pnr = PNR.findById({
            id: $stateParams.id
        }, function() {
            if (vm.pnr.startingStation)
                vm.startStationId = vm.pnr.startingStation.id;
            if (vm.pnr.endStation)
                vm.endStationId = vm.pnr.endStation.id;
            if (vm.pnr.customer)
                vm.customerId = vm.pnr.customer.id;
            changeDates();
        });
    }

    function changeDates() {
        if (vm.pnr.startDate) {
            vm.pnr.startDate = formatDate(vm.pnr.startDate);
        }
        if (vm.pnr.endDate) {
            vm.pnr.endDate = formatDate(vm.pnr.endDate);
        }
    }

    function formatDate(date) {
        return $filter('date')(date, 'MM/dd/yyyy');
    }
    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                name: true,
                code: true
            },
            order: 'code'
        }
    }, function() {
        vm.stations = stations;
    });

    var customers = Customer.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function() {
        vm.customers = customers;
    });

    var trains = Train.find({
        filter: {
            fields: {
                id: true,
                number: true
            }
        }
    }, function() {
        vm.trains = trains;
    });

    function beforeSave() {
        if (vm.customerId) {
            var customers = vm.customers.filter(function(c) {
                return c.id === vm.customerId;
            });
            if (customers.length)
                vm.pnr.customer = customers[0];
        }
        if (vm.startStationId) {
            var stations = vm.stations.filter(function(s) {
                return s.id === vm.startStationId;
            });
            if (stations.length)
                vm.pnr.startingStation = stations[0];
        }
        if (vm.endStationId) {
            var endStations = vm.stations.filter(function(s) {
                return s.id === vm.endStationId;
            });
            if (endStations.length)
                vm.pnr.endStation = endStations[0];
        }
    }

    vm.save = function() {
        vm.saving = true;
        beforeSave();
        var saveSuccess = function() {
            vm.saving = false;
            changeDates();
            notify.success('PNR saved!');
        };
        if (!vm.pnr.id) {
            PNR.create(vm.pnr, function() {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.pnr.$save(function() {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
