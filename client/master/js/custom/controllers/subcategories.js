'use strict';
angular.module('qeApp')
    .controller('SubCategoriesController', subCategories);

subCategories.$inject = ['SubCategory', 'datatables', '$timeout', 'csvParser'];

function subCategories(SubCategory, dataTables, $timeout, csvParser) {
    var vm = this;
    vm.subCategories = SubCategory.find(function() {
        $timeout(function() {
            dataTables.initDataTable('.subCategories-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', SubCategory);

    vm.delete = function(subcategoryId) {
        SubCategory.deleteById({
            id: subcategoryId
        }).$promise
            .then(function() {
                dataTables.removeRow(subcategoryId);
            });
    };

    vm.search = function() {
        dataTables.searchTable(vm.searchQuery);
    };
}
