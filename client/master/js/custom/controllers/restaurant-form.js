'use strict';
angular.module('qeApp')
    .controller('RestaurantsFormController', restaurant);

restaurant.$inject = ['Restaurant', 'Station', 'notify', '$stateParams', '$location', '$state', 'uploader'];

function restaurant(Restaurant, Station, notify, $stateParams, $location, $state, uploader) {
    var vm = this;
    vm.restaurant = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        vm.restaurant = Restaurant.findById({id: $stateParams.id});
    }

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                code: true,
                name: true
            },
            order: 'code'
        }
    }, function () {
        vm.stations = stations;
    });

    vm.uploader = null;
    vm.halfLength = 0;

    if ($stateParams.id) {
        vm.containerId = $stateParams.id;
        vm.uploader = uploader.create($stateParams.id, 'Restaurant');
        vm.uploader.onCompleteAll = function () {
            loadFiles();
        };
        uploader.getContainer()
            .success(function (container) {
                if (container) {
                    loadFiles();
                }
            })
            .error(function (data) {
                if (data.error.status === 404) {
                    uploader.createContainer();
                }
            });
    }
    else {
        vm.noFiles = true;
    }

    function loadFiles() {
        uploader.loadFilesList()
            .success(function (data) {
                vm.files = data;
                vm.halfLength = Math.ceil(vm.files.length / 2);
            });
    }

    vm.deleteFile = function (index, id) {
        uploader.remove(id)
            .success(function () {
                vm.files.splice(index, 1);
            });
    };

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('Restaurant saved!');
        };
        if (!vm.restaurant.id) {
            vm.restaurant = Restaurant.create(vm.restaurant, function () {
                saveSuccess(true);
                $state.go('app.restaurants-edit', {id: vm.restaurant.id})
            }, saveFailed);
        } else {
            vm.restaurant.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
