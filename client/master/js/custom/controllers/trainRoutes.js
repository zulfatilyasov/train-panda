'use strict';
angular.module('qeApp')
    .controller('TrainRoutesController', trainRoutes);

trainRoutes.$inject = ['TrainRoute', 'datatables', '$timeout', 'csvParser', '$stateParams', '$rootScope'];

function trainRoutes(TrainRoute, dataTables, $timeout, csvParser, $stateParams, $rootScope) {
    var vm = this;

    var where = null;
    if ($stateParams.trainNo) {
        where = {
            trainNo: $stateParams.trainNo
        };
    }

    $rootScope.loading = true;
    vm.trainRoutes = TrainRoute.find({filter: {where: where}}, function () {
        $timeout(function () {
            dataTables.initDataTable('.trainRoutes-datatable');
        })
    });

    csvParser.attachInputWatcher('.upload-select', TrainRoute);

    vm.delete = function (trainRouteId) {
        TrainRoute.deleteById({
            id: trainRouteId
        }).$promise
            .then(function () {
                dataTables.removeRow(trainRouteId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}
