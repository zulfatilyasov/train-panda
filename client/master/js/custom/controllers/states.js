'use strict';
angular.module('qeApp')
    .controller('StatesController', states);

states.$inject = ['State', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function states(State, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;

    $rootScope.loading = true;
    vm.states = State.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.states-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', State);

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    vm.delete = function (stateId) {
        State.deleteById({
            id: stateId
        }).$promise
            .then(function () {
                dataTables.removeRow(stateId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}
