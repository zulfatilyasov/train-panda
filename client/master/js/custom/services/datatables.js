'use strict';
angular.module('qeApp')
    .factory('datatables', ['$rootScope', dataTables]);

function dataTables($rootScope) {
    var table = null;

    function initDataTable(selector) {
        var tableSelector = selector || '.datatable';
        // if (!$.fn.DataTable.isDataTable(tableSelector || '.datatable')) {
        table = $(tableSelector).dataTable({
            'sDom': 't<pl>',
            'paging': true, // Table pagination
            'ordering': true,
            'initComplete': function () {
                $rootScope.loading = false;
            },
            'searching': true, // Column ordering
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            oLanguage: {
                sSearch: 'Search all columns:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            }
        });
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('thead.filters .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs
            .keyup(function () {
                table.fnFilter(this.value, columnInputs.index(this));
            });
        columnInputs.click(function (event) {
            event.stopPropagation();
            event.preventDefault();
        });
    }

    function removeRow(id) {
        var rowselector = '.' + id;
        table.fnDeleteRow($(rowselector));
    }

    function searchTable(query) {
        table.fnFilter(query);
    }

    function draw() {
        table.fnDraw();
    }

    function destroy() {
        table.fnDestroy();
    }

    return {
        searchTable: searchTable,
        initDataTable: initDataTable,
        removeRow: removeRow,
        destroy: destroy,
        draw: draw
    };
}
