'use strict';
module.exports = function (Train) {
    Train.insertMany = function (data, cb) {
        var createdCount = 0;
        var error = null;
        var getAfterCreateFn = function (createdTrain) {
            return function (err) {
                if (err) {
                    cb(err, null);
                    console.log(createdTrain);
                    error = err;
                }
                createdCount++;
                if (createdCount === data.length) {
                    cb(null);
                }
            };
        };

        for (var i = 0, len = data.length; i < len && !error; i++) {
            var train = data[i];
            train.pantryCheck = train.pantryCheck.toLowerCase() === 'true';
            var afterCreate = getAfterCreateFn(train);
            Train.create(train, afterCreate);
        }

        //Train.create(data, function (err) {
        //    if (err) {
        //        cb(err, null);
        //    }
        //    else {
        //        cb(null);
        //    }
        //});
    };

    Train.remoteMethod('insertMany',
        {
            accepts: {arg: 'data', type: 'array'}
        });
};
