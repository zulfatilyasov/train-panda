'use strict';

angular.module('qeApp')
    .factory('uploader', upload);

upload.$inject = ['FileUploader', '$http'];

function upload(FileUploader, $http) {
    var containerId = '';
    var filesUrl = '';
    var modelName = '';

    function create(id, model) {
        modelName = model;
        containerId = id;
        filesUrl = '/api/containers/' + containerId + '/files';
        return new FileUploader({
            url: '/api/containers/' + containerId + '/upload',
            formData: [
                {model: modelName}
            ]
        });
    }

    function remove(id) {
        return $http.delete(filesUrl + '/' + encodeURIComponent(id) + '?model=' + modelName);
    }

    function loadFilesList() {
        return $http.get(filesUrl);
    }

    function getContainer() {
        return $http.get('/api/containers/' + containerId);
    }

    function createContainer() {
        return $http.post('/api/containers', {
            name: containerId
        });
    }

    return {
        create: create,
        remove: remove,
        loadFilesList: loadFilesList,
        getContainer: getContainer,
        createContainer: createContainer
    }
}
