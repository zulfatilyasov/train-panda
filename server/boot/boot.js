"use strict";

module.exports = function (app) {
    app.models().forEach(function (Model) {
        if (Model.dataSource.name !== app.dataSources.mongodb.name){
            return;
        }
        if (Model.definition.settings.allowInsertMany) {
            Model.insertMany = function (data, cb) {
                Model.create(data, function (err) {
                    if (err) {
                        cb(err, null);
                    } else {
                        cb(null);
                    }
                });
            };

            Model.remoteMethod('insertMany',
                {
                    accepts: {arg: 'data', type: 'array'}
                });
        }
        if (hasUpdatedOnProperty(Model)) {
            Model.observe('before save', function (ctx, next) {
                if (ctx.instance) {
                    if (!ctx.instance.id) {
                        ctx.instance.addedOn = new Date();
                    }
                    ctx.instance.updatedOn = new Date();
                } else {
                    if (!ctx.data.id) {
                        ctx.data.addedOn = new Date();
                    }
                    ctx.data.updatedOn = new Date();
                }
                next();
            });
        }
    });

    function hasUpdatedOnProperty(Model){
        return Model.definition && Model.definition.properties && Model.definition.properties.addedOn && Model.definition.properties.updatedOn;
    }
};
