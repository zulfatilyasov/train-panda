'use strict';
angular.module('qeApp')
    .controller('TrainFormController', train);

train.$inject = ['Train', 'Station', 'notify', '$stateParams', '$location', 'uploader', '$state'];

function train(Train, Station, notify, $stateParams, $location, uploader, $state) {
    var vm = this;
    vm.train = {};
    vm.action = 'upsert';
    vm.uploader = null;

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    vm.halfLength = 0;
    if ($stateParams.id) {
        vm.train = Train.findById({
            id: $stateParams.id
        });

        vm.containerId = $stateParams.id;
        vm.uploader = uploader.create($stateParams.id, 'Train');
        vm.uploader.onCompleteAll = function () {
            loadFiles();
        };

        uploader.getContainer()
            .success(function (container) {
                if (container) {
                    loadFiles();
                }
            })
            .error(function (data) {
                if (data.error.status === 404) {
                    uploader.createContainer();
                }
            })
    }
    else {
        vm.noFiles = true;
    }

    function loadFiles() {
        uploader.loadFilesList()
            .success(function (data) {
                vm.files = data;
                vm.halfLength = Math.ceil(vm.files.length / 2);
            });
    }

    vm.deleteFile = function (index, id) {
        uploader.remove(id)
            .success(function () {
                vm.files.splice(index, 1);
            });
    };

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                name: true,
                code: true
            },
            order: 'code'
        }
    }, function () {
        vm.stations = stations;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('Train saved!');
        };
        if (!vm.train.id) {
            vm.train = Train.create(vm.train, function () {
                saveSuccess(true);
                $state.go('app.trains-edit', {id: vm.train.id})
            }, saveFailed);
        } else {
            vm.train.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
