'use strict';
angular.module('qeApp')
    .controller('CityFormController', city);

city.$inject = ['City', 'State', 'notify', '$stateParams', '$location'];

function city(City, State, notify, $stateParams, $location) {
    var vm = this;
    vm.city = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';

    if ($stateParams.id) {
        vm.city = City.findById({
            id: $stateParams.id
        }, function() {
            console.log(vm.city.isUrban);
            if (vm.city.state)
                vm.stateId = vm.city.state.id;
        });
    }

    var cities = City.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function() {
        vm.cities = cities;
    });

    vm.states = State.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    });

    function beforeSave() {
        if (vm.stateId) {
            var states = vm.states.filter(function(s) {
                return s.id === vm.stateId;
            });
            if (states.length)
                vm.city.state = states[0];
        }
    }

    vm.save = function() {
        vm.saving = true;
        beforeSave();
        var saveSuccess = function() {
            vm.saving = false;
            notify.success('city saved!');
        };
        if (!vm.city.id) {
            City.create(vm.city, function() {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.city.$save(function() {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
