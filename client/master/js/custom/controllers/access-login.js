'use strict';
angular.module('qeApp')
  .controller('LoginFormController', ['$state', 'Users', function($state, Users) {
    var vm = this;
    // bind here all data from the form
    vm.account = {};
    // place the message if something goes wrong
    vm.authMsg = '';

    var authSuccess = function(accessToken) {
      console.log(accessToken);
      if (accessToken.id) {
        $state.go('app.dashboard');
      }
    };

    var authFail = function(resp) {
      console.log(resp);
      vm.authMsg = 'Authentication failed';
    };

    vm.login = function() {
      vm.authMsg = '';
      console.log(vm.account);
      vm.loginResult = Users.login({
        rememberMe: vm.account.rememberMe
      }, vm.account, authSuccess, authFail);
    };

  }]);
