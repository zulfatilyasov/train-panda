'use strict';
angular.module('qeApp')
  .controller('StateFormController', state);

state.$inject = ['State', 'notify', '$stateParams', '$location'];

function state(State, notify, $stateParams, $location) {
  var vm = this;
  vm.state = {country: 'India'};
  vm.action = 'upsert';

  if ($location.path().indexOf('view') >= 0)
    vm.action = 'view';

  if ($stateParams.id) {
    vm.state = State.findById({
      id: $stateParams.id
    }, function () {
      if (vm.state.state)
        vm.stateId = vm.state.state.id;
    });
  }

  var states = State.find({
    filter: {
      fields: {
        id: true,
        name: true
      }
    }
  }, function () {
    vm.states = states;
  });

  vm.save = function () {
    vm.saving = true;
    var saveSuccess = function () {
      vm.saving = false;
      notify.success('State saved!');
    };
    if (!vm.state.id) {
      State.create(vm.state, function () {
        saveSuccess();
      }, saveFailed);
    } else {
      vm.state.$save(function () {
        saveSuccess();
      }, saveFailed);
    }
  };

  function saveFailed(resp) {
    vm.saving = false;
    if (resp && resp.data && resp.data.error)
      console.log(resp.data.error);
    if (resp && resp.data && resp.data.error)
      notify.error(resp.data.error);
  }
}
