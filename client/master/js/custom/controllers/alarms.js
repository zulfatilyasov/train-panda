'use strict';
angular.module('qeApp')
    .controller('AlarmsController', alarms);

alarms.$inject = ['Alarm', 'datatables', '$timeout', '$stateParams', '$rootScope'];

function alarms(Alarm, dataTables, $timeout, $stateParams, $rootScope) {
    var vm = this;
    vm.tripId = $stateParams.tripId;

    var filter = {
        'filter[include]': ['trip', 'customer']
    };

    if (vm.tripId) {
        filter['filter[where][tripId]'] = vm.tripId;
    }

    $rootScope.loading = true;
    Alarm.query(filter, function (alarms) {
        vm.alarms = alarms;
        $timeout(function () {
            dataTables.initDataTable('.alarms-datable');
        })
    });

    vm.delete = function (alarmId) {
        Alarm.deleteById({
            id: alarmId
        }).$promise
            .then(function () {
                debugger;
                dataTables.removeRow(alarmId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

