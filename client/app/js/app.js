/*!
 *
 * Angle - Bootstrap Admin App + AngularJS
 *
 * Author: @themicon_co
 * Website: http://themicon.co
 * License: http://support.wrapbootstrap.com/knowledge_base/topics/usage-licenses
 *
 */
'use strict';
if (typeof $ === 'undefined') {
    throw new Error('This application\'s JavaScript requires jQuery');
}


// APP START
// ----------------------------------- 

var App = angular.module('angle', ['ngRoute', 'ngAnimate', 'ngStorage', 'ngCookies', 'pascalprecht.translate', 'ui.bootstrap', 'ui.router',
        'oc.lazyLoad', 'cfp.loadingBar', 'ngSanitize', 'ngResource'
    ])
    .run(['$rootScope', '$state', '$stateParams', '$window', '$templateCache', function($rootScope, $state, $stateParams, $window, $templateCache) {
        // Set reference to access them from any scope
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.$storage = $window.localStorage;

        // Uncomment this to disables template cache
        /*$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            if (typeof(toState) !== 'undefined'){
              $templateCache.remove(toState.templateUrl);
            }
        });*/

        // Scope Globals
        // ----------------------------------- 
        $rootScope.app = {
            name: 'Train panda',
            year: ((new Date()).getFullYear()),
            layout: {
                isFixed: true,
                isCollapsed: false,
                isBoxed: false,
                isRTL: false
            },
            viewAnimation: 'animated ng-fadeInUp'
        };
        $rootScope.user = {
            name: 'Panda',
            job: 'Developer',
            picture: 'app/img/user/05.jpg'
        };
    }]);

/**=========================================================
 * Module: config.js
 * App routes and resources configuration
 * Created with Angular UI Router
 * https://github.com/angular-ui/ui-router
 =========================================================*/
'use strict';
App.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'APP_REQUIRES',
    function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, appRequires) {

        App.controller = $controllerProvider.register;
        App.directive = $compileProvider.directive;
        App.filter = $filterProvider.register;
        App.factory = $provide.factory;
        App.service = $provide.service;
        App.constant = $provide.constant;
        App.value = $provide.value;

        // LAZY MODULES
        // -----------------------------------

        $ocLazyLoadProvider.config({
            debug: false,
            events: true,
            modules: appRequires.modules
        });


        // defaults to dashboard
        $urlRouterProvider.otherwise('/app/dashboard');

        //
        // Application Routes
        // -----------------------------------
        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: basepath('app.html'),
                controller: 'AppController',
                resolve: resolveFor('fastclick', 'modernizr', 'icons', 'screenfull', 'animo', 'sparklines', 'slimscroll', 'classyloader', 'toaster', 'papaparse', 'angularSpinner')
            })
            .state('page', {
                url: '/page',
                templateUrl: 'app/pages/page.html',
                resolve: resolveFor('modernizr', 'icons', 'parsley')
            })
            .state('page.login', {
                url: '/login',
                title: 'Login',
                templateUrl: 'app/pages/login.html'
            })
            .state('page.register', {
                url: '/register',
                title: 'Register',
                templateUrl: 'app/pages/register.html'
            })
            .state('app.dashboard', {
                url: '/dashboard',
                title: 'Dashboard',
                controller: 'DashboardController',
                controllerAs: 'vm',
                templateUrl: basepath('dashboard.html'),
                resolve: resolveFor('flot-chart', 'flot-chart-plugins')
            })

            .state('app.customers', {
                url: '/customers',
                title: 'Customers',
                controller: 'CustomersController',
                controllerAs: 'vm',
                templateUrl: basepath('customers.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.customers-new', {
                url: '/customers/new',
                title: 'Add Customer',
                controller: 'AddCustomersController',
                controllerAs: 'vm',
                templateUrl: basepath('customers-new.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.customers-edit', {
                url: '/customers/:id/edit',
                title: 'Add Customer',
                controller: 'AddCustomersController',
                controllerAs: 'vm',
                templateUrl: basepath('customers-new.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.customers-view', {
                url: '/customers/:id/view',
                title: 'Add Customer',
                controller: 'AddCustomersController',
                controllerAs: 'vm',
                templateUrl: basepath('customers-new.html')
            })

            .state('app.cities', {
                url: '/cities',
                title: 'Cities',
                controller: 'CitiesController',
                controllerAs: 'vm',
                templateUrl: basepath('cities.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.cities-new', {
                url: '/cities/new',
                title: 'City form',
                controller: 'CityFormController',
                controllerAs: 'vm',
                templateUrl: basepath('cities-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.cities-edit', {
                url: '/cities/:id/edit',
                title: 'City form',
                controller: 'CityFormController',
                controllerAs: 'vm',
                templateUrl: basepath('cities-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.cities-view', {
                url: '/cities/:id/view',
                title: 'City form',
                controller: 'CityFormController',
                controllerAs: 'vm',
                templateUrl: basepath('cities-form.html')
            })

            .state('app.states', {
                url: '/states',
                title: 'states',
                controller: 'StatesController',
                controllerAs: 'vm',
                templateUrl: basepath('states.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.states-new', {
                url: '/states/new',
                title: 'State form',
                controller: 'StateFormController',
                controllerAs: 'vm',
                templateUrl: basepath('state-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.states-edit', {
                url: '/states/:id/edit',
                title: 'State form',
                controller: 'StateFormController',
                controllerAs: 'vm',
                templateUrl: basepath('state-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.states-view', {
                url: '/states/:id/view',
                title: 'State form',
                controller: 'StateFormController',
                controllerAs: 'vm',
                templateUrl: basepath('state-form.html')
            })

            .state('app.stationCategories', {
                url: '/stationCategories',
                title: 'stationCategories',
                controller: 'StationCategoriesController',
                controllerAs: 'vm',
                templateUrl: basepath('station-categories.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.stationCategories-new', {
                url: '/stationCategories/new',
                title: 'StationCategory form',
                controller: 'StationCategoryFormController',
                controllerAs: 'vm',
                templateUrl: basepath('station-category-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.stationCategories-edit', {
                url: '/stationCategories/:id/edit',
                title: 'StationCategory form',
                controller: 'StationCategoryFormController',
                controllerAs: 'vm',
                templateUrl: basepath('station-category-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley')
            })

            .state('app.stationCategories-view', {
                url: '/stationCategories/:id/view',
                title: 'StationCategory form',
                controller: 'StationCategoryFormController',
                controllerAs: 'vm',
                templateUrl: basepath('station-category-form.html')
            })

            .state('app.stations', {
                url: '/stations',
                title: 'stations',
                controller: 'StationsController',
                controllerAs: 'vm',
                templateUrl: basepath('stations.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.stations-new', {
                url: '/stations/new',
                title: 'Station form',
                controller: 'StationFormController',
                controllerAs: 'vm',
                templateUrl: basepath('station-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley', 'angularFileUpload')
            })

            .state('app.stations-edit', {
                url: '/stations/:id/edit',
                title: 'Station form',
                controller: 'StationFormController',
                controllerAs: 'vm',
                templateUrl: basepath('station-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley', 'angularFileUpload')
            })

            .state('app.stations-view', {
                url: '/stations/:id/view',
                title: 'Station form',
                controller: 'StationFormController',
                controllerAs: 'vm',
                templateUrl: basepath('station-form.html'),
                resolve: resolveFor('angularFileUpload')
            })

            .state('app.trains', {
                url: '/trains',
                title: 'trains',
                controller: 'TrainsController',
                controllerAs: 'vm',
                templateUrl: basepath('trains.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.trains-new', {
                url: '/trains/new',
                title: 'Train form',
                controller: 'TrainFormController',
                controllerAs: 'vm',
                templateUrl: basepath('train-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley', 'angularFileUpload')
            })

            .state('app.trains-edit', {
                url: '/trains/:id/edit',
                title: 'Train form',
                controller: 'TrainFormController',
                controllerAs: 'vm',
                templateUrl: basepath('train-form.html'),
                resolve: resolveFor('angularSpinner', 'parsley', 'angularFileUpload')
            })

            .state('app.trains-view', {
                url: '/trains/:id/view',
                title: 'Train form',
                controller: 'TrainFormController',
                controllerAs: 'vm',
                templateUrl: basepath('train-form.html'),
                resolve: resolveFor('angularFileUpload')
            })

            .state('app.pnrs', {
                url: '/pnrs',
                title: 'pnrs',
                controller: 'PNRsController',
                controllerAs: 'vm',
                templateUrl: basepath('pnrs.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.pnrs-new', {
                url: '/pnrs/new',
                title: 'PNR form',
                controller: 'PNRFormController',
                controllerAs: 'vm',
                templateUrl: basepath('pnr-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.pnrs-edit', {
                url: '/pnrs/:id/edit',
                title: 'PNR form',
                controller: 'PNRFormController',
                controllerAs: 'vm',
                templateUrl: basepath('pnr-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.pnrs-view', {
                url: '/pnrs/:id/view',
                title: 'PNR form',
                controller: 'PNRFormController',
                controllerAs: 'vm',
                templateUrl: basepath('pnr-form.html')
            })

            .state('app.trips', {
                url: '/trips',
                title: 'trips',
                controller: 'TripsController',
                controllerAs: 'vm',
                templateUrl: basepath('trips.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.trips-new', {
                url: '/trips/new',
                title: 'Trip form',
                controller: 'TripFormController',
                controllerAs: 'vm',
                templateUrl: basepath('trip-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.trips-edit', {
                url: '/trips/:id/edit',
                title: 'Trip form',
                controller: 'TripFormController',
                controllerAs: 'vm',
                templateUrl: basepath('trip-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.trips-view', {
                url: '/trips/:id/view',
                title: 'Trip form',
                controller: 'TripFormController',
                controllerAs: 'vm',
                templateUrl: basepath('trip-form.html')
            })

            .state('app.alarms', {
                url: '/alarms',
                title: 'alarms',
                controller: 'AlarmsController',
                controllerAs: 'vm',
                templateUrl: basepath('alarms.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularSpinner')
            })

            .state('app.trip-alarms', {
                url: '/trip/{tripId}/alarms',
                title: 'alarms',
                controller: 'AlarmsController',
                controllerAs: 'vm',
                templateUrl: basepath('alarms.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.alarms-new', {
                url: '/alarms/new',
                title: 'Alarm form',
                controller: 'AlarmFormController',
                controllerAs: 'vm',
                templateUrl: basepath('alarm-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.alarms-edit', {
                url: '/alarms/:id/edit',
                title: 'Alarm form',
                controller: 'AlarmFormController',
                controllerAs: 'vm',
                templateUrl: basepath('alarm-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.alarms-view', {
                url: '/alarms/:id/view',
                title: 'Alarm form',
                controller: 'AlarmFormController',
                controllerAs: 'vm',
                templateUrl: basepath('alarm-form.html')
            })

            .state('app.trainRatings', {
                url: '/trainRates',
                title: 'trainRatings',
                controller: 'TrainRatingsController',
                controllerAs: 'vm',
                templateUrl: basepath('train-raitings.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.trainRatings-new', {
                url: '/trainRatings/new',
                title: 'TrainRating form',
                controller: 'TrainRatingFormController',
                controllerAs: 'vm',
                templateUrl: basepath('train-raiting-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.trainRatings-edit', {
                url: '/trainRatings/:id/edit',
                title: 'TrainRating form',
                templateUrl: basepath('train-raiting-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.trainRatings-view', {
                url: '/trainRatings/:id/view',
                title: 'TrainRating form',
                controller: 'TrainRatingFormController',
                controllerAs: 'vm',
                templateUrl: basepath('train-raiting-form.html')
            })

            .state('app.cabs', {
                url: '/cabs',
                title: 'cabss',
                controller: 'CabsController',
                controllerAs: 'vm',
                templateUrl: basepath('cabs.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.cabs-new', {
                url: '/cabs/new',
                title: 'Cabs form',
                controller: 'CabsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('cab-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.cabs-edit', {
                url: '/cabs/:id/edit',
                title: 'Cabs form',
                controller: 'CabsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('cab-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.cabs-view', {
                url: '/cabs/:id/view',
                title: 'Cabs form',
                controller: 'CabsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('cab-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.restaurants', {
                url: '/restaurants',
                title: 'restaurants',
                controller: 'RestaurantsController',
                controllerAs: 'vm',
                templateUrl: basepath('restaurants.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.restaurants-items', {
                url: '/restaurant/{restaurantId}/items',
                title: 'restaurant items',
                controller: 'ItemsController',
                controllerAs: 'vm',
                templateUrl: basepath('items.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.restaurants-new', {
                url: '/restaurants/new',
                title: 'Restaurants form',
                controller: 'RestaurantsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('restaurant-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.restaurants-edit', {
                url: '/restaurants/:id/edit',
                title: 'Restaurants form',
                controller: 'RestaurantsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('restaurant-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.restaurants-view', {
                url: '/restaurants/:id/view',
                title: 'Restaurants form',
                controller: 'RestaurantsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('restaurant-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.hotels', {
                url: '/hotels',
                title: 'hotels',
                controller: 'HotelsController',
                controllerAs: 'vm',
                templateUrl: basepath('hotels.html'),
                resolve: resolveFor('datatables', 'datatables-pugins', 'angularFileUpload')
            })

            .state('app.hotels-new', {
                url: '/hotels/new',
                title: 'Hotels form',
                controller: 'HotelsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('hotel-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.hotels-edit', {
                url: '/hotels/:id/edit',
                title: 'Hotels form',
                controller: 'HotelsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('hotel-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.hotels-view', {
                url: '/hotels/:id/view',
                title: 'Hotels form',
                controller: 'HotelsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('hotel-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley', 'angularFileUpload')
            })

            .state('app.items', {
                url: '/items',
                title: 'items',
                controller: 'ItemsController',
                controllerAs: 'vm',
                templateUrl: basepath('items.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.items-new', {
                url: '/items/new',
                title: 'Items form',
                controller: 'ItemsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('item-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.items-edit', {
                url: '/items/:id/edit',
                title: 'Items form',
                controller: 'ItemsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('item-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.items-view', {
                url: '/items/:id/view',
                title: 'Items form',
                controller: 'ItemsFormController',
                controllerAs: 'vm',
                templateUrl: basepath('item-form.html')
            })

            .state('app.categories', {
                url: '/categories',
                title: 'categories',
                controller: 'CategoriesController',
                controllerAs: 'vm',
                templateUrl: basepath('categories.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.categories-new', {
                url: '/categories/new',
                title: 'Categories form',
                controller: 'CategoriesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('category-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.categories-edit', {
                url: '/categories/:id/edit',
                title: 'Categories form',
                controller: 'CategoriesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('category-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.categories-view', {
                url: '/categories/:id/view',
                title: 'Categories form',
                controller: 'CategoriesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('category-form.html')
            })

            .state('app.subCategories', {
                url: '/subCategories',
                title: 'subCategories',
                controller: 'SubCategoriesController',
                controllerAs: 'vm',
                templateUrl: basepath('subcategories.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.subCategories-new', {
                url: '/subCategories/new',
                title: 'SubCategories form',
                controller: 'SubCategoriesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('subcategory-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.subCategories-edit', {
                url: '/subCategories/:id/edit',
                title: 'SubCategories form',
                controller: 'SubCategoriesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('subcategory-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.subCategories-view', {
                url: '/subCategories/:id/view',
                title: 'SubCategories form',
                controller: 'SubCategoriesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('subcategory-form.html')
            })

            .state('app.orders', {
                url: '/orders',
                title: 'orders',
                controller: 'OrdersController',
                controllerAs: 'vm',
                templateUrl: basepath('orders.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.orders-new', {
                url: '/orders/new',
                title: 'Orders form',
                controller: 'OrdersFormController',
                controllerAs: 'vm',
                templateUrl: basepath('order-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.orders-edit', {
                url: '/orders/:id/edit',
                title: 'Orders form',
                controller: 'OrdersFormController',
                controllerAs: 'vm',
                templateUrl: basepath('order-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.orders-view', {
                url: '/orders/:id/view',
                title: 'TrainRoutes form',
                controller: 'TrainRoutesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('order-form.html')
            })

            .state('app.trainRoutes', {
                url: '/trainRoutes',
                title: 'trainRoutes',
                controller: 'TrainRoutesController',
                controllerAs: 'vm',
                templateUrl: basepath('trainRoutes.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            })

            .state('app.trainRoutes-new', {
                url: '/trainRoutes/new',
                title: 'TrainRoutes form',
                controller: 'TrainRoutesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('trainRoute-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.trainRoutes-edit', {
                url: '/trainRoutes/:id/edit',
                title: 'TrainRoutes form',
                controller: 'TrainRoutesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('trainRoute-form.html'),
                resolve: resolveFor('angularSpinner', 'inputmask', 'parsley')
            })

            .state('app.trainRoutes-view', {
                url: '/trainRoutes/:id/view',
                title: 'TrainRoutes form',
                controller: 'TrainRoutesFormController',
                controllerAs: 'vm',
                templateUrl: basepath('trainRoute-form.html')
            })

            .state('app.train-routes', {
                url: '/train/{trainNo}/routes',
                title: 'train routes',
                controller: 'TrainRoutesController',
                controllerAs: 'vm',
                templateUrl: basepath('trainRoutes.html'),
                resolve: resolveFor('datatables', 'datatables-pugins')
            });
        // Set here the base of the relative path
        // for all app views
        function basepath(uri) {
            return 'app/views/' + uri;
        }

        // Generates a resolve object by passing script names
        // previously configured in constant.APP_REQUIRES
        function resolveFor() {
            var _args = arguments;
            return {
                deps: ['$ocLazyLoad', '$q', function ($ocLL, $q) {
                    // Creates a promise chain for each argument
                    var promise = $q.when(1); // empty promise
                    for (var i = 0, len = _args.length; i < len; i++) {
                        promise = andThen(_args[i]);
                    }
                    return promise;

                    // creates promise to chain dynamically
                    function andThen(_arg) {
                        // also support a function that returns a promise
                        if (typeof _arg === 'function')
                            return promise.then(_arg);
                        else
                            return promise.then(function () {
                                // if is a module, pass the name. If not, pass the array
                                var whatToLoad = getRequired(_arg);
                                // simple error check
                                if (!whatToLoad) return $.error('Route resolve: Bad resource name [' + _arg + ']');
                                // finally, return a promise
                                return $ocLL.load(whatToLoad);
                            });
                    }

                    // check and returns required data
                    // analyze module items with the form [name: '', files: []]
                    // and also simple array of script files (for not angular js)
                    function getRequired(name) {
                        if (appRequires.modules)
                            for (var m in appRequires.modules)
                                if (appRequires.modules[m].name && appRequires.modules[m].name === name) {
                                    return appRequires.modules[m];
                                }
                        return appRequires.scripts && appRequires.scripts[name];
                    }

                }]
            };
        }

    }
]).config(['$translateProvider', function ($translateProvider) {

    $translateProvider.useStaticFilesLoader({
        prefix: 'app/i18n/',
        suffix: '.json'
    });
    $translateProvider.preferredLanguage('en');
    $translateProvider.useLocalStorage();

}]).config(['cfpLoadingBarProvider', function (cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeBar = true;
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.latencyThreshold = 500;
    cfpLoadingBarProvider.parentSelector = '.wrapper > section';
}])
    .controller('NullController', function () {
    });

/**=========================================================
 * Module: constants.js
 * Define constants to inject across the application
 =========================================================*/
App
    .constant('APP_COLORS', {
        'primary': '#5d9cec',
        'success': '#27c24c',
        'info': '#23b7e5',
        'warning': '#ff902b',
        'danger': '#f05050',
        'inverse': '#131e26',
        'green': '#37bc9b',
        'pink': '#f532e5',
        'purple': '#7266ba',
        'dark': '#3a3f51',
        'yellow': '#fad732',
        'gray-darker': '#232735',
        'gray-dark': '#3a3f51',
        'gray': '#dde6e9',
        'gray-light': '#e4eaec',
        'gray-lighter': '#edf1f2'
    })
    .constant('APP_MEDIAQUERY', {
        'desktopLG': 1200,
        'desktop': 992,
        'tablet': 768,
        'mobile': 480
    })
    .constant('APP_REQUIRES', {
        // jQuery based and standalone scripts
        scripts: {
            'whirl': ['vendor/whirl/dist/whirl.css'],
            'classyloader': ['vendor/jquery-classyloader/js/jquery.classyloader.min.js'],
            'animo': ['vendor/animo.js/animo.js'],
            'fastclick': ['vendor/fastclick/lib/fastclick.js'],
            'modernizr': ['vendor/modernizr/modernizr.js'],
            'animate': ['vendor/animate.css/animate.min.css'],
            'icons': ['vendor/skycons/skycons.js',
                'vendor/fontawesome/css/font-awesome.min.css',
                'app/vendor/simplelineicons/simple-line-icons.css',
                'vendor/weather-icons/css/weather-icons.min.css'
            ],
            'sparklines': ['app/vendor/sparklines/jquery.sparkline.min.js'],
            'slider': ['vendor/seiyria-bootstrap-slider/dist/bootstrap-slider.min.js',
                'vendor/seiyria-bootstrap-slider/dist/css/bootstrap-slider.min.css'
            ],
            'wysiwyg': ['vendor/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
                'vendor/bootstrap-wysiwyg/external/jquery.hotkeys.js'
            ],
            'slimscroll': ['vendor/slimscroll/jquery.slimscroll.min.js'],
            'screenfull': ['vendor/screenfull/dist/screenfull.min.js'],
            'vector-map': ['vendor/ika.jvectormap/jquery-jvectormap-1.2.2.min.js',
                'vendor/ika.jvectormap/jquery-jvectormap-world-mill-en.js',
                'vendor/ika.jvectormap/jquery-jvectormap-us-mill-en.js',
                'vendor/ika.jvectormap/jquery-jvectormap-1.2.2.css'
            ],
            'loadGoogleMapsJS': ['app/vendor/gmap/load-google-maps.js'],
            'google-map': ['vendor/jQuery-gMap/jquery.gmap.min.js'],
            'flot-chart': ['vendor/flot/jquery.flot.js'],
            'flot-chart-plugins': ['vendor/flot.tooltip/js/jquery.flot.tooltip.min.js',
                'vendor/flot/jquery.flot.resize.js',
                'vendor/flot/jquery.flot.pie.js',
                'vendor/flot/jquery.flot.time.js',
                'vendor/flot/jquery.flot.categories.js',
                'vendor/flot-spline/js/jquery.flot.spline.min.js'
            ],
            'jquery-ui': ['vendor/jquery-ui/jquery-ui.min.js',
                'vendor/jqueryui-touch-punch/jquery.ui.touch-punch.min.js'
            ],
            'moment': ['vendor/moment/min/moment-with-locales.min.js'],
            'inputmask': ['vendor/jquery.inputmask/dist/jquery.inputmask.bundle.min.js'],
            'flatdoc': ['vendor/flatdoc/flatdoc.js'],
            'codemirror': ['vendor/codemirror/lib/codemirror.js',
                'vendor/codemirror/lib/codemirror.css'
            ],
            'codemirror-plugins': ['vendor/codemirror/addon/mode/overlay.js',
                'vendor/codemirror/mode/markdown/markdown.js',
                'vendor/codemirror/mode/xml/xml.js',
                'vendor/codemirror/mode/gfm/gfm.js',
                'vendor/marked/lib/marked.js'
            ],
            'taginput': ['vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.css',
                'vendor/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js'
            ],
            'filestyle': ['vendor/bootstrap-filestyle/src/bootstrap-filestyle.js'],
            'parsley': ['vendor/parsleyjs/dist/parsley.min.js'],
            'datatables': ['vendor/datatables/media/js/jquery.dataTables.min.js',
                'app/vendor/datatable-bootstrap/css/dataTables.bootstrap.css'
            ],
            'datatables-pugins': ['app/vendor/datatable-bootstrap/js/dataTables.bootstrap.js',
                'app/vendor/datatable-bootstrap/js/dataTables.bootstrapPagination.js',
                'vendor/datatables-colvis/js/dataTables.colVis.js',
                'vendor/datatables-colvis/css/dataTables.colVis.css'
            ],
            'fullcalendar': ['vendor/fullcalendar/dist/fullcalendar.min.js',
                'vendor/fullcalendar/dist/fullcalendar.css'
            ],
            'gcal': ['vendor/fullcalendar/dist/gcal.js'],
            'papaparse': ['vendor/papaparse/papaparse.min.js'],
            'file-saver': ['vendor/file-saver/FileSaver.min.js']
        },

        // Angular based script (use the right module name)
        modules: [{
            name: 'angularFileUpload',
            files: [
                'vendor/angular-file-upload/angular-file-upload.js'
            ]
        }, {
            name: 'angularSpinner',
            files: ['vendor/spin.js/spin.js',
                'vendor/angular-spinner/angular-spinner.min.js'
            ]
        }, {
            name: 'toaster',
            files: ['vendor/angularjs-toaster/toaster.js',
                'vendor/angularjs-toaster/toaster.css'
            ]
        }, {
            name: 'localytics.directives',
            files: ['vendor/chosen_v1.2.0/chosen.jquery.min.js',
                'vendor/chosen_v1.2.0/chosen.min.css',
                'vendor/angular-chosen-localytics/chosen.js'
            ]
        }, {
            name: 'ngDialog',
            files: ['vendor/ngDialog/js/ngDialog.min.js',
                'vendor/ngDialog/css/ngDialog.min.css',
                'vendor/ngDialog/css/ngDialog-theme-default.min.css'
            ]
        }, {
            name: 'ngWig',
            files: ['vendor/ngWig/dist/ng-wig.min.js']
        }, {
            name: 'ngTable',
            files: ['vendor/ng-table/ng-table.min.js',
                'vendor/ng-table/ng-table.min.css'
            ]
        }, {
            name: 'ngTableExport',
            files: ['vendor/ng-table-export/ng-table-export.js']
        }]

    });

/**=========================================================
 * Module: anchor.js
 * Disables null anchor behavior
 =========================================================*/

App.directive('href', function() {

  return {
    restrict: 'A',
    compile: function(element, attr) {
        return function(scope, element) {
          if(attr.ngClick || attr.href === '' || attr.href === '#'){
            if( !element.hasClass('dropdown-toggle') )
              element.on('click', function(e){
                e.preventDefault();
                e.stopPropagation();
              });
          }
        };
      }
   };
});
/**=========================================================
 * Module: animate-enabled.js
 * Enable or disables ngAnimate for element with directive
 =========================================================*/

App.directive("animateEnabled", ["$animate", function ($animate) {
  return {
    link: function (scope, element, attrs) {
      scope.$watch(function () {
        return scope.$eval(attrs.animateEnabled, scope);
      }, function (newValue) {
        $animate.enabled(!!newValue, element);
      });
    }
  };
}]);
/**=========================================================
 * Module: classy-loader.js
 * Enable use of classyloader directly from data attributes
 =========================================================*/

App.directive('classyloader', function($timeout) {
  'use strict';

  var $scroller       = $(window),
      inViewFlagClass = 'js-is-in-view'; // a classname to detect when a chart has been triggered after scroll

  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      // run after interpolation  
      $timeout(function(){
  
        var $element = $(element),
            options  = $element.data();
        
        // At lease we need a data-percentage attribute
        if(options) {
          if( options.triggerInView ) {

            $scroller.scroll(function() {
              checkLoaderInVIew($element, options);
            });
            // if the element starts already in view
            checkLoaderInVIew($element, options);
          }
          else
            startLoader($element, options);
        }

      }, 0);

      function checkLoaderInVIew(element, options) {
        var offset = -20;
        if( ! element.hasClass(inViewFlagClass) &&
            $.Utils.isInView(element, {topoffset: offset}) ) {
          startLoader(element, options);
        }
      }
      function startLoader(element, options) {
        element.ClassyLoader(options).addClass(inViewFlagClass);
      }
    }
  };
});

/**=========================================================
 * Module: clear-storage.js
 * Removes a key from the browser storage via element click
 =========================================================*/

App.directive('resetKey',  ['$state','$rootScope', function($state, $rootScope) {
  'use strict';

  return {
    restrict: 'A',
    scope: {
      resetKey: '='
    },
    link: function(scope, element, attrs) {
      
      scope.resetKey = attrs.resetKey;

    },
    controller: function($scope, $element) {
    
      $element.on('click', function (e) {
          e.preventDefault();

          if($scope.resetKey) {
            delete $rootScope.$storage[$scope.resetKey];
            $state.go($state.current, {}, {reload: true});
          }
          else {
            $.error('No storage key specified for reset.');
          }
      });

    }

  };

}]);
/**=========================================================
 * Module: filestyle.js
 * Initializes the fielstyle plugin
 =========================================================*/

App.directive('filestyle', function() {
  return {
    restrict: 'A',
    controller: function($scope, $element) {
      var $elem = $($element);
      $elem.filestyle({
        classInput: $elem.data('classinput')
      });
    }
  };
});

/**=========================================================
 * Module: flatdoc.js
 * Creates the flatdoc markup and initializes the plugin
 =========================================================*/

App.directive('flatdoc', ['$location', function($location) {
  return {
    restrict: "EA",
    template: "<div role='flatdoc'><div role='flatdoc-menu'></div><div role='flatdoc-content'></div></div>",
    link: function(scope, element, attrs) {

      Flatdoc.run({
        fetcher: Flatdoc.file(attrs.src)
      });
      
      var $root = $('html, body');
      $(document).on('flatdoc:ready', function() {
        var docMenu = $('[role="flatdoc-menu"]');
        docMenu.find('a').on('click', function(e) {
          e.preventDefault(); e.stopPropagation();
          
          var $this = $(this);
          
          docMenu.find('a.active').removeClass('active');
          $this.addClass('active');

          $root.animate({
                scrollTop: $(this.getAttribute('href')).offset().top - ($('.topnavbar').height() + 10)
            }, 800);
        });

      });
    }
  };

}]);
/**=========================================================
 * Module: form-wizard.js
 * Handles form wizard plugin and validation
 =========================================================*/

App.directive('formWizard', function($parse){
  'use strict';

  return {
    restrict: 'EA',
    scope: true,
    link: function(scope, element, attribute) {
      var validate = $parse(attribute.validateSteps)(scope),
          wiz = new Wizard(attribute.steps, !!validate, element);
      scope.wizard = wiz.init();

    }
  };

  function Wizard (quantity, validate, element) {
    
    var self = this;
    self.quantity = parseInt(quantity,10);
    self.validate = validate;
    self.element = element;
    
    self.init = function() {
      self.createsteps(self.quantity);
      self.go(1); // always start at fist step
      return self;
    };

    self.go = function(step) {
      
      if ( angular.isDefined(self.steps[step]) ) {

        if(self.validate && step !== 1) {
          var form = $(self.element),
              group = form.children().children('div').get(step - 2);

          if (false === form.parsley().validate( group.id )) {
            return false;
          }
        }

        self.cleanall();
        self.steps[step] = true;
      }
    };

    self.active = function(step) {
      return !!self.steps[step];
    };

    self.cleanall = function() {
      for(var i in self.steps){
        self.steps[i] = false;
      }
    };

    self.createsteps = function(q) {
      self.steps = [];
      for(var i = 1; i <= q; i++) self.steps[i] = false;
    };

  }

});

/**=========================================================
 * Module: fullscreen.js
 * Toggle the fullscreen mode on/off
 =========================================================*/

App.directive('toggleFullscreen', function() {
  'use strict';

  return {
    restrict: 'A',
    link: function(scope, element, attrs) {

      element.on('click', function (e) {
          e.preventDefault();

          if (screenfull.enabled) {
            
            screenfull.toggle();
            
            // Switch icon indicator
            if(screenfull.isFullscreen)
              $(this).children('em').removeClass('fa-expand').addClass('fa-compress');
            else
              $(this).children('em').removeClass('fa-compress').addClass('fa-expand');

          } else {
            $.error('Fullscreen not enabled');
          }

      });
    }
  };

});


/**=========================================================
 * Module: gmap.js
 * Init Google Map plugin
 =========================================================*/

App.directive('gmap', ['$window','gmap', function($window, gmap){
  'use strict';

  // Map Style definition
  // Get more styles from http://snazzymaps.com/style/29/light-monochrome
  // - Just replace and assign to 'MapStyles' the new style array
  var MapStyles = [{featureType:'water',stylers:[{visibility:'on'},{color:'#bdd1f9'}]},{featureType:'all',elementType:'labels.text.fill',stylers:[{color:'#334165'}]},{featureType:'landscape',stylers:[{color:'#e9ebf1'}]},{featureType:'road.highway',elementType:'geometry',stylers:[{color:'#c5c6c6'}]},{featureType:'road.arterial',elementType:'geometry',stylers:[{color:'#fff'}]},{featureType:'road.local',elementType:'geometry',stylers:[{color:'#fff'}]},{featureType:'transit',elementType:'geometry',stylers:[{color:'#d8dbe0'}]},{featureType:'poi',elementType:'geometry',stylers:[{color:'#cfd5e0'}]},{featureType:'administrative',stylers:[{visibility:'on'},{lightness:33}]},{featureType:'poi.park',elementType:'labels',stylers:[{visibility:'on'},{lightness:20}]},{featureType:'road',stylers:[{color:'#d8dbe0',lightness:20}]}];
  
  gmap.setStyle( MapStyles );

  // Center Map marker on resolution change

  $($window).resize(function() {

    gmap.autocenter();

  });

  return {
    restrict: 'A',
    link: function (scope, element) {
      
      gmap.init(element);

    }
  };

}]);

/**=========================================================
 * Module: load-css.js
 * Request and load into the current page a css file
 =========================================================*/

App.directive('loadCss', function() {
  'use strict';

  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.on('click', function (e) {
          if(element.is('a')) e.preventDefault();
          var uri = attrs.loadCss,
              link;

          if(uri) {
            link = createLink(uri);
            if ( !link ) {
              $.error('Error creating stylesheet link element.');
            }
          }
          else {
            $.error('No stylesheet location defined.');
          }

      });

    }
  };

  function createLink(uri) {
    var linkId = 'autoloaded-stylesheet',
        oldLink = $('#'+linkId).attr('id', linkId + '-old');

    $('head').append($('<link/>').attr({
      'id':   linkId,
      'rel':  'stylesheet',
      'href': uri
    }));

    if( oldLink.length ) {
      oldLink.remove();
    }

    return $('#'+linkId);
  }


});
/**=========================================================
 * Module: markdownarea.js
 * Markdown Editor from UIKit adapted for Bootstrap Layout.
 =========================================================*/

App.directive('markdownarea', function() {
  'use strict';
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var area         = $(element),
          Markdownarea = $.fn["markdownarea"],
          options      = $.Utils.options(attrs.markdownarea);
      
      var obj = new Markdownarea(area, $.Utils.options(attrs.markdownarea));

    }
  };
});


// Markdown plugin defintion
// Customized to work with bootstrap 
// classnames
// ----------------------------------- 

(function($, window, document){
    'use strict';

    var Markdownarea = function(element, options){

        var $element = $(element);

        if($element.data("markdownarea")) return;

        this.element = $element;
        this.options = $.extend({}, Markdownarea.defaults, options);

        this.marked     = this.options.marked || marked;
        this.CodeMirror = this.options.CodeMirror || CodeMirror;

        this.marked.setOptions({
          gfm           : true,
          tables        : true,
          breaks        : true,
          pedantic      : true,
          sanitize      : false,
          smartLists    : true,
          smartypants   : false,
          langPrefix    : 'lang-'
        });

        this.init();

        this.element.data("markdownarea", this);
    };

    $.extend(Markdownarea.prototype, {

        init: function(){

            var $this = this, tpl = Markdownarea.template;

            tpl = tpl.replace(/\{\:lblPreview\}/g, this.options.lblPreview);
            tpl = tpl.replace(/\{\:lblCodeview\}/g, this.options.lblCodeview);

            this.markdownarea = $(tpl);
            this.content      = this.markdownarea.find(".uk-markdownarea-content");
            this.toolbar      = this.markdownarea.find(".uk-markdownarea-toolbar");
            this.preview      = this.markdownarea.find(".uk-markdownarea-preview").children().eq(0);
            this.code         = this.markdownarea.find(".uk-markdownarea-code");

            this.element.before(this.markdownarea).appendTo(this.code);

            this.editor = this.CodeMirror.fromTextArea(this.element[0], this.options.codemirror);

            this.editor.markdownarea = this;

            this.editor.on("change", (function(){
                var render = function(){

                    var value   = $this.editor.getValue();

                    $this.currentvalue  = String(value);

                    $this.element.trigger("markdownarea-before", [$this]);

                    $this.applyPlugins();

                    $this.marked($this.currentvalue, function (err, markdown) {

                      if (err) throw err;

                      $this.preview.html(markdown);
                      $this.element.val($this.editor.getValue()).trigger("markdownarea-update", [$this]);
                    });
                };
                render();
                return $.Utils.debounce(render, 150);
            })());

            this.code.find(".CodeMirror").css("height", this.options.height);

            this._buildtoolbar();
            this.fit();

            $(window).on("resize", $.Utils.debounce(function(){
                $this.fit();
            }, 200));


            var previewContainer = $this.preview.parent(),
                codeContent      = this.code.find('.CodeMirror-sizer'),
                codeScroll       = this.code.find('.CodeMirror-scroll').on('scroll',$.Utils.debounce(function() {

                    if($this.markdownarea.attr("data-mode")=="tab") return;

                    // calc position
                    var codeHeight       = codeContent.height()   - codeScroll.height(),
                        previewHeight    = previewContainer[0].scrollHeight - previewContainer.height(),
                        ratio            = previewHeight / codeHeight,
                        previewPostition = codeScroll.scrollTop() * ratio;

                    // apply new scroll
                    previewContainer.scrollTop(previewPostition);
            }, 10));

            this.markdownarea.on("click", ".uk-markdown-button-markdown, .uk-markdown-button-preview", function(e){

                e.preventDefault();

                if($this.markdownarea.attr("data-mode")=="tab") {

                    $this.markdownarea.find(".uk-markdown-button-markdown, .uk-markdown-button-preview").removeClass("uk-active").filter(this).addClass("uk-active");

                    $this.activetab = $(this).hasClass("uk-markdown-button-markdown") ? "code":"preview";
                    $this.markdownarea.attr("data-active-tab", $this.activetab);
                }
            });

            this.preview.parent().css("height", this.code.height());
        },

        applyPlugins: function(){

            var $this   = this,
                plugins = Object.keys(Markdownarea.plugins),
                plgs    = Markdownarea.plugins;

            this.markers = {};

            if(plugins.length) {

                var lines = this.currentvalue.split("\n");

                plugins.forEach(function(name){
                    this.markers[name] = [];
                }, this);

                for(var line=0,max=lines.length;line<max;line++) {

                    (function(line){
                        plugins.forEach(function(name){

                            var i = 0;

                            lines[line] = lines[line].replace(plgs[name].identifier, function(){

                                var replacement =  plgs[name].cb({
                                    "area" : $this,
                                    "found": arguments,
                                    "line" : line,
                                    "pos"  : i++,
                                    "uid"  : [name, line, i, (new Date().getTime())+"RAND"+(Math.ceil(Math.random() *100000))].join('-'),
                                    "replace": function(strwith){
                                        var src   = this.area.editor.getLine(this.line),
                                            start = src.indexOf(this.found[0]),
                                            end   = start + this.found[0].length;

                                        this.area.editor.replaceRange(strwith, {"line": this.line, "ch":start}, {"line": this.line, "ch":end} );
                                    }
                                });

                                return replacement;
                            });
                        });
                    }(line));
                }

                this.currentvalue = lines.join("\n");

            }
        },

        _buildtoolbar: function(){

            if(!(this.options.toolbar && this.options.toolbar.length)) return;

            var $this = this, bar = [];

            this.options.toolbar.forEach(function(cmd){
                if(Markdownarea.commands[cmd]) {

                   var title = Markdownarea.commands[cmd].title ? Markdownarea.commands[cmd].title : cmd;

                   bar.push('<li><a data-markdownarea-cmd="'+cmd+'" title="'+title+'" data-toggle="tooltip">'+Markdownarea.commands[cmd].label+'</a></li>');

                   if(Markdownarea.commands[cmd].shortcut) {
                       $this.registerShortcut(Markdownarea.commands[cmd].shortcut, Markdownarea.commands[cmd].action);
                   }
                }
            });

            this.toolbar.html(bar.join("\n"));

            this.markdownarea.on("click", "a[data-markdownarea-cmd]", function(){
                var cmd = $(this).data("markdownareaCmd");

                if(cmd && Markdownarea.commands[cmd] && (!$this.activetab || $this.activetab=="code" || cmd=="fullscreen")) {
                    Markdownarea.commands[cmd].action.apply($this, [$this.editor]);
                }

            });
        },

        fit: function() {

            var mode = this.options.mode;

            if(mode=="split" && this.markdownarea.width() < this.options.maxsplitsize) {
                mode = "tab";
            }

            if(mode=="tab") {

                if(!this.activetab) {
                    this.activetab = "code";
                    this.markdownarea.attr("data-active-tab", this.activetab);
                }

                this.markdownarea.find(".uk-markdown-button-markdown, .uk-markdown-button-preview").removeClass("uk-active")
                                 .filter(this.activetab=="code" ? '.uk-markdown-button-markdown':'.uk-markdown-button-preview').addClass("uk-active");

            }

            this.editor.refresh();
            this.preview.parent().css("height", this.code.height());

            this.markdownarea.attr("data-mode", mode);
        },

        registerShortcut: function(combination, callback){

            var $this = this;

            combination = $.isArray(combination) ? combination : [combination];

            for(var i=0,max=combination.length;i < max;i++) {
                var map = {};

                map[combination[i]] = function(){
                    callback.apply($this, [$this.editor]);
                };

                $this.editor.addKeyMap(map);
            }
        },

        getMode: function(){
            var pos = this.editor.getDoc().getCursor();

            return this.editor.getTokenAt(pos).state.base.htmlState ? 'html':'markdown';
        }
    });

    //jQuery plugin

    $.fn.markdownarea = function(options){

        return this.each(function(){

            var ele = $(this);

            if(!ele.data("markdownarea")) {
                var obj = new Markdownarea(ele, options);
            }
        });
    };

    var baseReplacer = function(replace, editor){
        var text     = editor.getSelection(),
            markdown = replace.replace('$1', text);

        editor.replaceSelection(markdown, 'end');
    };

    Markdownarea.commands = {
        "fullscreen": {
            "title"  : 'Fullscreen',
            "label"  : '<i class="fa fa-expand"></i>',
            "action" : function(editor){

                editor.markdownarea.markdownarea.toggleClass("uk-markdownarea-fullscreen");

                // dont use uk- to avoid rules declaration
                $('html').toggleClass("markdownarea-fullscreen");
                $('html, body').scrollTop(0);

                var wrap = editor.getWrapperElement();

                if(editor.markdownarea.markdownarea.hasClass("uk-markdownarea-fullscreen")) {

                    editor.state.fullScreenRestore = {scrollTop: window.pageYOffset, scrollLeft: window.pageXOffset, width: wrap.style.width, height: wrap.style.height};
                    wrap.style.width  = "";
                    wrap.style.height = editor.markdownarea.content.height()+"px";
                    document.documentElement.style.overflow = "hidden";

                } else {

                    document.documentElement.style.overflow = "";
                    var info = editor.state.fullScreenRestore;
                    wrap.style.width = info.width; wrap.style.height = info.height;
                    window.scrollTo(info.scrollLeft, info.scrollTop);
                }

                editor.refresh();
                editor.markdownarea.preview.parent().css("height", editor.markdownarea.code.height());
            }
        },

        "bold" : {
            "title"  : "Bold",
            "label"  : '<i class="fa fa-bold"></i>',
            "shortcut": ['Ctrl-B', 'Cmd-B'],
            "action" : function(editor){
                baseReplacer(this.getMode() == 'html' ? "<strong>$1</strong>":"**$1**", editor);
            }
        },
        "italic" : {
            "title"  : "Italic",
            "label"  : '<i class="fa fa-italic"></i>',
            "action" : function(editor){
                baseReplacer(this.getMode() == 'html' ? "<em>$1</em>":"*$1*", editor);
            }
        },
        "strike" : {
            "title"  : "Strikethrough",
            "label"  : '<i class="fa fa-strikethrough"></i>',
            "action" : function(editor){
                baseReplacer(this.getMode() == 'html' ? "<del>$1</del>":"~~$1~~", editor);
            }
        },
        "blockquote" : {
            "title"  : "Blockquote",
            "label"  : '<i class="fa fa-quote-right"></i>',
            "action" : function(editor){
                baseReplacer(this.getMode() == 'html' ? "<blockquote><p>$1</p></blockquote>":"> $1", editor);
            }
        },
        "link" : {
            "title"  : "Link",
            "label"  : '<i class="fa fa-link"></i>',
            "action" : function(editor){
                baseReplacer(this.getMode() == 'html' ? '<a href="http://">$1</a>':"[$1](http://)", editor);
            }
        },
        "picture" : {
            "title"  : "Picture",
            "label"  : '<i class="fa fa-picture-o"></i>',
            "action" : function(editor){
                baseReplacer(this.getMode() == 'html' ? '<img src="http://" alt="$1">':"![$1](http://)", editor);
            }
        },
        "listUl" : {
            "title"  : "Unordered List",
            "label"  : '<i class="fa fa-list-ul"></i>',
            "action" : function(editor){
                if(this.getMode() == 'markdown') baseReplacer("* $1", editor);
            }
        },
        "listOl" : {
            "title"  : "Ordered List",
            "label"  : '<i class="fa fa-list-ol"></i>',
            "action" : function(editor){
                if(this.getMode() == 'markdown') baseReplacer("1. $1", editor);
            }
        }
    };

    Markdownarea.defaults = {
        "mode"         : "split",
        "height"       : 500,
        "maxsplitsize" : 1000,
        "codemirror"   : { mode: 'gfm', tabMode: 'indent', tabindex: "2", lineWrapping: true, dragDrop: false, autoCloseTags: true, matchTags: true },
        "toolbar"      : [ "bold", "italic", "strike", "link", "picture", "blockquote", "listUl", "listOl" ],
        "lblPreview"   : "Preview",
        "lblCodeview"  : "Markdown"
    };

    Markdownarea.template = '<div class="uk-markdownarea uk-clearfix" data-mode="split">' +
                                '<div class="uk-markdownarea-navbar">' +
                                    '<ul class="uk-markdownarea-navbar-nav uk-markdownarea-toolbar"></ul>' +
                                    '<div class="uk-markdownarea-navbar-flip">' +
                                        '<ul class="uk-markdownarea-navbar-nav">' +
                                            '<li class="uk-markdown-button-markdown"><a>{:lblCodeview}</a></li>' +
                                            '<li class="uk-markdown-button-preview"><a>{:lblPreview}</a></li>' +
                                            '<li><a data-markdownarea-cmd="fullscreen" data-toggle="tooltip" title="Zen Mode"><i class="fa fa-expand"></i></a></li>' +
                                        '</ul>' +
                                    '</div>' +
                                '</div>' +
                                '<div class="uk-markdownarea-content">' +
                                    '<div class="uk-markdownarea-code"></div>' +
                                    '<div class="uk-markdownarea-preview"><div></div></div>' +
                                '</div>' +
                            '</div>';

    Markdownarea.plugins   = {};
    Markdownarea.addPlugin = function(name, identifier, callback) {
        Markdownarea.plugins[name] = {"identifier":identifier, "cb":callback};
    };

    $.fn["markdownarea"] = Markdownarea;

    // init code
    $(function() {

        $("textarea[data-uk-markdownarea]").each(function() {
            var area = $(this), obj;

            if (!area.data("markdownarea")) {
                obj = new Markdownarea(area, $.Utils.options(area.attr("data-uk-markdownarea")));
            }
        });
    });

    return Markdownarea;

}(jQuery, window, document));

/**=========================================================
 * Module: masked,js
 * Initializes the masked inputs
 =========================================================*/
'use strict';
App.directive('masked', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {
            var $elem = $($element);

            if ($.fn.inputmask) {
                $elem.inputmask();
            }
        }
    };
});

/**=========================================================
 * Module: navbar-search.js
 * Navbar search toggler * Auto dismiss on ESC key
 =========================================================*/

App.directive('searchOpen', ['navSearch', function(navSearch) {
  'use strict';

  return {
    restrict: 'A',
    controller: function($scope, $element) {
      $element
        .on('click', function (e) { e.stopPropagation(); })
        .on('click', navSearch.toggle);
    }
  };

}]).directive('searchDismiss', ['navSearch', function(navSearch) {
  'use strict';

  var inputSelector = '.navbar-form input[type="text"]';

  return {
    restrict: 'A',
    controller: function($scope, $element) {

      $(inputSelector)
        .on('click', function (e) { e.stopPropagation(); })
        .on('keyup', function(e) {
          if (e.keyCode == 27) // ESC
            navSearch.dismiss();
        });
        
      // click anywhere closes the search
      $(document).on('click', navSearch.dismiss);
      // dismissable options
      $element
        .on('click', function (e) { e.stopPropagation(); })
        .on('click', navSearch.dismiss);
    }
  };

}]);


/**=========================================================
 * Module: notify.js
 * Create a notifications that fade out automatically.
 * Based on Notify addon from UIKit (http://getuikit.com/docs/addons_notify.html)
 =========================================================*/

App.directive('notify', function($window){

  return {
    restrict: 'A',
    controller: function ($scope, $element) {
      
      $element.on('click', function (e) {
        e.preventDefault();
        notifyNow($element);
      });

    }
  };

  function notifyNow(elem) {
    var $element = $(elem),
        message = $element.data('message'),
        options = $element.data('options');

    if(!message)
      $.error('Notify: No message specified');

    $.notify(message, options || {});
  }


});


/**
 * Notify Addon definition as jQuery plugin
 * Adapted version to work with Bootstrap classes
 * More information http://getuikit.com/docs/addons_notify.html
 */

(function($, window, document){

    var containers = {},
        messages   = {},

        notify     =  function(options){

            if ($.type(options) == 'string') {
                options = { message: options };
            }

            if (arguments[1]) {
                options = $.extend(options, $.type(arguments[1]) == 'string' ? {status:arguments[1]} : arguments[1]);
            }

            return (new Message(options)).show();
        },
        closeAll  = function(group, instantly){
            if(group) {
                for(var id in messages) { if(group===messages[id].group) messages[id].close(instantly); }
            } else {
                for(var id in messages) { messages[id].close(instantly); }
            }
        };

    var Message = function(options){

        var $this = this;

        this.options = $.extend({}, Message.defaults, options);

        this.uuid    = "ID"+(new Date().getTime())+"RAND"+(Math.ceil(Math.random() * 100000));
        this.element = $([
            // @geedmo: alert-dismissable enables bs close icon
            '<div class="uk-notify-message alert-dismissable">',
                '<a class="close">&times;</a>',
                '<div>'+this.options.message+'</div>',
            '</div>'

        ].join('')).data("notifyMessage", this);

        // status
        if (this.options.status) {
            this.element.addClass('alert alert-'+this.options.status);
            this.currentstatus = this.options.status;
        }

        this.group = this.options.group;

        messages[this.uuid] = this;

        if(!containers[this.options.pos]) {
            containers[this.options.pos] = $('<div class="uk-notify uk-notify-'+this.options.pos+'"></div>').appendTo('body').on("click", ".uk-notify-message", function(){
                $(this).data("notifyMessage").close();
            });
        }
    };


    $.extend(Message.prototype, {

        uuid: false,
        element: false,
        timout: false,
        currentstatus: "",
        group: false,

        show: function() {

            if (this.element.is(":visible")) return;

            var $this = this;

            containers[this.options.pos].show().prepend(this.element);

            var marginbottom = parseInt(this.element.css("margin-bottom"), 10);

            this.element.css({"opacity":0, "margin-top": -1*this.element.outerHeight(), "margin-bottom":0}).animate({"opacity":1, "margin-top": 0, "margin-bottom":marginbottom}, function(){

                if ($this.options.timeout) {

                    var closefn = function(){ $this.close(); };

                    $this.timeout = setTimeout(closefn, $this.options.timeout);

                    $this.element.hover(
                        function() { clearTimeout($this.timeout); },
                        function() { $this.timeout = setTimeout(closefn, $this.options.timeout);  }
                    );
                }

            });

            return this;
        },

        close: function(instantly) {

            var $this    = this,
                finalize = function(){
                    $this.element.remove();

                    if(!containers[$this.options.pos].children().length) {
                        containers[$this.options.pos].hide();
                    }

                    delete messages[$this.uuid];
                };

            if(this.timeout) clearTimeout(this.timeout);

            if(instantly) {
                finalize();
            } else {
                this.element.animate({"opacity":0, "margin-top": -1* this.element.outerHeight(), "margin-bottom":0}, function(){
                    finalize();
                });
            }
        },

        content: function(html){

            var container = this.element.find(">div");

            if(!html) {
                return container.html();
            }

            container.html(html);

            return this;
        },

        status: function(status) {

            if(!status) {
                return this.currentstatus;
            }

            this.element.removeClass('alert alert-'+this.currentstatus).addClass('alert alert-'+status);

            this.currentstatus = status;

            return this;
        }
    });

    Message.defaults = {
        message: "",
        status: "normal",
        timeout: 5000,
        group: null,
        pos: 'top-center'
    };


    $["notify"]          = notify;
    $["notify"].message  = Message;
    $["notify"].closeAll = closeAll;

    return notify;

}(jQuery, window, document));

/**=========================================================
 * Module: now.js
 * Provides a simple way to display the current time formatted
 =========================================================*/

App.directive("now", ['dateFilter', '$interval', function(dateFilter, $interval){
    return {
      restrict: 'E',
      link: function(scope, element, attrs){
        
        var format = attrs.format;

        function updateTime() {
          var dt = dateFilter(new Date(), format);
          element.text(dt);
        }

        updateTime();
        $interval(updateTime, 1000);
      }
    };
}]);
/**=========================================================
 * Module panel-tools.js
 * Directive tools to control panels. 
 * Allows collapse, refresh and dismiss (remove)
 * Saves panel state in browser storage
 =========================================================*/

App.directive('paneltool', function(){
  var templates = {
    /* jshint multistr: true */
    collapse:"<a href='#' panel-collapse='' data-toggle='tooltip' title='Collapse Panel' ng-click='{{panelId}} = !{{panelId}}' ng-init='{{panelId}}=false'> \
                <em ng-show='{{panelId}}' class='fa fa-plus'></em> \
                <em ng-show='!{{panelId}}' class='fa fa-minus'></em> \
              </a>",
    dismiss: "<a href='#' panel-dismiss='' data-toggle='tooltip' title='Close Panel'>\
               <em class='fa fa-times'></em>\
             </a>",
    refresh: "<a href='#' panel-refresh='' data-toggle='tooltip' data-spinner='{{spinner}}' title='Refresh Panel'>\
               <em class='fa fa-refresh'></em>\
             </a>"
  };
  
  return {
    restrict: 'E',
    template: function( elem, attrs ){
      var temp = '';
      if(attrs.toolCollapse)
        temp += templates.collapse.replace(/{{panelId}}/g, (elem.parent().parent().attr('id')) );
      if(attrs.toolDismiss)
        temp += templates.dismiss;
      if(attrs.toolRefresh)
        temp += templates.refresh.replace(/{{spinner}}/g, attrs.toolRefresh);
      return temp;
    },
    // scope: true,
    // transclude: true,
    link: function (scope, element, attrs) {
      element.addClass('pull-right');
    }
  };
})
/**=========================================================
 * Dismiss panels * [panel-dismiss]
 =========================================================*/
.directive('panelDismiss', function(){
  'use strict';
  return {
    restrict: 'A',
    controller: function ($scope, $element) {
      var removeEvent   = 'panel-remove',
          removedEvent  = 'panel-removed';

      $element.on('click', function () {

        // find the first parent panel
        var parent = $(this).closest('.panel');

        if($.support.animation) {
          parent.animo({animation: 'bounceOut'}, removeElement);
        }
        else removeElement();

        function removeElement() {
          // Trigger the event and finally remove the element
          $.when(parent.trigger(removeEvent, [parent]))
           .done(destroyPanel);
        }

        function destroyPanel() {
          var col = parent.parent();
          parent.remove();
          // remove the parent if it is a row and is empty and not a sortable (portlet)
          col
            .trigger(removedEvent) // An event to catch when the panel has been removed from DOM
            .filter(function() {
            var el = $(this);
            return (el.is('[class*="col-"]:not(.sortable)') && el.children('*').length === 0);
          }).remove();

        }
      });
    }
  };
})
/**=========================================================
 * Collapse panels * [panel-collapse]
 =========================================================*/
.directive('panelCollapse', ['$timeout', function($timeout){
  'use strict';
  
  var storageKeyName = 'panelState',
      storage;
  
  return {
    restrict: 'A',
    // transclude: true,
    controller: function ($scope, $element) {

      // Prepare the panel to be collapsible
      var $elem   = $($element),
          parent  = $elem.closest('.panel'), // find the first parent panel
          panelId = parent.attr('id');

      storage = $scope.$storage;

      // Load the saved state if exists
      var currentState = loadPanelState( panelId );
      if ( typeof currentState !== undefined) {
        $timeout(function(){
            $scope[panelId] = currentState; },
          10);
      }

      // bind events to switch icons
      $element.bind('click', function() {

        savePanelState( panelId, !$scope[panelId] );

      });
    }
  };

  function savePanelState(id, state) {
    if(!id) return false;
    var data = angular.fromJson(storage[storageKeyName]);
    if(!data) { data = {}; }
    data[id] = state;
    storage[storageKeyName] = angular.toJson(data);
  }

  function loadPanelState(id) {
    if(!id) return false;
    var data = angular.fromJson(storage[storageKeyName]);
    if(data) {
      return data[id];
    }
  }

}])
/**=========================================================
 * Refresh panels
 * [panel-refresh] * [data-spinner="standard"]
 =========================================================*/
.directive('panelRefresh', function(){
  'use strict';
  
  return {
    restrict: 'A',
    controller: function ($scope, $element) {
      
      var refreshEvent   = 'panel-refresh',
          whirlClass     = 'whirl',
          defaultSpinner = 'standard';

      // method to clear the spinner when done
      function removeSpinner() {
        this.removeClass(whirlClass);
      }

      // catch clicks to toggle panel refresh
      $element.on('click', function () {
        var $this   = $(this),
            panel   = $this.parents('.panel').eq(0),
            spinner = $this.data('spinner') || defaultSpinner
            ;

        // start showing the spinner
        panel.addClass(whirlClass + ' ' + spinner);

        // attach as public method
        panel.removeSpinner = removeSpinner;

        // Trigger the event and send the panel object
        $this.trigger(refreshEvent, [panel]);

      });

    }
  };
});


  /**
   * This function is only to show a demonstration
   * of how to use the panel refresh system via 
   * custom event. 
   * IMPORTANT: see how to remove the spinner.
   */
(function($, window, document){
  'use strict';

  $(document).on('panel-refresh', '.panel.panel-demo', function(e, panel){
    
    // perform any action when a .panel triggers a the refresh event
    setTimeout(function(){
      // when the action is done, just remove the spinner class
      panel.removeSpinner();
    }, 3000);

  });

}(jQuery, window, document));

/**=========================================================
 * Module: play-animation.js
 * Provides a simple way to run animation with a trigger
 * Requires animo.js
 =========================================================*/
 
App.directive('animate', function($window){

  'use strict';

  var $scroller = $(window).add('body, .wrapper');
  
  return {
    restrict: 'A',
    link: function (scope, elem, attrs) {

      // Parse animations params and attach trigger to scroll
      var $elem     = $(elem),
          offset    = $elem.data('offset'),
          delay     = $elem.data('delay')     || 100, // milliseconds
          animation = $elem.data('play')      || 'bounce';
      
      if(typeof offset !== 'undefined') {
        
        // test if the element starts visible
        testAnimation($elem);
        // test on scroll
        $scroller.scroll(function(){
          testAnimation($elem);
        });

      }

      // Test an element visibilty and trigger the given animation
      function testAnimation(element) {
          if ( !element.hasClass('anim-running') &&
              $.Utils.isInView(element, {topoffset: offset})) {
          element
            .addClass('anim-running');

          setTimeout(function() {
            element
              .addClass('anim-done')
              .animo( { animation: animation, duration: 0.7} );
          }, delay);

        }
      }

      // Run click triggered animations
      $elem.on('click', function() {

        var $elem     = $(this),
            targetSel = $elem.data('target'),
            animation = $elem.data('play') || 'bounce',
            target    = $(targetSel);

        if(target && target) {
          target.animo( { animation: animation } );
        }
        
      });
    }
  };

});

/**=========================================================
 * Module: scroll.js
 * Make a content box scrollable
 =========================================================*/

App.directive('scrollable', function(){
  return {
    restrict: 'EA',
    link: function(scope, elem, attrs) {
      var defaultHeight = 250;
      elem.slimScroll({
          height: (attrs.height || defaultHeight)
      });
    }
  };
});
/**=========================================================
 * Module: sidebar.js
 * Wraps the sidebar and handles collapsed state
 =========================================================*/

App.directive('sidebar', ['$window', 'APP_MEDIAQUERY', function($window, mq) {
  
  var $win  = $($window);
  var $html = $('html');
  var $body = $('body');
  var $scope;
  var $sidebar;

  return {
    restrict: 'EA',
    template: '<nav class="sidebar" ng-transclude></nav>',
    transclude: true,
    replace: true,
    link: function(scope, element, attrs) {
      
      $scope   = scope;
      $sidebar = element;

      var eventName = isTouch() ? 'click' : 'mouseenter' ;
      $sidebar.on( eventName, '.nav > li', function() {
        if( isSidebarCollapsed() && !isMobile() )
          toggleMenuItem( $(this) );
      });

      scope.$on('closeSidebarMenu', function() {
        removeFloatingNav();
        $('.sidebar li.open').removeClass('open');
      });
    }
  };


  // Open the collapse sidebar submenu items when on touch devices 
  // - desktop only opens on hover
  function toggleTouchItem($element){
    $element
      .siblings('li')
      .removeClass('open')
      .end()
      .toggleClass('open');
  }

  // Handles hover to open items under collapsed menu
  // ----------------------------------- 
  function toggleMenuItem($listItem) {

    removeFloatingNav();

    var ul = $listItem.children('ul');
    
    if( !ul.length ) return;
    if( $listItem.hasClass('open') ) {
      toggleTouchItem($listItem);
      return;
    }

    var $aside = $('.aside');
    var mar =  $scope.app.layout.isFixed ?  parseInt( $aside.css('margin-top'), 0) : 0;

    var subNav = ul.clone().appendTo( $aside );
    
    toggleTouchItem($listItem);

    var itemTop = ($listItem.position().top + mar) - $sidebar.scrollTop();
    var vwHeight = $win.height();

    subNav
      .addClass('nav-floating')
      .css({
        position: $scope.app.layout.isFixed ? 'fixed' : 'absolute',
        top:      itemTop,
        bottom:   (subNav.outerHeight(true) + itemTop > vwHeight) ? 0 : 'auto'
      });

    subNav.on('mouseleave', function() {
      toggleTouchItem($listItem);
      subNav.remove();
    });

  }

  function removeFloatingNav() {
    $('.sidebar-subnav.nav-floating').remove();
  }

  function isTouch() {
    return $html.hasClass('touch');
  }
  function isSidebarCollapsed() {
    return $body.hasClass('aside-collapsed');
  }
  function isSidebarToggled() {
    return $body.hasClass('aside-toggled');
  }
  function isMobile() {
    return $win.width() < mq.tablet;
  }
}]);
/**=========================================================
 * Module: skycons.js
 * Include any animated weather icon from Skycons
 =========================================================*/

App.directive('skycon', function(){

  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      
      var skycons = new Skycons({'color': (attrs.color || 'white')});

      element.html('<canvas width="' + attrs.width + '" height="' + attrs.height + '"></canvas>');

      skycons.add(element.children()[0], attrs.skycon);

      skycons.play();

    }
  };
});
/**=========================================================
 * Module: sparkline.js
 * SparkLines Mini Charts
 =========================================================*/
 
App.directive('sparkline', ['$timeout', '$window', function($timeout, $window){

  'use strict';

  return {
    restrict: 'EA',
    controller: function ($scope, $element) {
      var runSL = function(){
        initSparLine($element);
      };

      $timeout(runSL);
    }
  };

  function initSparLine($element) {
    var options = $element.data();

    options.type = options.type || 'bar'; // default chart is bar
    options.disableHiddenCheck = true;

    $element.sparkline('html', options);

    if(options.resize) {
      $(window).resize(function(){
        $element.sparkline('html', options);
      });
    }
  }

}]);

/**=========================================================
 * Module: table-checkall.js
 * Tables check all checkbox
 =========================================================*/

App.directive('checkAll', function() {
  'use strict';
  
  return {
    restrict: 'A',
    controller: function($scope, $element){
      
      $element.on('change', function() {
        var $this = $(this),
            index= $this.index() + 1,
            checkbox = $this.find('input[type="checkbox"]'),
            table = $this.parents('table');
        // Make sure to affect only the correct checkbox column
        table.find('tbody > tr > td:nth-child('+index+') input[type="checkbox"]')
          .prop('checked', checkbox[0].checked);

      });
    }
  };

});
/**=========================================================
 * Module: tags-input.js
 * Initializes the tag inputs plugin
 =========================================================*/

App.directive('tagsinput', function($timeout) {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {

      element.on('itemAdded itemRemoved', function(){
        // check if view value is not empty and is a string
        // and update the view from string to an array of tags
        if(ngModel.$viewValue && ngModel.$viewValue.split) {
          ngModel.$setViewValue( ngModel.$viewValue.split(',') );
          ngModel.$render();
        }
      });

      $timeout(function(){
        element.tagsinput();
      });

    }
  };
});

/**=========================================================
 * Module: toggle-state.js
 * Toggle a classname from the BODY Useful to change a state that 
 * affects globally the entire layout or more than one item 
 * Targeted elements must have [toggle-state="CLASS-NAME-TO-TOGGLE"]
 * User no-persist to avoid saving the sate in browser storage
 =========================================================*/

App.directive('toggleState', ['toggleStateService', function(toggle) {
  'use strict';
  
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {

      var $body = $('body');

      $(element)
        .on('click', function (e) {
          e.preventDefault();
          var classname = attrs.toggleState;
          
          if(classname) {
            if( $body.hasClass(classname) ) {
              $body.removeClass(classname);
              if( ! attrs.noPersist)
                toggle.removeState(classname);
            }
            else {
              $body.addClass(classname);
              if( ! attrs.noPersist)
                toggle.addState(classname);
            }
            
          }

      });
    }
  };
  
}]);

/**=========================================================
 * Module: masked,js
 * Initializes the jQuery UI slider controls
 =========================================================*/

App.directive('uiSlider', function() {
  return {
    restrict: 'A',
    controller: function($scope, $element) {
      var $elem = $($element);
      if($.fn.slider)
        $elem.slider();
    }
  };
});

/**=========================================================
 * Module: validate-form.js
 * Initializes the validation plugin Parsley
 =========================================================*/
'use strict';
App.directive('validateForm', function() {
    return {
        restrict: 'A',
        controller: function($scope, $element) {
            var $elem = $($element);
            if ($.fn.parsley) {
                $elem.parsley();
            }
        }
    };
});

/**=========================================================
 * Module: vector-map.js.js
 * Init jQuery Vector Map plugin
 =========================================================*/

App.directive('vectorMap', ['vectorMap', function(vectorMap){
  'use strict';

  var defaultColors = {
      markerColor:  '#23b7e5',      // the marker points
      bgColor:      'transparent',      // the background
      scaleColors:  ['#878c9a'],    // the color of the region in the serie
      regionFill:   '#bbbec6'       // the base region color
  };

  return {
    restrict: 'EA',
    link: function(scope, element, attrs) {

      var mapHeight   = attrs.height || '300',
          options     = {
            markerColor:  attrs.markerColor  || defaultColors.markerColor,
            bgColor:      attrs.bgColor      || defaultColors.bgColor,
            scale:        attrs.scale        || 1,
            scaleColors:  attrs.scaleColors  || defaultColors.scaleColors,
            regionFill:   attrs.regionFill   || defaultColors.regionFill,
            mapName:      attrs.mapName      || 'world_mill_en'
          };
      
      element.css('height', mapHeight);
      
      vectorMap.init( element , options, scope.seriesData, scope.markersData);

    }
  };

}]);
App.service('browser', function(){
  "use strict";

  var matched, browser;

  var uaMatch = function( ua ) {
    ua = ua.toLowerCase();

    var match = /(opr)[\/]([\w.]+)/.exec( ua ) ||
      /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
      /(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec( ua ) ||
      /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
      /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
      /(msie) ([\w.]+)/.exec( ua ) ||
      ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec( ua ) ||
      ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
      [];

    var platform_match = /(ipad)/.exec( ua ) ||
      /(iphone)/.exec( ua ) ||
      /(android)/.exec( ua ) ||
      /(windows phone)/.exec( ua ) ||
      /(win)/.exec( ua ) ||
      /(mac)/.exec( ua ) ||
      /(linux)/.exec( ua ) ||
      /(cros)/i.exec( ua ) ||
      [];

    return {
      browser: match[ 3 ] || match[ 1 ] || "",
      version: match[ 2 ] || "0",
      platform: platform_match[ 0 ] || ""
    };
  };

  matched = uaMatch( window.navigator.userAgent );
  browser = {};

  if ( matched.browser ) {
    browser[ matched.browser ] = true;
    browser.version = matched.version;
    browser.versionNumber = parseInt(matched.version);
  }

  if ( matched.platform ) {
    browser[ matched.platform ] = true;
  }

  // These are all considered mobile platforms, meaning they run a mobile browser
  if ( browser.android || browser.ipad || browser.iphone || browser[ "windows phone" ] ) {
    browser.mobile = true;
  }

  // These are all considered desktop platforms, meaning they run a desktop browser
  if ( browser.cros || browser.mac || browser.linux || browser.win ) {
    browser.desktop = true;
  }

  // Chrome, Opera 15+ and Safari are webkit based browsers
  if ( browser.chrome || browser.opr || browser.safari ) {
    browser.webkit = true;
  }

  // IE11 has a new token so we will assign it msie to avoid breaking changes
  if ( browser.rv )
  {
    var ie = "msie";

    matched.browser = ie;
    browser[ie] = true;
  }

  // Opera 15+ are identified as opr
  if ( browser.opr )
  {
    var opera = "opera";

    matched.browser = opera;
    browser[opera] = true;
  }

  // Stock Android browsers are marked as Safari on Android.
  if ( browser.safari && browser.android )
  {
    var android = "android";

    matched.browser = android;
    browser[android] = true;
  }

  // Assign the name and platform variable
  browser.name = matched.browser;
  browser.platform = matched.platform;


  return browser;

});
/**=========================================================
 * Module: colors.js
 * Services to retrieve global colors
 =========================================================*/
 
App.factory('colors', ['APP_COLORS', function(colors) {
  
  return {
    byName: function(name) {
      return (colors[name] || '#fff');
    }
  };

}]);

/**=========================================================
 * Module: google-map.js
 * Services to share gmap functions
 =========================================================*/

App.service('gmap', function() {

  return {
    setStyle: function(style) {
      this.MapStyles = style;
    },
    autocenter: function() {
      var refs = this.gMapRefs;
      if(refs && refs.length) {
        for( var r in refs) {
          var mapRef = refs[r];
          var currMapCenter = mapRef.getCenter();
          if(mapRef && currMapCenter) {
              google.maps.event.trigger(mapRef, 'resize');
              mapRef.setCenter(currMapCenter);
          }
        }
      }
    },
    init: function (element) { //initGmap

      var self      = this,
          $element  = $(element),
          addresses = $element.data('address') && $element.data('address').split(';'),
          titles    = $element.data('title') && $element.data('title').split(';'),
          zoom      = $element.data('zoom') || 14,
          maptype   = $element.data('maptype') || 'ROADMAP', // or 'TERRAIN'
          markers   = [];

      if(addresses) {
        for(var a in addresses)  {
            if(typeof addresses[a] == 'string') {
                markers.push({
                    address:  addresses[a],
                    html:     (titles && titles[a]) || '',
                    popup:    true   /* Always popup */
                  });
            }
        }

        var options = {
            controls: {
                   panControl:         true,
                   zoomControl:        true,
                   mapTypeControl:     true,
                   scaleControl:       true,
                   streetViewControl:  true,
                   overviewMapControl: true
               },
            scrollwheel: false,
            maptype: maptype,
            markers: markers,
            zoom: zoom
            // More options https://github.com/marioestrada/jQuery-gMap
        };

        var gMap = $element.gMap(options);

        var ref = gMap.data('gMap.reference');
        // save in the map references list
        if( ! self.gMapRefs )
          self.gMapRefs = [];
        self.gMapRefs.push(ref);

        // set the styles
        if($element.data('styled') !== undefined) {
          
          ref.setOptions({
            styles: self.MapStyles
          });

        }
      }
    }
  };
});
/**=========================================================
 * Module: nav-search.js
 * Services to share navbar search functions
 =========================================================*/
 
App.service('navSearch', function() {
  var navbarFormSelector = 'form.navbar-form';
  return {
    toggle: function() {
      
      var navbarForm = $(navbarFormSelector);

      navbarForm.toggleClass('open');
      
      var isOpen = navbarForm.hasClass('open');
      
      navbarForm.find('input')[isOpen ? 'focus' : 'blur']();

    },

    dismiss: function() {
      $(navbarFormSelector)
        .removeClass('open')      // Close control
        .find('input[type="text"]').blur() // remove focus
        .val('')                    // Empty input
        ;
    }
  };

});
/**=========================================================
 * Module: toggle-state.js
 * Services to share toggle state functionality
 =========================================================*/

App.service('toggleStateService', ['$rootScope', function($rootScope) {

  var storageKeyName  = 'toggleState';

  // Helper object to check for words in a phrase //
  var WordChecker = {
    hasWord: function (phrase, word) {
      return new RegExp('(^|\\s)' + word + '(\\s|$)').test(phrase);
    },
    addWord: function (phrase, word) {
      if (!this.hasWord(phrase, word)) {
        return (phrase + (phrase ? ' ' : '') + word);
      }
    },
    removeWord: function (phrase, word) {
      if (this.hasWord(phrase, word)) {
        return phrase.replace(new RegExp('(^|\\s)*' + word + '(\\s|$)*', 'g'), '');
      }
    }
  };

  // Return service public methods
  return {
    // Add a state to the browser storage to be restored later
    addState: function(classname){
      var data = angular.fromJson($rootScope.$storage[storageKeyName]);
      
      if(!data)  {
        data = classname;
      }
      else {
        data = WordChecker.addWord(data, classname);
      }

      $rootScope.$storage[storageKeyName] = angular.toJson(data);
    },

    // Remove a state from the browser storage
    removeState: function(classname){
      var data = $rootScope.$storage[storageKeyName];
      // nothing to remove
      if(!data) return;

      data = WordChecker.removeWord(data, classname);

      $rootScope.$storage[storageKeyName] = angular.toJson(data);
    },
    
    // Load the state string and restore the classlist
    restoreState: function($elem) {
      var data = angular.fromJson($rootScope.$storage[storageKeyName]);
      
      // nothing to restore
      if(!data) return;
      $elem.addClass(data);
    }

  };

}]);
/**=========================================================
 * Module: vector-map.js
 * Services to initialize vector map plugin
 =========================================================*/

App.service('vectorMap', function() {
  'use strict';
  return {
    init: function($element, opts, series, markers) {
          $element.vectorMap({
            map:             opts.mapName,
            backgroundColor: opts.bgColor,
            zoomMin:         2,
            zoomMax:         8,
            zoomOnScroll:    false,
            regionStyle: {
              initial: {
                'fill':           opts.regionFill,
                'fill-opacity':   1,
                'stroke':         'none',
                'stroke-width':   1.5,
                'stroke-opacity': 1
              },
              hover: {
                'fill-opacity': 0.8
              },
              selected: {
                fill: 'blue'
              },
              selectedHover: {
              }
            },
            focusOn:{ x:0.4, y:0.6, scale: opts.scale},
            markerStyle: {
              initial: {
                fill: opts.markerColor,
                stroke: opts.markerColor
              }
            },
            onRegionLabelShow: function(e, el, code) {
              if ( series && series[code] )
                el.html(el.html() + ': ' + series[code] + ' visitors');
            },
            markers: markers,
            series: {
                regions: [{
                    values: series,
                    scale: opts.scaleColors,
                    normalizeFunction: 'polynomial'
                }]
            },
          });
        }
  };
});
// To run this code, edit file
// index.html or index.jade and change
// html data-ng-app attribute from
// angle to qeApp
// -----------------------------------

'use strict';
angular.module('qeApp', ['angle', 'lbServices', 'ngAnimate'])
    .run(runQuoteApp);

runQuoteApp.$inject = ['$log', '$rootScope', '$state', 'LoopBackAuth'];

function runQuoteApp($log, $rootScope, $state, LoopBackAuth) {
    var navigatingToLogin = false;
    console.log('quote engine app running');
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (!LoopBackAuth.accessTokenId && !navigatingToLogin) {
            //console.log('navigating login');
            navigatingToLogin = true;
            event.preventDefault();
            $state.go('page.login');
        }
    });
}

(function() {
    'use strict';

    angular.module('qeApp').config(configure);

    configure.$inject = ['$httpProvider'];

    function configure($httpProvider) {

        //$httpProvider.interceptors.push(function($q, $location) {
        //    return {
        //        responseError: function(rejection) {
        //            if (rejection.status == 401 && $location.path().indexOf('login') == -1 && $location.path().indexOf('register') == -1) {
        //                debugger;
        //                $location.nextAfterLogin = $location.path();
        //                $location.path('/page/login');
        //            }
        //            return $q.reject(rejection);
        //        function runQuoteApp($log, $rootScope, $state, L
        //    };
        //});
    }

})();

'use strict';

angular.module('qeApp')
    .constant('states', [{
        'name': '',
        'abbreviation': ''
    }, {
        'name': 'Alabama',
        'abbreviation': 'AL'
    }, {
        'name': 'Alaska',
        'abbreviation': 'AK'
    }, {
        'name': 'American Samoa',
        'abbreviation': 'AS'
    }, {
        'name': 'Arizona',
        'abbreviation': 'AZ'
    }, {
        'name': 'Arkansas',
        'abbreviation': 'AR'
    }, {
        'name': 'California',
        'abbreviation': 'CA'
    }, {
        'name': 'Colorado',
        'abbreviation': 'CO'
    }, {
        'name': 'Connecticut',
        'abbreviation': 'CT'
    }, {
        'name': 'Delaware',
        'abbreviation': 'DE'
    }, {
        'name': 'District Of Columbia',
        'abbreviation': 'DC'
    }, {
        'name': 'Federated States Of Micronesia',
        'abbreviation': 'FM'
    }, {
        'name': 'Florida',
        'abbreviation': 'FL'
    }, {
        'name': 'Georgia',
        'abbreviation': 'GA'
    }, {
        'name': 'Guam',
        'abbreviation': 'GU'
    }, {
        'name': 'Hawaii',
        'abbreviation': 'HI'
    }, {
        'name': 'Idaho',
        'abbreviation': 'ID'
    }, {
        'name': 'Illinois',
        'abbreviation': 'IL'
    }, {
        'name': 'Indiana',
        'abbreviation': 'IN'
    }, {
        'name': 'Iowa',
        'abbreviation': 'IA'
    }, {
        'name': 'Kansas',
        'abbreviation': 'KS'
    }, {
        'name': 'Kentucky',
        'abbreviation': 'KY'
    }, {
        'name': 'Louisiana',
        'abbreviation': 'LA'
    }, {
        'name': 'Maine',
        'abbreviation': 'ME'
    }, {
        'name': 'Marshall Islands',
        'abbreviation': 'MH'
    }, {
        'name': 'Maryland',
        'abbreviation': 'MD'
    }, {
        'name': 'Massachusetts',
        'abbreviation': 'MA'
    }, {
        'name': 'Michigan',
        'abbreviation': 'MI'
    }, {
        'name': 'Minnesota',
        'abbreviation': 'MN'
    }, {
        'name': 'Mississippi',
        'abbreviation': 'MS'
    }, {
        'name': 'Missouri',
        'abbreviation': 'MO'
    }, {
        'name': 'Montana',
        'abbreviation': 'MT'
    }, {
        'name': 'Nebraska',
        'abbreviation': 'NE'
    }, {
        'name': 'Nevada',
        'abbreviation': 'NV'
    }, {
        'name': 'New Hampshire',
        'abbreviation': 'NH'
    }, {
        'name': 'New Jersey',
        'abbreviation': 'NJ'
    }, {
        'name': 'New Mexico',
        'abbreviation': 'NM'
    }, {
        'name': 'New York',
        'abbreviation': 'NY'
    }, {
        'name': 'North Carolina',
        'abbreviation': 'NC'
    }, {
        'name': 'North Dakota',
        'abbreviation': 'ND'
    }, {
        'name': 'Northern Mariana Islands',
        'abbreviation': 'MP'
    }, {
        'name': 'Ohio',
        'abbreviation': 'OH'
    }, {
        'name': 'Oklahoma',
        'abbreviation': 'OK'
    }, {
        'name': 'Oregon',
        'abbreviation': 'OR'
    }, {
        'name': 'Palau',
        'abbreviation': 'PW'
    }, {
        'name': 'Pennsylvania',
        'abbreviation': 'PA'
    }, {
        'name': 'Puerto Rico',
        'abbreviation': 'PR'
    }, {
        'name': 'Rhode Island',
        'abbreviation': 'RI'
    }, {
        'name': 'South Carolina',
        'abbreviation': 'SC'
    }, {
        'name': 'South Dakota',
        'abbreviation': 'SD'
    }, {
        'name': 'Tennessee',
        'abbreviation': 'TN'
    }, {
        'name': 'Texas',
        'abbreviation': 'TX'
    }, {
        'name': 'Utah',
        'abbreviation': 'UT'
    }, {
        'name': 'Vermont',
        'abbreviation': 'VT'
    }, {
        'name': 'Virgin Islands',
        'abbreviation': 'VI'
    }, {
        'name': 'Virginia',
        'abbreviation': 'VA'
    }, {
        'name': 'Washington',
        'abbreviation': 'WA'
    }, {
        'name': 'West Virginia',
        'abbreviation': 'WV'
    }, {
        'name': 'Wisconsin',
        'abbreviation': 'WI'
    }, {
        'name': 'Wyoming',
        'abbreviation': 'WY'
    }])
    .constant('months', [{
        'name': 'January',
        'value': 1
    }, {
        'name': 'February',
        'value': 2
    }, {
        'name': 'March',
        'value': 3
    }, {
        'name': 'April',
        'value': 4
    }, {
        'name': 'May',
        'value': 5
    }, {
        'name': 'June',
        'value': 6
    }, {
        'name': 'July',
        'value': 7
    }, {
        'name': 'August',
        'value': 8
    }, {
        'name': 'September',
        'value': 9
    }, {
        'name': 'October',
        'value': 10
    }, {
        'name': 'November',
        'value': 11
    }, {
        'name': 'December',
        'value': 12
    }])
    .constant('InsnginLeadFields', [{
        displayName: '',
        dbName: ''
    }, {
        displayName: "First Name",
        dbName: "firstname",
        required: true
    }, {
        displayName: "Last Name",
        dbName: "lastname",
        required: true
    }, {
        displayName: "Date of birth",
        dbName: "dob"
    }, {
        displayName: "Address",
        dbName: "address1",
        required: true
    }, {
        displayName: "City",
        dbName: "city",
        required: true
    }, {
        displayName: "State",
        dbName: "state",
        required: true
    }, {
        displayName: "Zip Code",
        dbName: "zipcode",
        required: true
    }, {
        displayName: "Area Code",
        dbName: "phonenumberareacode"
    }, {
        displayName: "10-Digit Phone Number",
        dbName: "10digitPhoneNumber"
    }, {
        displayName: "7-Digit Phone Number",
        dbName: "phonenumber"
    }, {
        displayName: "Year Home Built",
        dbName: "yearbuilt"
    }, {
        displayName: "Market Value",
        dbName: "marketvalue"
    }, {
        displayName: "Purchase Month",
        dbName: "purchaseDateMM"
    }, {
        displayName: "Purchase Year",
        dbName: "purchaseDateYYYY"
    }]);

$(document).ready(function(){
  console.log('not yet clicked on the checkbox');
    $('#agreeToTerms').on('click', function(){
      $('#agreeToTermsTrue').removeClass('disabled');
  });
});
//angular.module('qeApp')
//    .controller('UploadController', function ($scope, FileUploader) {
//        'use strict';
//
//        // create a uploader with options
//        var uploader = $scope.uploader =new FileUploader({
//            url: '/api/containers/container1/upload',
//            formData: [
//                {key: 'value'}
//            ]
//        });
//
//        // ADDING FILTERS
//
//        //uploader.filters.push(function (item) { // second user filter
//        //  console.info('filter2');
//        //  return true;
//        //});
//        //
//        //// REGISTER HANDLERS
//        //
//        //uploader.bind('afteraddingfile', function (event, item) {
//        //  console.info('After adding a file', item);
//        //});
//        //
//        //uploader.bind('whenaddingfilefailed', function (event, item) {
//        //  console.info('When adding a file failed', item);
//        //});
//        //
//        //uploader.bind('afteraddingall', function (event, items) {
//        //  console.info('After adding all files', items);
//        //});
//        //
//        //uploader.bind('beforeupload', function (event, item) {
//        //  console.info('Before upload', item);
//        //});
//        //
//        //uploader.bind('progress', function (event, item, progress) {
//        //  console.info('Progress: ' + progress, item);
//        //});
//        //
//        uploader.onCompleteAll = function () {
//            debugger;
//            $scope.$broadcast('uploadCompleted');
//        };
//        //
//        //uploader.bind('cancel', function (event, xhr, item) {
//        //  console.info('Cancel', xhr, item);
//        //});
//        //
//        //uploader.bind('error', function (event, xhr, item, response) {
//        //  console.info('Error', xhr, item, response);
//        //});
//        //
//        //uploader.bind('complete', function (event, xhr, item, response) {
//        //  console.info('Complete', xhr, item, response);
//        //});
//        //
//        //uploader.bind('progressall', function (event, progress) {
//        //  console.info('Total progress: ' + progress);
//        //});
//        //
//        //uploader.bind('completeall', function (event, items) {
//        //  console.info('Complete all', items);
//        //});
//
//    }
//).controller('FilesController', function ($scope, $http) {
//
//        $scope.load = function () {
//            debugger;
//            $http.get('/api/containers/container1/files').success(function (data) {
//                console.log(data);
//                $scope.files = data;
//            });
//        };
//
//        $scope.delete = function (index, id) {
//            $http.delete('/api/containers/container1/files/' + encodeURIComponent(id)).success(function (data, status, headers) {
//                $scope.files.splice(index, 1);
//            });
//        };
//
//        $scope.$on('uploadCompleted', function (event) {
//            console.log('uploadCompleted event received');
//            $scope.load();
//        });
//
//    });

'use strict';
angular.module('qeApp')
  .controller('LoginFormController', ['$state', 'Users', function($state, Users) {
    var vm = this;
    // bind here all data from the form
    vm.account = {};
    // place the message if something goes wrong
    vm.authMsg = '';

    var authSuccess = function(accessToken) {
      console.log(accessToken);
      if (accessToken.id) {
        $state.go('app.dashboard');
      }
    };

    var authFail = function(resp) {
      console.log(resp);
      vm.authMsg = 'Authentication failed';
    };

    vm.login = function() {
      vm.authMsg = '';
      console.log(vm.account);
      vm.loginResult = Users.login({
        rememberMe: vm.account.rememberMe
      }, vm.account, authSuccess, authFail);
    };

  }]);

'use strict';
angular.module('qeApp')
  .controller('RegisterFormController', register);

register.$inject = ['$state', 'Users'];

function register($state, Users) {
  var vm = this;

  // bind here all data from the form
  vm.account = {};
  // place the message if something goes wrong
  vm.authMsg = '';

  var logInUser = function(user) {
    Users.login({
      email: user.email,
      password: user.password2 || user.passowrd
    }, loginSucess, loginFail);
  };

  var loginFail = function(resp) {
    console.log(resp);
    vm.authMsg = 'Authentication failed';
  };

  var loginSucess = function(accessToken) {
    console.log(accessToken);
    if (accessToken.id) {
      $state.go('app.dashboard');
    }
  };

  var createSuccess = function(user) {
    if (user.id) {
      logInUser(user);
    }
  };

  var createFail = function(response) {
    console.log(response);
    vm.authMsg = 'Registration failed';
  };

  vm.register = function() {
    vm.authMsg = '';
    vm.account.username = vm.account.email;
    //Users.create(vm.account, createSuccess, createFail);
  };

}

'use strict';
angular.module('qeApp')
    .controller('AlarmFormController', alarm);

alarm.$inject = ['Alarm', 'Trip', 'Station', 'Customer', 'notify', '$stateParams', '$location', '$state'];

function alarm(Alarm, Trip, Station, Customer, notify, $stateParams, $location, $state) {
    var vm = this;
    vm.alarm = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    var filter = {
        'filter[include]': ['trip', 'customer'],
        'filter[where][id]': $stateParams.id
    };

    if ($stateParams.id) {
        Alarm.query(filter, function (alarms) {
            vm.alarm = alarms[0];
        });
    }

    var trips = Trip.find({
        filter: {
            fields: {
                id: true,
                name:true
            }
        }
    }, function () {
        vm.trips = trips;
    });

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                name: true,
                code:true
            }
        }
    }, function () {
        vm.stations = stations;
    });
    var customers = Customer.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.customers = customers;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('Alarm saved!');
        };
        if (!vm.alarm.id) {
            vm.alarm = Alarm.create(vm.alarm, function () {
                saveSuccess(true);
                $state.go('app.alarms-edit', {id: vm.alarm.id})
            }, saveFailed);
        } else {
            vm.alarm.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('AlarmsController', alarms);

alarms.$inject = ['Alarm', 'datatables', '$timeout', '$stateParams', '$rootScope'];

function alarms(Alarm, dataTables, $timeout, $stateParams, $rootScope) {
    var vm = this;
    vm.tripId = $stateParams.tripId;

    var filter = {
        'filter[include]': ['trip', 'customer']
    };

    if (vm.tripId) {
        filter['filter[where][tripId]'] = vm.tripId;
    }

    $rootScope.loading = true;
    Alarm.query(filter, function (alarms) {
        vm.alarms = alarms;
        $timeout(function () {
            dataTables.initDataTable('.alarms-datable');
        })
    });

    vm.delete = function (alarmId) {
        Alarm.deleteById({
            id: alarmId
        }).$promise
            .then(function () {
                debugger;
                dataTables.removeRow(alarmId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}


'use strict';
angular.module('qeApp')
    .controller('CabsFormController', cab);

cab.$inject = ['Cab','Station', 'notify', '$stateParams', '$location', '$state','uploader'];

function cab(Cab, Station, notify, $stateParams, $location, $state, uploader) {
    var vm = this;
    vm.cab = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        vm.cab = Cab.findById({id: $stateParams.id});
    }

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                code: true,
                name: true
            },
            order: 'code'
        }
    }, function () {
        vm.stations = stations;
    });

    vm.uploader = null;
    vm.halfLength = 0;

    if ($stateParams.id) {
        vm.containerId = $stateParams.id;
        vm.uploader = uploader.create($stateParams.id, 'Cab');
        vm.uploader.onCompleteAll = function () {
            loadFiles();
        };

        uploader.getContainer()
            .success(function (container) {
                if (container) {
                    loadFiles();
                }
            })
            .error(function (data) {
                if (data.error.status === 404) {
                    uploader.createContainer();
                }
            });
    }
    else {
        vm.noFiles = true;
    }

    function loadFiles() {
        uploader.loadFilesList()
            .success(function (data) {
                vm.files = data;
                vm.halfLength = Math.ceil(vm.files.length / 2);
            });
    }

    vm.deleteFile = function (index, id) {
        uploader.remove(id)
            .success(function () {
                vm.files.splice(index, 1);
            });
    };

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('Cab saved!');
        };
        if (!vm.cab.id) {
            vm.cab = Cab.create(vm.cab, function () {
                saveSuccess(true);
                $state.go('app.cabs-edit', {id: vm.cab.id})
            }, saveFailed);
        } else {
            vm.cab.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('CabsController', cabs);

cabs.$inject = ['Cab', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function cabs(Cab, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;

    $rootScope.loading = true;
    vm.cabs = Cab.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.cabs-datatable');
        })
    });

    csvParser.attachInputWatcher('.upload-select', Cab);

    vm.delete = function (cabId) {
        Cab.deleteById({
            id: cabId
        }).$promise
            .then(function () {
                dataTables.removeRow(cabId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

'use strict';
angular.module('qeApp')
    .controller('CategoriesController', categories);

categories.$inject = ['Category', 'datatables', '$timeout', 'csvParser'];

function categories(category, dataTables, $timeout, csvParser) {
    var vm = this;
    vm.categories = category.find(function() {
        $timeout(function() {
            dataTables.initDataTable('.categories-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', category);

    vm.delete = function(categoryId) {
        category.deleteById({
            id: categoryId
        }).$promise
            .then(function() {
                dataTables.removeRow(categoryId);
            });
    };

    vm.search = function() {
        dataTables.searchTable(vm.searchQuery);
    };
}

'use strict';
angular.module('qeApp')
    .controller('CategoriesFormController', category);

category.$inject = ['Category', 'notify', '$stateParams', '$location'];

function category(Category, notify, $stateParams, $location) {
    var vm = this;
    vm.category = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';

    if ($stateParams.id) {
        vm.category = Category.findById({
            id: $stateParams.id
        });
    }
    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success(' Category saved!');
        };
        if (!vm.category.id) {
            Category.create(vm.category, function () {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.category.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }

}

'use strict';
angular.module('qeApp')
    .controller('CityFormController', city);

city.$inject = ['City', 'State', 'notify', '$stateParams', '$location'];

function city(City, State, notify, $stateParams, $location) {
    var vm = this;
    vm.city = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';

    if ($stateParams.id) {
        vm.city = City.findById({
            id: $stateParams.id
        }, function() {
            console.log(vm.city.isUrban);
            if (vm.city.state)
                vm.stateId = vm.city.state.id;
        });
    }

    var cities = City.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function() {
        vm.cities = cities;
    });

    vm.states = State.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    });

    function beforeSave() {
        if (vm.stateId) {
            var states = vm.states.filter(function(s) {
                return s.id === vm.stateId;
            });
            if (states.length)
                vm.city.state = states[0];
        }
    }

    vm.save = function() {
        vm.saving = true;
        beforeSave();
        var saveSuccess = function() {
            vm.saving = false;
            notify.success('city saved!');
        };
        if (!vm.city.id) {
            City.create(vm.city, function() {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.city.$save(function() {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('CitiesController', cities);

cities.$inject = ['City', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function cities(City, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;

    $rootScope.loading = true;
    vm.cities = City.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.cities-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', City);

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    vm.delete = function (cityId) {
        City.deleteById({
            id: cityId
        }).$promise
            .then(function () {
                dataTables.removeRow(cityId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

'use strict';
angular.module('qeApp')
    .controller('AddCustomersController', customer);

customer.$inject = ['Customer', 'City', 'State', 'notify', '$stateParams', '$location'];

function customer(Customer, City, State, notify, $stateParams, $location) {
    var vm = this;
    vm.customer = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';
    console.log(vm.action);


    if ($stateParams.id) {
        vm.customer = Customer.findById({
            id: $stateParams.id
        }, function() {
            if (vm.customer.city)
                vm.cityId = vm.customer.city.id;
            if (vm.customer.state)
                vm.stateId = vm.customer.state.id;
        });
    }
    var hello = {
        world:"world"
    }
    if ($stateParams.id) {
        console.log(hello.world);
    }

    var cities = City.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function() {
        vm.cities = cities;
    });

    var states = State.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function() {
      vm.states = states;
    });

    function beforeSave() {
        if (vm.cityId) {
            var cities = vm.cities.filter(function(c) {
                return c.id === vm.cityId;
            });
            if (cities.length)
                vm.customer.city = cities[0];
        }
        if (vm.stateId) {
            var states = vm.states.filter(function(s) {
                return s.id === vm.stateId;
            });
            if (states.length)
                vm.customer.state = states[0];
        }
    }

    vm.save = function() {
        vm.saving = true;
        beforeSave();
        var saveSuccess = function() {
            vm.saving = false;
            notify.success('Customer saved!');
        };
        if (!vm.customer.id) {
            Customer.create(vm.customer, function() {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.customer.$save(function() {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('CustomersController', customers);

customers.$inject = ['Customer', 'datatables', '$timeout', '$state', 'csvParser', '$rootScope'];

function customers(Customer, dataTables, $timeout, $state, csvParser, $rootScope) {
    var vm = this;
    $rootScope.loading = true;
    vm.customers = Customer.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.customers-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', Customer);

    vm.delete = function (customerId) {
        Customer.deleteById({
            id: customerId
        }).$promise
            .then(function () {
                dataTables.removeRow(customerId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

'use strict';
angular.module('qeApp')
    .controller('DashboardController', dashboard);

dashboard.$inject = ['Customer', 'Order', 'Restaurant', 'Cab', 'Hotel', 'PNR', 'State', 'Train', 'City', 'Station', 'StationCategory', 'Trip', 'Alarm',
    'TrainRating', 'Item', 'TrainRoute', 'Category', '$timeout', '$state'];


function dashboard(Customer, Order, Restaurant, Cab, Hotel, PNR, State, Train, City, Station, StationCategory, Trip, Alarm, TrainRating, Item, TrainRoute, Category, $timeout, $state) {
    var vm = this;
    vm.customers = vm.orders = vm.cabs = vm.pnrs = vm.hotels = vm.states = vm.restaurants = 'loading..';
    vm.go = function(state) {
       $state.go(state);
    };
    Customer.count(function (data) {
        vm.customers = data.count;
    });

    Order.count(function (data) {
        vm.orders = data.count;
    });

    Restaurant.count(function (data) {
        vm.restaurants = data.count;
    });

    Cab.count(function (data) {
        vm.cabs = data.count;
    });

    Hotel.count(function (data) {
        vm.hotels = data.count;
    });

    PNR.count(function (data) {
        vm.pnrs = data.count;
    });

    State.count(function (data) {
        vm.states = data.count;
    });

    Train.count(function (data) {
        vm.trains = data.count;
    });

    City.count(function (data) {
        vm.cities = data.count;
    });

    Station.count(function (data) {
        vm.stations = data.count;
    });

    Trip.count(function (data) {
        vm.trips = data.count;
    });

    StationCategory.count(function (data) {
        vm.stationCategories = data.count;
    });

    Alarm.count(function (data) {
        vm.alarms = data.count;
    });

    TrainRating.count(function (data) {
        vm.trainRatings = data.count;
    });

    Category.count(function (data) {
        vm.itemCategories = data.count;
    });

    TrainRoute.count(function (data) {
        vm.routes = data.count;
    });

    Item.count(function (data) {
        vm.items = data.count;
    });
}

/**=========================================================
 * Module: flot-chart.js
 * Initializes the flot chart plugin and attaches the 
 * plugin to elements according to its type
 =========================================================*/

App.controller('FlotChartController', ['$scope', '$window','$http', function($scope, $window, $http) {
  'use strict';

  /**
   * Global object to load data for charts using ajax 
   * Request the chart data from the server via post
   * Expects a response in JSON format to init the plugin
   * Usage
   *   chart = new floatChart(domSelector || domElement, 'server/chart/chart-type.json')
   *   ...
   *   chart.requestData(options);
   *
   * @param  Chart element placeholder or selector
   * @param  Url to get the data via post. Response in JSON format
   */
  $window.FlotChart = function (element, url) {
    // Properties
    this.element = $(element);
    this.url = url;

    // Public method
    this.requestData = function (option, method, callback) {
      var self = this;
      
      // support params (option), (option, method, callback) or (option, callback)
      callback = (method && $.isFunction(method)) ? method : callback;
      method = (method && typeof method == 'string') ? method : 'GET';

      self.option = option; // save options

      $http({
          url:      self.url,
          cache:    false,
          method:   method
      }).success(function (data) {
          
          $.plot( self.element, data, option );
          
          if(callback) callback();

      }).error(function(){
        $.error('Bad chart request.');
      });

      return this; // chain-ability

    };

    // Listen to refresh events
    this.listen = function() {
      var self = this,
          chartPanel = this.element.parents('.panel').eq(0);
      
      // attach custom event
      chartPanel.on('panel-refresh', function(event, panel) {
        // request data and remove spinner when done
        self.requestData(self.option, function(){
          panel.removeSpinner();
        });

      });

      return this; // chain-ability
    };

  };

  //
  // Start of Demo Script
  // 
  angular.element(document).ready(function () {

    // Bar chart
    (function () {
        var Selector = '.chart-bar';
        $(Selector).each(function() {
            var source = $(this).data('source') || $.error('Bar: No source defined.');
            var chart = new FlotChart(this, source),
                //panel = $(Selector).parents('.panel'),
                option = {
                    series: {
                        bars: {
                            align: 'center',
                            lineWidth: 0,
                            show: true,
                            barWidth: 0.6,
                            fill: 0.9
                        }
                    },
                    grid: {
                        borderColor: '#eee',
                        borderWidth: 1,
                        hoverable: true,
                        backgroundColor: '#fcfcfc'
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: '%x : %y'
                    },
                    xaxis: {
                        tickColor: '#fcfcfc',
                        mode: 'categories'
                    },
                    yaxis: {
                        position: ($scope.app.layout.isRTL ? 'right' : 'left'),
                        tickColor: '#eee'
                    },
                    shadowSize: 0
                };
            // Send Request
            chart.requestData(option);
        });

    })();
    // Bar Stacked chart
    (function () {
        var Selector = '.chart-bar-stacked';
        $(Selector).each(function() {
            var source = $(this).data('source') || $.error('Bar Stacked: No source defined.');
            var chart = new FlotChart(this, source),
                option = {
                    series: {
                        stack: true,
                        bars: {
                            align: 'center',
                            lineWidth: 0,
                            show: true,
                            barWidth: 0.6,
                            fill: 0.9
                        }
                    },
                    grid: {
                        borderColor: '#eee',
                        borderWidth: 1,
                        hoverable: true,
                        backgroundColor: '#fcfcfc'
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: '%x : %y'
                    },
                    xaxis: {
                        tickColor: '#fcfcfc',
                        mode: 'categories'
                    },
                    yaxis: {
                        position: ($scope.app.layout.isRTL ? 'right' : 'left'),
                        tickColor: '#eee'
                    },
                    shadowSize: 0
                };
            // Send Request
            chart.requestData(option);
        });
    })();
    // Spline chart
    (function () {
        var Selector = '.chart-spline';
        $(Selector).each(function() {
            var source = $(this).data('source') || $.error('Spline: No source defined.');
            var chart = new FlotChart(this, source),
                option = {
                    series: {
                        lines: {
                            show: false
                        },
                        points: {
                            show: true,
                            radius: 4
                        },
                        splines: {
                            show: true,
                            tension: 0.4,
                            lineWidth: 1,
                            fill: 0.5
                        }
                    },
                    grid: {
                        borderColor: '#eee',
                        borderWidth: 1,
                        hoverable: true,
                        backgroundColor: '#fcfcfc'
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: '%x : %y'
                    },
                    xaxis: {
                        tickColor: '#fcfcfc',
                        mode: 'categories'
                    },
                    yaxis: {
                        min: 0,
                        tickColor: '#eee',
                        position: ($scope.app.layout.isRTL ? 'right' : 'left'),
                        tickFormatter: function (v) {
                            return v/* + ' visitors'*/;
                        }
                    },
                    shadowSize: 0
                };
            
            // Send Request and Listen for refresh events
            chart.requestData(option).listen();

        });
    })();
    // Area chart
    (function () {
        var Selector = '.chart-area';
        $(Selector).each(function() {
            var source = $(this).data('source') || $.error('Area: No source defined.');
            var chart = new FlotChart(this, source),
                option = {
                    series: {
                        lines: {
                            show: true,
                            fill: 0.8
                        },
                        points: {
                            show: true,
                            radius: 4
                        }
                    },
                    grid: {
                        borderColor: '#eee',
                        borderWidth: 1,
                        hoverable: true,
                        backgroundColor: '#fcfcfc'
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: '%x : %y'
                    },
                    xaxis: {
                        tickColor: '#fcfcfc',
                        mode: 'categories'
                    },
                    yaxis: {
                        min: 0,
                        tickColor: '#eee',
                        position: ($scope.app.layout.isRTL ? 'right' : 'left'),
                        tickFormatter: function (v) {
                            return v + ' visitors';
                        }
                    },
                    shadowSize: 0
                };
            
            // Send Request and Listen for refresh events
            chart.requestData(option).listen();

        });
    })();
    // Line chart
    (function () {
        var Selector = '.chart-line';
        $(Selector).each(function() {
            var source = $(this).data('source') || $.error('Line: No source defined.');
            var chart = new FlotChart(this, source),
                option = {
                    series: {
                        lines: {
                            show: true,
                            fill: 0.01
                        },
                        points: {
                            show: true,
                            radius: 4
                        }
                    },
                    grid: {
                        borderColor: '#eee',
                        borderWidth: 1,
                        hoverable: true,
                        backgroundColor: '#fcfcfc'
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: '%x : %y'
                    },
                    xaxis: {
                        tickColor: '#eee',
                        mode: 'categories'
                    },
                    yaxis: {
                        position: ($scope.app.layout.isRTL ? 'right' : 'left'),
                        tickColor: '#eee'
                    },
                    shadowSize: 0
                };
            // Send Request
            chart.requestData(option);
        });
    })();
    // Pïe
    (function () {
        var Selector = '.chart-pie';
        $(Selector).each(function() {
            var source = $(this).data('source') || $.error('Pie: No source defined.');
            var chart = new FlotChart(this, source),
                option = {
                    series: {
                        pie: {
                            show: true,
                            innerRadius: 0,
                            label: {
                                show: true,
                                radius: 0.8,
                                formatter: function (label, series) {
                                    return '<div class="flot-pie-label">' +
                                    //label + ' : ' +
                                    Math.round(series.percent) +
                                    '%</div>';
                                },
                                background: {
                                    opacity: 0.8,
                                    color: '#222'
                                }
                            }
                        }
                    }
                };
            // Send Request
            chart.requestData(option);
        });
    })();
    // Donut
    (function () {
        var Selector = '.chart-donut';
        $(Selector).each(function() {
            var source = $(this).data('source') || $.error('Donut: No source defined.');
            var chart = new FlotChart(this, source),
                option = {
                    series: {
                        pie: {
                            show: true,
                            innerRadius: 0.5 // This makes the donut shape
                        }
                    }
                };
            // Send Request
            chart.requestData(option);
        });
    })();
  });

}]);
angular.module('qeApp')
  .controller('QEFolderController', folder);

folder.$inject = ['$scope', 'mails', '$stateParams', ];

function folder($scope, mails, $stateParams) {
  $scope.folder = $stateParams.folder;
  mails.all().then(function(mails) {
    $scope.mails = mails;
  });
}

'use strict';
angular.module('qeApp')
    .controller('HotelsFormController', hotel);

hotel.$inject = ['Hotel', 'Station', 'notify', '$stateParams', '$location', 'uploader'];

function hotel(Hotel, Station, notify, $stateParams, $location, uploader) {
    var vm = this;
    vm.hotel = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        vm.hotel = Hotel.findById({
            id: $stateParams.id
        });
    }

    vm.uploader = null;
    vm.halfLength = 0;

    if ($stateParams.id) {
        vm.containerId = $stateParams.id;
        vm.uploader = uploader.create($stateParams.id, 'Hotel');
        vm.uploader.onCompleteAll = function () {
            loadFiles();
        };

        uploader.getContainer()
            .success(function (container) {
                if (container) {
                    loadFiles();
                }
            })
            .error(function (data) {
                if (data.error.status === 404) {
                    uploader.createContainer();
                }
            });
    }
    else {
        vm.noFiles = true;
    }

    function loadFiles() {
        uploader.loadFilesList()
            .success(function (data) {
                vm.files = data;
                vm.halfLength = Math.ceil(vm.files.length / 2);
            });
    }

    vm.deleteFile = function (index, id) {
        uploader.remove(id)
            .success(function () {
                vm.files.splice(index, 1);
            });
    };

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                name: true,
                code:true
            },
            order: 'code'
        }
    }, function () {
        vm.stations = stations;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('hotel saved!');
        };
        if (!vm.hotel.id) {
            Hotel.create(vm.hotel, function () {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.hotel.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('HotelsController', hotels);

hotels.$inject = ['Hotel', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function hotels(Hotel, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;
    $rootScope.loading = true;
    vm.hotels = Hotel.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.hotels-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', Hotel);

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    vm.delete = function (cityId) {
        Hotel.deleteById({
            id: cityId
        }).$promise
            .then(function () {
                dataTables.removeRow(cityId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

'use strict';
angular.module('qeApp')
    .controller('ItemsFormController', item);

item.$inject = ['Item', 'Restaurant', 'Category', 'SubCategory', 'notify', '$stateParams', '$location'];

function item(Item, Restaurant, Category, SubCategory, notify, $stateParams, $location) {
    var vm = this;
    vm.item = {};
    vm.action = 'upsert';
    vm.restaurantId = null;

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        var filter = {
            'filter[include]': ['category', 'subcategory', 'restaurant'],
            'filter[where][id]': $stateParams.id
        };
        if ($stateParams.id) {
            Item.query(filter, function(trips) {
                vm.item = trips[0];
            });
        }
    }

    var restaurants = Restaurant.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.restaurants = restaurants;
    });

    var categories = Category.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.categories = categories;
    });

    var subCategories = SubCategory.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.subCategories = subCategories;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('item saved!');
        };
        if (!vm.item.id) {
            Item.create(vm.item, function () {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.item.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('ItemsController', items);

items.$inject = ['Item', 'datatables', '$timeout', '$state', '$stateParams', 'csvParser', '$rootScope'];

function items(Item, dataTables, $timeout, $state, $statePrams, csvParser,  $rootScope) {
    var vm = this;

    var filter = {
        'filter[include]': ['category', 'subcategory', 'restaurant']
    };

    if($statePrams.restaurantId) {
        filter['filter[where][restaurantId]'] = $statePrams.restaurantId;
    }

    $rootScope.loading = true;
    Item.query(filter, function (items) {
        vm.items = items;
        $timeout(function () {
            dataTables.initDataTable('.items-datatable');
        })
    });

    csvParser.attachInputWatcher('.upload-select', Item);

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    vm.delete = function (itemId) {
        Item.deleteById({
            id: itemId
        }).$promise
            .then(function () {
                dataTables.removeRow(itemId);
            });
    };

    vm.showAlarms = function (itemId) {
        $state.go('app.item-alarms', {itemId: itemId});
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

/**=========================================================
 * Module: main.js
 * Main Application Controller
 =========================================================*/

'use strict';
App.controller('AppController', ['$rootScope', '$scope', '$state', '$translate',
    '$window', '$localStorage', '$timeout', 'toggleStateService', 'colors', 'browser', 'cfpLoadingBar','Users',
    function($rootScope, $scope, $state, $translate, $window, $localStorage, $timeout, toggle, colors, browser, cfpLoadingBar, Users) {

        // Loading bar transition
        // -----------------------------------
        var thBar;
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
            if ($('.wrapper > section').length) // check if bar container exists
                thBar = $timeout(function() {
                cfpLoadingBar.start();
            }, 0); // sets a latency Threshold
        });
        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
            event.targetScope.$watch('$viewContentLoaded', function() {
                $timeout.cancel(thBar);
                cfpLoadingBar.complete();
            });
        });


        // Hook not found
        $rootScope.$on('$stateNotFound',
            function(event, unfoundState, fromState, fromParams) {
                console.log(unfoundState.to); // "lazy.state"
                console.log(unfoundState.toParams); // {a:1, b:2}
                console.log(unfoundState.options); // {inherit:false} + default options
            });
        // Hook error
        $rootScope.$on('$stateChangeError',
            function(event, toState, toParams, fromState, fromParams, error) {
                console.log(error);
            });
        // Hook success
        $rootScope.$on('$stateChangeSuccess',
            function(event, toState, toParams, fromState, fromParams) {
                // display new view from top
                $window.scrollTo(0, 0);
                // Save the route title
                $rootScope.currTitle = $state.current.title;
            });

        $rootScope.currTitle = $state.current.title;
        $rootScope.pageTitle = function() {
            return $rootScope.app.name + ' - ' + ($rootScope.currTitle || $rootScope.app.description);
        };

        // iPad may presents ghost click issues
        // if( ! browser.ipad )
        // FastClick.attach(document.body);

        // Close submenu when sidebar change from collapsed to normal
        $rootScope.$watch('app.layout.isCollapsed', function(newValue, oldValue) {
            if (newValue === false)
                $rootScope.$broadcast('closeSidebarMenu');
        });

        // Restore layout settings
        if (angular.isDefined($localStorage.layout))
            $scope.app.layout = $localStorage.layout;
        else
            $localStorage.layout = $scope.app.layout;

        $rootScope.$watch('app.layout', function() {
            $localStorage.layout = $scope.app.layout;
        }, true);


        // Allows to use branding color with interpolation
        // {{ colorByName('primary') }}
        $scope.colorByName = colors.byName;

        // Hides/show user avatar on sidebar
        $scope.toggleUserBlock = function() {
            $scope.$broadcast('toggleUserBlock');
        };

        $scope.logout = function() {
            Users.logout();
            $state.go('page.login');
        };

        // Internationalization
        // ----------------------

        $scope.language = {
            // Handles language dropdown
            listIsOpen: false,
            // list of available languages
            available: {
                'en': 'English',
                'es_AR': 'Español'
            },
            // display always the current ui language
            init: function() {
                var proposedLanguage = $translate.proposedLanguage() || $translate.use();
                var preferredLanguage = $translate.preferredLanguage(); // we know we have set a preferred one in app.config
                $scope.language.selected = $scope.language.available[(proposedLanguage || preferredLanguage)];
            },
            set: function(localeId, ev) {
                // Set the new idiom
                $translate.use(localeId);
                // save a reference for the current language
                $scope.language.selected = $scope.language.available[localeId];
                // finally toggle dropdown
                $scope.language.listIsOpen = !$scope.language.listIsOpen;
            }
        };

        $scope.language.init();

        // Restore application classes state
        toggle.restoreState($(document.body));

        // Applies animation to main view for the next pages to load
        $timeout(function() {
            $rootScope.mainViewAnimation = $rootScope.app.viewAnimation;
        });

        // cancel click event easily
        $rootScope.cancel = function($event) {
            $event.stopPropagation();
        };

    }
]);

'use strict';
angular.module('qeApp')
    .controller('OrdersFormController', order);

order.$inject = ['Order', 'Restaurant', 'Station', 'Customer', 'notify', '$stateParams', '$location', '$filter'];

function order(Order, Restaurant, Station, Customer, notify, $stateParams, $location, $filter) {
    var vm = this;
    vm.order = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        var filter = {
            'filter[include]': ['restaurant', 'customer'],
            'filter[where][id]': $stateParams.id
        };
        if ($stateParams.id) {
            Order.query(filter, function(trips) {
                vm.order = trips[0];
                changeDates();
            });
        }
    }

    function changeDates() {
        if (vm.order.date) {
            vm.order.date = formatDate(vm.order.date);
        }
    }

    function formatDate(date) {
        return $filter('date')(date, 'MM/dd/yyyy H:mm');
    }

    var restaurants = Restaurant.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.restaurants = restaurants;
    });

    var customers = Customer.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.customers = customers;
    });

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                code:true,
                name: true
            }
        }
    }, function () {
        vm.stations = stations;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('order saved!');
        };
        if (!vm.order.id) {
            Order.create(vm.order, function () {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.order.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('OrdersController', orders);

orders.$inject = ['Order', 'datatables', '$timeout', '$state', 'csvParser', '$rootScope'];

function orders(Order, dataTables, $timeout, $state, csvParser, $rootScope) {
    var vm = this;

    var filter = {
        'filter[include]': ['customer', 'restaurant']
    };

    $rootScope.loading = true;
    Order.query(filter, function (orders) {
        vm.orders = orders;
        $timeout(function () {
            dataTables.initDataTable('.orders-datatable');
        })
    });

    csvParser.attachInputWatcher('.upload-select', Order);

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    vm.delete = function (orderId) {
        Order.deleteById({
            id: orderId
        }).$promise
            .then(function () {
                dataTables.removeRow(orderId);
            });
    };

    vm.showAlarms = function (orderId) {
        $state.go('app.order-alarms', {orderId: orderId});
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

'use strict';
angular.module('qeApp')
    .controller('PNRFormController', pnr);

pnr.$inject = ['PNR', 'Station', 'Customer', 'Train', 'notify', '$stateParams', '$location', '$filter'];

function pnr(PNR, Station, Customer, Train, notify, $stateParams, $location, $filter) {
    var vm = this;
    vm.pnr = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';

    if ($stateParams.id) {
        vm.pnr = PNR.findById({
            id: $stateParams.id
        }, function() {
            if (vm.pnr.startingStation)
                vm.startStationId = vm.pnr.startingStation.id;
            if (vm.pnr.endStation)
                vm.endStationId = vm.pnr.endStation.id;
            if (vm.pnr.customer)
                vm.customerId = vm.pnr.customer.id;
            changeDates();
        });
    }

    function changeDates() {
        if (vm.pnr.startDate) {
            vm.pnr.startDate = formatDate(vm.pnr.startDate);
        }
        if (vm.pnr.endDate) {
            vm.pnr.endDate = formatDate(vm.pnr.endDate);
        }
    }

    function formatDate(date) {
        return $filter('date')(date, 'MM/dd/yyyy');
    }
    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                name: true,
                code: true
            },
            order: 'code'
        }
    }, function() {
        vm.stations = stations;
    });

    var customers = Customer.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function() {
        vm.customers = customers;
    });

    var trains = Train.find({
        filter: {
            fields: {
                id: true,
                number: true
            }
        }
    }, function() {
        vm.trains = trains;
    });

    function beforeSave() {
        if (vm.customerId) {
            var customers = vm.customers.filter(function(c) {
                return c.id === vm.customerId;
            });
            if (customers.length)
                vm.pnr.customer = customers[0];
        }
        if (vm.startStationId) {
            var stations = vm.stations.filter(function(s) {
                return s.id === vm.startStationId;
            });
            if (stations.length)
                vm.pnr.startingStation = stations[0];
        }
        if (vm.endStationId) {
            var endStations = vm.stations.filter(function(s) {
                return s.id === vm.endStationId;
            });
            if (endStations.length)
                vm.pnr.endStation = endStations[0];
        }
    }

    vm.save = function() {
        vm.saving = true;
        beforeSave();
        var saveSuccess = function() {
            vm.saving = false;
            changeDates();
            notify.success('PNR saved!');
        };
        if (!vm.pnr.id) {
            PNR.create(vm.pnr, function() {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.pnr.$save(function() {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('PNRsController', pnrs);

pnrs.$inject = ['PNR', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function pnrs(PNR, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;
    $rootScope.loading = true;
    vm.pnrs = PNR.find(function() {
        $timeout(function() {
            dataTables.initDataTable('.pnrs-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', PNR);

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    vm.delete = function(pnrId) {
        PNR.deleteById({
                id: pnrId
            }).$promise
            .then(function() {
                dataTables.removeRow(pnrId);
            });
    };

    vm.search = function() {
        dataTables.searchTable(vm.searchQuery);
    };
}

'use strict';
angular.module('qeApp')
    .controller('RestaurantsFormController', restaurant);

restaurant.$inject = ['Restaurant', 'Station', 'notify', '$stateParams', '$location', '$state', 'uploader'];

function restaurant(Restaurant, Station, notify, $stateParams, $location, $state, uploader) {
    var vm = this;
    vm.restaurant = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        vm.restaurant = Restaurant.findById({id: $stateParams.id});
    }

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                code: true,
                name: true
            },
            order: 'code'
        }
    }, function () {
        vm.stations = stations;
    });

    vm.uploader = null;
    vm.halfLength = 0;

    if ($stateParams.id) {
        vm.containerId = $stateParams.id;
        vm.uploader = uploader.create($stateParams.id, 'Restaurant');
        vm.uploader.onCompleteAll = function () {
            loadFiles();
        };
        uploader.getContainer()
            .success(function (container) {
                if (container) {
                    loadFiles();
                }
            })
            .error(function (data) {
                if (data.error.status === 404) {
                    uploader.createContainer();
                }
            });
    }
    else {
        vm.noFiles = true;
    }

    function loadFiles() {
        uploader.loadFilesList()
            .success(function (data) {
                vm.files = data;
                vm.halfLength = Math.ceil(vm.files.length / 2);
            });
    }

    vm.deleteFile = function (index, id) {
        uploader.remove(id)
            .success(function () {
                vm.files.splice(index, 1);
            });
    };

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('Restaurant saved!');
        };
        if (!vm.restaurant.id) {
            vm.restaurant = Restaurant.create(vm.restaurant, function () {
                saveSuccess(true);
                $state.go('app.restaurants-edit', {id: vm.restaurant.id})
            }, saveFailed);
        } else {
            vm.restaurant.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('RestaurantsController', restaurants);

restaurants.$inject = ['Restaurant', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function restaurants(Restaurant, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;

    $rootScope.loading = true;
    vm.restaurants = Restaurant.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.restaurants-datatable');
        })
    });

    csvParser.attachInputWatcher('.upload-select', Restaurant);

    vm.delete = function (restaurantId) {
        Restaurant.deleteById({
            id: restaurantId
        }).$promise
            .then(function () {
                dataTables.removeRow(restaurantId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

/**=========================================================
 * Module: sidebar-menu.js
 * Provides a simple way to implement bootstrap collapse plugin using a target
 * next to the current element (sibling)
 * Targeted elements must have [data-toggle="collapse-next"]
 =========================================================*/
App.controller('SidebarController', ['$rootScope', '$scope', '$state', '$location', '$http', '$timeout', 'APP_MEDIAQUERY',
    function ($rootScope, $scope, $state, $location, $http, $timeout, mq) {

        var currentState = $rootScope.$state.current.name;
        var $win = $(window);
        var $html = $('html');
        var $body = $('body');

        // Adjustment on route changes
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
            currentState = toState.name;
            // Hide sidebar automatically on mobile
            $('body.aside-toggled').removeClass('aside-toggled');

            $rootScope.$broadcast('closeSidebarMenu');
        });

        // Normalize state on resize to avoid multiple checks
        $win.on('resize', function () {
            if (isMobile())
                $body.removeClass('aside-collapsed');
            else
                $body.removeClass('aside-toggled');
        });

        // Check item and children active state
        var isActive = function (item) {

            if (!item) return;

            if (!item.sref || item.sref == '#') {
                var foundActive = false;
                angular.forEach(item.submenu, function (value, key) {
                    if (isActive(value)) foundActive = true;
                });
                return foundActive;
            }
            else
                return $state.is(item.sref) || $location.url().indexOf(item.activate) >= 0;
        };

        // Load menu from json file
        // -----------------------------------

        $scope.getMenuItemPropClasses = function (item) {
            return (item.heading ? 'nav-heading' : '') +
                (isActive(item) ? ' active' : '');
        };

        $scope.loadSidebarMenu = function () {

            var menuJson = 'server/sidebar-menu.json',
                menuURL = menuJson + '?v=' + (new Date().getTime()); // jumps cache
            $http.get(menuURL)
                .success(function (items) {
                    $rootScope.menuItems = items;
                })
                .error(function (data, status, headers, config) {
                    alert('Failure loading menu');
                });
        };

        $scope.loadSidebarMenu();

        // Handle sidebar collapse items
        // -----------------------------------
        var collapseList = [];

        $scope.addCollapse = function ($index, item) {
            collapseList[$index] = !isActive(item);
        };

        $scope.isCollapse = function ($index) {
            return (collapseList[$index]);
        };

        $scope.toggleCollapse = function ($index, isParentItem) {


            // collapsed sidebar doesn't toggle drodopwn
            if (isSidebarCollapsed() && !isMobile()) return true;

            // make sure the item index exists
            if (angular.isDefined(collapseList[$index])) {
                collapseList[$index] = !collapseList[$index];
                closeAllBut($index);
            }
            else if (isParentItem) {
                closeAllBut(-1);
            }

            return true;

            function closeAllBut(index) {
                index += '';
                for (var i in collapseList) {
                    if (index < 0 || index.indexOf(i) < 0)
                        collapseList[i] = true;
                }
                // angular.forEach(collapseList, function(v, i) {
                // });
            }
        };

        // Helper checks
        // -----------------------------------

        function isMobile() {
            return $win.width() < mq.tablet;
        }

        function isTouch() {
            return $html.hasClass('touch');
        }

        function isSidebarCollapsed() {
            return $body.hasClass('aside-collapsed');
        }

        function isSidebarToggled() {
            return $body.hasClass('aside-toggled');
        }
    }]);

'use strict';
angular.module('qeApp')
  .controller('StateFormController', state);

state.$inject = ['State', 'notify', '$stateParams', '$location'];

function state(State, notify, $stateParams, $location) {
  var vm = this;
  vm.state = {country: 'India'};
  vm.action = 'upsert';

  if ($location.path().indexOf('view') >= 0)
    vm.action = 'view';

  if ($stateParams.id) {
    vm.state = State.findById({
      id: $stateParams.id
    }, function () {
      if (vm.state.state)
        vm.stateId = vm.state.state.id;
    });
  }

  var states = State.find({
    filter: {
      fields: {
        id: true,
        name: true
      }
    }
  }, function () {
    vm.states = states;
  });

  vm.save = function () {
    vm.saving = true;
    var saveSuccess = function () {
      vm.saving = false;
      notify.success('State saved!');
    };
    if (!vm.state.id) {
      State.create(vm.state, function () {
        saveSuccess();
      }, saveFailed);
    } else {
      vm.state.$save(function () {
        saveSuccess();
      }, saveFailed);
    }
  };

  function saveFailed(resp) {
    vm.saving = false;
    if (resp && resp.data && resp.data.error)
      console.log(resp.data.error);
    if (resp && resp.data && resp.data.error)
      notify.error(resp.data.error);
  }
}

'use strict';
angular.module('qeApp')
    .controller('StatesController', states);

states.$inject = ['State', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function states(State, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;

    $rootScope.loading = true;
    vm.states = State.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.states-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', State);

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    vm.delete = function (stateId) {
        State.deleteById({
            id: stateId
        }).$promise
            .then(function () {
                dataTables.removeRow(stateId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

'use strict';
angular.module('qeApp')
    .controller('StationCategoriesController', stationCategories);

stationCategories.$inject = ['StationCategory', 'datatables', '$timeout', 'csvParser'];

function stationCategories(StationCategory, dataTables, $timeout, csvParser) {
    var vm = this;
    vm.stationCategories = StationCategory.find(function() {
        $timeout(function() {
            dataTables.initDataTable('.stationCategories-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', StationCategory);

    vm.delete = function(stationCategoryId) {
        StationCategory.deleteById({
                id: stationCategoryId
            }).$promise
            .then(function() {
                dataTables.removeRow(stationCategoryId);
            });
    };

    vm.search = function() {
        dataTables.searchTable(vm.searchQuery);
    };
}

'use strict';
angular.module('qeApp')
    .controller('StationCategoryFormController', stationCategory);

stationCategory.$inject = ['StationCategory', 'notify', '$stateParams', '$location'];

function stationCategory(StationCategory, notify, $stateParams, $location) {
    var vm = this;
    vm.stationCategory = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';

    if ($stateParams.id) {
        vm.stationCategory = StationCategory.findById({
            id: $stateParams.id
        });
    }
    vm.save = function() {
        vm.saving = true;
        var saveSuccess = function() {
            vm.saving = false;
            notify.success('Station Category saved!');
        };
        if (!vm.stationCategory.id) {
            StationCategory.create(vm.stationCategory, function() {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.stationCategory.$save(function() {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('StationFormController', station);

station.$inject = ['Station', 'City', 'State', 'StationCategory', 'notify', '$stateParams', '$location', 'uploader'];

function station(Station, City, State, StationCategory, notify, $stateParams, $location, uploader) {
    var vm = this;
    vm.station = {};
    vm.action = 'upsert';
    vm.halfLength = 0;

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';

    if ($stateParams.id) {
        vm.station = Station.findById({
            id: $stateParams.id
        }, function () {
            if (vm.station.category)
                vm.categoryId = vm.station.category.id;
            if (vm.station.cityId)
                vm.station.city = City.findById({id: vm.station.cityId});
            if (vm.station.stateId)
                vm.station.state = State.findById({id: vm.station.stateId});
        });

        vm.containerId = $stateParams.id;
        vm.uploader = uploader.create($stateParams.id, 'Station');
        vm.uploader.onCompleteAll = function () {
            loadFiles();
        };

        uploader.getContainer()
            .success(function (container) {
                if (container) {
                    loadFiles();
                }
            })
            .error(function (data) {
                if (data.error.status === 404) {
                    uploader.createContainer();
                }
            });
    }

    function loadFiles() {
        uploader.loadFilesList()
            .success(function (data) {
                vm.files = data;
                vm.halfLength = Math.ceil(vm.files.length / 2);
            });
    }

    vm.deleteFile = function (index, id) {
        uploader.remove(id)
            .success(function () {
                vm.files.splice(index, 1);
            });
    };

    var categories = StationCategory.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.categories = categories;
    });

    var cities = City.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.cities = cities;
    });

    var states = State.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.states = states;
    });

    vm.stateChanged = function () {
        City.findByStateId({
            stateId: vm.station.stateId
        }, function (data) {
            debugger;
            vm.cities = data.cities;
        });
    };

    function beforeSave() {
        if (vm.categoryId) {
            var categories = vm.categories.filter(function (c) {
                return c.id === vm.categoryId;
            });
            if (categories.length)
                vm.station.category = categories[0];
        }
    }

    vm.save = function () {
        vm.saving = true;
        beforeSave();
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('Station saved!');
        };
        if (!vm.station.id) {
            Station.create(vm.station, function () {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.station.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('StationsController', stations);

stations.$inject = ['Station', 'datatables', '$timeout', 'csvParser', '$scope', '$rootScope'];

function stations(Station, dataTables, $timeout, csvParser, $scope, $rootScope) {
    var vm = this;
    vm.uploading = false;

    $rootScope.loading = true;
    vm.stations = Station.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.stations-datatable');
        });
    });

    $scope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $scope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    csvParser.attachInputWatcher('.upload-select', Station);

    vm.delete = function (customerId) {
        Station.deleteById({
            id: customerId
        }).$promise
            .then(function () {
                dataTables.removeRow(customerId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

'use strict';
angular.module('qeApp')
    .controller('SubCategoriesController', subCategories);

subCategories.$inject = ['SubCategory', 'datatables', '$timeout', 'csvParser'];

function subCategories(SubCategory, dataTables, $timeout, csvParser) {
    var vm = this;
    vm.subCategories = SubCategory.find(function() {
        $timeout(function() {
            dataTables.initDataTable('.subCategories-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', SubCategory);

    vm.delete = function(subcategoryId) {
        SubCategory.deleteById({
            id: subcategoryId
        }).$promise
            .then(function() {
                dataTables.removeRow(subcategoryId);
            });
    };

    vm.search = function() {
        dataTables.searchTable(vm.searchQuery);
    };
}

'use strict';
angular.module('qeApp')
    .controller('SubCategoriesFormController', subCategory);

subCategory.$inject = ['SubCategory','Category' ,'notify', '$stateParams', '$location'];

function subCategory(SubCategory, Category, notify, $stateParams, $location) {
    var vm = this;
    vm.subCategory = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';

    if ($stateParams.id) {
        vm.subCategory = SubCategory.findById({
            id: $stateParams.id
        });
    }

    var categories = Category.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.categories = categories;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success(' SubCategory saved!');
        };
        if (!vm.subCategory.id) {
            SubCategory.create(vm.subCategory, function () {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.subCategory.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }

}

'use strict';
angular.module('qeApp')
    .controller('TrainFormController', train);

train.$inject = ['Train', 'Station', 'notify', '$stateParams', '$location', 'uploader', '$state'];

function train(Train, Station, notify, $stateParams, $location, uploader, $state) {
    var vm = this;
    vm.train = {};
    vm.action = 'upsert';
    vm.uploader = null;

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    vm.halfLength = 0;
    if ($stateParams.id) {
        vm.train = Train.findById({
            id: $stateParams.id
        });

        vm.containerId = $stateParams.id;
        vm.uploader = uploader.create($stateParams.id, 'Train');
        vm.uploader.onCompleteAll = function () {
            loadFiles();
        };

        uploader.getContainer()
            .success(function (container) {
                if (container) {
                    loadFiles();
                }
            })
            .error(function (data) {
                if (data.error.status === 404) {
                    uploader.createContainer();
                }
            })
    }
    else {
        vm.noFiles = true;
    }

    function loadFiles() {
        uploader.loadFilesList()
            .success(function (data) {
                vm.files = data;
                vm.halfLength = Math.ceil(vm.files.length / 2);
            });
    }

    vm.deleteFile = function (index, id) {
        uploader.remove(id)
            .success(function () {
                vm.files.splice(index, 1);
            });
    };

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                name: true,
                code: true
            },
            order: 'code'
        }
    }, function () {
        vm.stations = stations;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('Train saved!');
        };
        if (!vm.train.id) {
            vm.train = Train.create(vm.train, function () {
                saveSuccess(true);
                $state.go('app.trains-edit', {id: vm.train.id})
            }, saveFailed);
        } else {
            vm.train.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('TrainRatingFormController', trainRating);

trainRating.$inject = ['TrainRating', 'PNR', 'Train', 'Trip', 'notify', '$stateParams', '$location', '$state'];

function trainRating(TrainRating, PNR, Train, Trip, notify, $stateParams, $location, $state) {
    var vm = this;
    vm.trainRating = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    var filter = {
        'filter[include]': ['train', 'trip', 'PNR'],
        'filter[where][id]': $stateParams.id
    };

    if ($stateParams.id) {
        TrainRating.query(filter, function (trainRatings) {
            vm.trainRating = trainRatings[0];
        });
    }

    if (vm.action === 'upsert') {
        var pnrs = PNR.find({
            filter: {
                fields: {
                    id: true,
                    pnr: true
                }
            }
        }, function () {
            vm.pnrs = pnrs;
        });

        var trips = Trip.find({
            filter: {
                fields: {
                    id: true,
                    name: true
                }
            }
        }, function () {
            vm.trips = trips;
        });

        var trains = Train.find({
            filter: {
                fields: {
                    id: true,
                    number: true
                }
            }
        }, function () {
            vm.trains = trains;
        });
    }

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('TrainRating saved!');
        };
        if (!vm.trainRating.id) {
            vm.trainRating = TrainRating.create(vm.trainRating, function () {
                saveSuccess(true);
                $state.go('app.trainRatings-edit', {id: vm.trainRating.id})
            }, saveFailed);
        } else {
            vm.trainRating.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('TrainRatingsController', trainRatings);

trainRatings.$inject = ['TrainRating', 'datatables', '$timeout', '$state', '$rootScope'];

function trainRatings(TrainRating, dataTables, $timeout, $state, $rootScope) {
    var vm = this;

    var filter = {
        'filter[include]': ['PNR', 'train', 'trip']
    };

    $rootScope.loading = true;
    TrainRating.query(filter, function (trainRatings) {
        vm.trainRatings = trainRatings;
        $timeout(function () {
            dataTables.initDataTable('.trainRatings-datatable');
        })
    });


    vm.delete = function (trainRatingId) {
        TrainRating.deleteById({
            id: trainRatingId
        }).$promise
            .then(function () {
                dataTables.removeRow(trainRatingId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

/**
 * Created by zulfat on 27/02/15.
 */

/**
 * Created by zulfat on 27/02/15.
 */

'use strict';
angular.module('qeApp')
    .controller('TrainRoutesFormController', trainRoute);

trainRoute.$inject = ['TrainRoute', 'Train', 'Station', 'notify', '$stateParams', '$location', '$state'];

function trainRoute(TrainRoute, Train, Station, notify, $stateParams, $location, $state) {
    var vm = this;
    vm.trainRoute = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        vm.trainRoute = TrainRoute.findById({id: $stateParams.id});
    }

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                code: true,
                name: true
            },
            order: 'code'
        }
    }, function () {
        vm.stations = stations;
    });

    var trains = Train.find({
        filter: {
            fields: {
                id: true,
                number: true
            },
            order: 'number'
        }
    }, function () {
        vm.trains = trains;
    });

    vm.deleteFile = function (index, id) {
        uploader.remove(id)
            .success(function () {
                vm.files.splice(index, 1);
            });
    };

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('TrainRoute saved!');
        };
        if (!vm.trainRoute.id) {
            vm.trainRoute = TrainRoute.create(vm.trainRoute, function () {
                saveSuccess(true);
                $state.go('app.trainRoutes-edit', {id: vm.trainRoute.id})
            }, saveFailed);
        } else {
            vm.trainRoute.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('TrainRoutesController', trainRoutes);

trainRoutes.$inject = ['TrainRoute', 'datatables', '$timeout', 'csvParser', '$stateParams', '$rootScope'];

function trainRoutes(TrainRoute, dataTables, $timeout, csvParser, $stateParams, $rootScope) {
    var vm = this;

    var where = null;
    if ($stateParams.trainNo) {
        where = {
            trainNo: $stateParams.trainNo
        };
    }

    $rootScope.loading = true;
    vm.trainRoutes = TrainRoute.find({filter: {where: where}}, function () {
        $timeout(function () {
            dataTables.initDataTable('.trainRoutes-datatable');
        })
    });

    csvParser.attachInputWatcher('.upload-select', TrainRoute);

    vm.delete = function (trainRouteId) {
        TrainRoute.deleteById({
            id: trainRouteId
        }).$promise
            .then(function () {
                dataTables.removeRow(trainRouteId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

'use strict';
angular.module('qeApp')
    .controller('TrainsController', trains);

trains.$inject = ['Train', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function trains(Train, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;
    $rootScope.loading = true;
    vm.trains = Train.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.trains-datatable');
        });
    });

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    csvParser.attachInputWatcher('.upload-select', Train);

    vm.delete = function (trainId) {
        Train.deleteById({
            id: trainId
        }).$promise
            .then(function () {
                dataTables.removeRow(trainId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

'use strict';
angular.module('qeApp')
    .controller('TripFormController', trip);

trip.$inject = ['Trip', 'PNR', 'Customer', 'notify', '$stateParams', '$location', '$state'];

function trip(Trip, PNR, Customer, notify, $stateParams, $location, $state) {
    var vm = this;
    vm.trip = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        var filter = {
            'filter[include]': ['PNR', 'customer'],
            'filter[where][id]': $stateParams.id
        };
        Trip.query(filter, function(trips) {
            vm.trip = trips[0];
        });
    }

    var pnrs = PNR.find({
        filter: {
            fields: {
                id: true,
                pnr: true
            }
        }
    }, function () {
        vm.pnrs = pnrs;
    });

    var customers = Customer.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.customers = customers;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('Trip saved!');
        };
        if (!vm.trip.id) {
            vm.trip = Trip.create(vm.trip, function () {
                saveSuccess(true);
                $state.go('app.trips-edit', {id: vm.trip.id})
            }, saveFailed);
        } else {
            vm.trip.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}

'use strict';
angular.module('qeApp')
    .controller('TripsController', trips);

trips.$inject = ['Trip', 'datatables', '$timeout', '$state', '$rootScope'];

function trips(Trip, dataTables, $timeout, $state, $rootScope) {
    var vm = this;

    var filter = {
        'filter[include]': ['PNR', 'customer']
    };

    $rootScope.loading = true;
    Trip.query(filter, function (trips) {
        vm.trips = trips;
        $timeout(function () {
            dataTables.initDataTable('.trips-datatable');
        })
    });


    vm.delete = function (tripId) {
        Trip.deleteById({
            id: tripId
        }).$promise
            .then(function () {
                dataTables.removeRow(tripId);
            });
    };

    vm.showAlarms = function (tripId) {
        $state.go('app.trip-alarms', {tripId: tripId});
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}

App.controller('UserBlockController', ['$scope', function($scope) {

  $scope.userBlockVisible = true;
  
  $scope.$on('toggleUserBlock', function(event, args) {

    $scope.userBlockVisible = ! $scope.userBlockVisible;
    
  });

}]);
angular.module('qeApp')
  .controller('QEViewController', ['$scope', 'mails', '$stateParams', function($scope, mails, $stateParams) {
    mails.get($stateParams.mid).then(function(mail) {
      $scope.mail = mail;
    });
  }]);

'use strict';
angular.module('qeApp')
    .factory('csvParser', csvParser);

csvParser.$inject = ['notify', 'datatables', '$state', '$rootScope'];

function csvParser(notify, dataTables, $state, $rootScope) {
    function attachInputWatcher(inputSelector, Model) {
        $(inputSelector).on("change", function () {
            var file = $(this)[0].files[0];
            if (file)
                onFileSelected(file);
        });

        function onFileSelected(file) {
            $rootScope.$broadcast('uploadStart');
            Papa.parse(file, {
                header: true,
                complete: parseCompleted,
                error: function (err, file, inputElem, reason) {
                    notify.error("File couldn't be parsed. Only csv files accepted");
                    console.log(err + '\n' + reason);
                }
            });
        }

        function parseCompleted(results) {
            if (results.errors && results.errors.length) {
                notify.error("File couldn't be parsed. Only csv files accepted");
                return;
            }

            if (!results.data)
                throw new Error('no data in parsed results');

            Model.insertMany({data: results.data})
                .$promise
                .then(function () {
                    dataTables.destroy();
                    $state.go($state.current, {}, {reload: true});
                }, function (err) {
                    if (err.data && err.data.error && err.data.error.message) {
                        notify.error(err.data.error.message);
                    }
                    else {
                        notify.error("Data import failed. Please check you csv file format is valid");
                    }
                })
                .finally(function () {
                    $rootScope.$broadcast('uploadFinish');
                });
        }
    }

    return {
        attachInputWatcher: attachInputWatcher
    }
}

'use strict';
angular.module('qeApp')
    .factory('datatables', ['$rootScope', dataTables]);

function dataTables($rootScope) {
    var table = null;

    function initDataTable(selector) {
        var tableSelector = selector || '.datatable';
        // if (!$.fn.DataTable.isDataTable(tableSelector || '.datatable')) {
        table = $(tableSelector).dataTable({
            'sDom': 't<pl>',
            'paging': true, // Table pagination
            'ordering': true,
            'initComplete': function () {
                $rootScope.loading = false;
            },
            'searching': true, // Column ordering
            // Text translation options
            // Note the required keywords between underscores (e.g _MENU_)
            oLanguage: {
                sSearch: 'Search all columns:',
                sLengthMenu: '_MENU_ records per page',
                info: 'Showing page _PAGE_ of _PAGES_',
                zeroRecords: 'Nothing found - sorry',
                infoEmpty: 'No records available',
                infoFiltered: '(filtered from _MAX_ total records)'
            }
        });
        var inputSearchClass = 'datatable_input_col_search';
        var columnInputs = $('thead.filters .' + inputSearchClass);

        // On input keyup trigger filtering
        columnInputs
            .keyup(function () {
                table.fnFilter(this.value, columnInputs.index(this));
            });
        columnInputs.click(function (event) {
            event.stopPropagation();
            event.preventDefault();
        });
    }

    function removeRow(id) {
        var rowselector = '.' + id;
        table.fnDeleteRow($(rowselector));
    }

    function searchTable(query) {
        table.fnFilter(query);
    }

    function draw() {
        table.fnDraw();
    }

    function destroy() {
        table.fnDestroy();
    }

    return {
        searchTable: searchTable,
        initDataTable: initDataTable,
        removeRow: removeRow,
        destroy: destroy,
        draw: draw
    };
}

(function(window, angular, undefined) {'use strict';

var urlBase = "/api";
var authHeader = 'authorization';

/**
 * @ngdoc overview
 * @name lbServices
 * @module
 * @description
 *
 * The `lbServices` module provides services for interacting with
 * the models exposed by the LoopBack server via the REST API.
 *
 */
var module = angular.module("lbServices", ['ngResource']);

/**
 * @ngdoc object
 * @name lbServices.Customer
 * @header lbServices.Customer
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Customer` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Customer",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/customers/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Customer.trips.findById() instead.
        "prototype$__findById__trips": {
          url: urlBase + "/customers/:id/trips/:fk",
          method: "GET"
        },

        // INTERNAL. Use Customer.trips.destroyById() instead.
        "prototype$__destroyById__trips": {
          url: urlBase + "/customers/:id/trips/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Customer.trips.updateById() instead.
        "prototype$__updateById__trips": {
          url: urlBase + "/customers/:id/trips/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Customer.trips() instead.
        "prototype$__get__trips": {
          isArray: true,
          url: urlBase + "/customers/:id/trips",
          method: "GET"
        },

        // INTERNAL. Use Customer.trips.create() instead.
        "prototype$__create__trips": {
          url: urlBase + "/customers/:id/trips",
          method: "POST"
        },

        // INTERNAL. Use Customer.trips.destroyAll() instead.
        "prototype$__delete__trips": {
          url: urlBase + "/customers/:id/trips",
          method: "DELETE"
        },

        // INTERNAL. Use Customer.trips.count() instead.
        "prototype$__count__trips": {
          url: urlBase + "/customers/:id/trips/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Customer#create
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Customer` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/customers",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Customer#upsert
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Customer` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/customers",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Customer#exists
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/customers/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Customer#findById
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Customer` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/customers/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Customer#find
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Customer` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/customers",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Customer#findOne
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Customer` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/customers/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Customer#updateAll
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/customers/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Customer#deleteById
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/customers/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Customer#count
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/customers/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Customer#prototype$updateAttributes
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Customer` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/customers/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Customer#insertMany
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `data` – `{array=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "insertMany": {
          url: urlBase + "/customers/insertMany",
          method: "POST"
        },

        // INTERNAL. Use Trip.customer() instead.
        "::get::trip::customer": {
          url: urlBase + "/trips/:id/customer",
          method: "GET"
        },

        // INTERNAL. Use Alarm.customer() instead.
        "::get::Alarm::customer": {
          url: urlBase + "/alarms/:id/customer",
          method: "GET"
        },

        // INTERNAL. Use Order.customer() instead.
        "::get::Order::customer": {
          url: urlBase + "/orders/:id/customer",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Customer#updateOrCreate
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Customer` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Customer#update
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Customer#destroyById
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Customer#removeById
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Customer#modelName
    * @propertyOf lbServices.Customer
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Customer`.
    */
    R.modelName = "Customer";

    /**
     * @ngdoc object
     * @name lbServices.Customer.trips
     * @header lbServices.Customer.trips
     * @object
     * @description
     *
     * The object `Customer.trips` groups methods
     * manipulating `Trip` instances related to `Customer`.
     *
     * Call {@link lbServices.Customer#trips Customer.trips()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Customer#trips
         * @methodOf lbServices.Customer
         *
         * @description
         *
         * Queries trips of Customer.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        R.trips = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::get::Customer::trips"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Customer.trips#count
         * @methodOf lbServices.Customer.trips
         *
         * @description
         *
         * Counts trips of Customer.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.trips.count = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::count::Customer::trips"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Customer.trips#create
         * @methodOf lbServices.Customer.trips
         *
         * @description
         *
         * Creates a new instance in trips of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        R.trips.create = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::create::Customer::trips"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Customer.trips#destroyAll
         * @methodOf lbServices.Customer.trips
         *
         * @description
         *
         * Deletes all trips of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.trips.destroyAll = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::delete::Customer::trips"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Customer.trips#destroyById
         * @methodOf lbServices.Customer.trips
         *
         * @description
         *
         * Delete a related item by id for trips
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for trips
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.trips.destroyById = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::destroyById::Customer::trips"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Customer.trips#findById
         * @methodOf lbServices.Customer.trips
         *
         * @description
         *
         * Find a related item by id for trips
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for trips
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        R.trips.findById = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::findById::Customer::trips"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Customer.trips#updateById
         * @methodOf lbServices.Customer.trips
         *
         * @description
         *
         * Update a related item by id for trips
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for trips
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        R.trips.updateById = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::updateById::Customer::trips"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.State
 * @header lbServices.State
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `State` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "State",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/states/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.State#create
         * @methodOf lbServices.State
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/states",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.State#upsert
         * @methodOf lbServices.State
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/states",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.State#exists
         * @methodOf lbServices.State
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/states/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.State#findById
         * @methodOf lbServices.State
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/states/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.State#find
         * @methodOf lbServices.State
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/states",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.State#findOne
         * @methodOf lbServices.State
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/states/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.State#updateAll
         * @methodOf lbServices.State
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/states/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.State#deleteById
         * @methodOf lbServices.State
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/states/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.State#count
         * @methodOf lbServices.State
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/states/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.State#prototype$updateAttributes
         * @methodOf lbServices.State
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/states/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.State#insertMany
         * @methodOf lbServices.State
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `data` – `{array=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "insertMany": {
          url: urlBase + "/states/insertMany",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.State#updateOrCreate
         * @methodOf lbServices.State
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `State` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.State#update
         * @methodOf lbServices.State
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.State#destroyById
         * @methodOf lbServices.State
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.State#removeById
         * @methodOf lbServices.State
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.State#modelName
    * @propertyOf lbServices.State
    * @description
    * The name of the model represented by this $resource,
    * i.e. `State`.
    */
    R.modelName = "State";


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.City
 * @header lbServices.City
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `City` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "City",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/cities/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.City#create
         * @methodOf lbServices.City
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `City` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/cities",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.City#upsert
         * @methodOf lbServices.City
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `City` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/cities",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.City#exists
         * @methodOf lbServices.City
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/cities/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.City#findById
         * @methodOf lbServices.City
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `City` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/cities/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.City#find
         * @methodOf lbServices.City
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `City` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/cities",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.City#findOne
         * @methodOf lbServices.City
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `City` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/cities/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.City#updateAll
         * @methodOf lbServices.City
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/cities/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.City#deleteById
         * @methodOf lbServices.City
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/cities/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.City#count
         * @methodOf lbServices.City
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/cities/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.City#prototype$updateAttributes
         * @methodOf lbServices.City
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `City` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/cities/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.City#findByStateId
         * @methodOf lbServices.City
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `stateId` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `cities` – `{array=}` - 
         */
        "findByStateId": {
          url: urlBase + "/cities/findByStateId",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.City#insertMany
         * @methodOf lbServices.City
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `data` – `{array=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "insertMany": {
          url: urlBase + "/cities/insertMany",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.City#updateOrCreate
         * @methodOf lbServices.City
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `City` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.City#update
         * @methodOf lbServices.City
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.City#destroyById
         * @methodOf lbServices.City
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.City#removeById
         * @methodOf lbServices.City
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.City#modelName
    * @propertyOf lbServices.City
    * @description
    * The name of the model represented by this $resource,
    * i.e. `City`.
    */
    R.modelName = "City";


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.TempCode
 * @header lbServices.TempCode
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `TempCode` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "TempCode",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/tempCodes/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.TempCode#create
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TempCode` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/tempCodes",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.TempCode#upsert
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TempCode` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/tempCodes",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.TempCode#exists
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/tempCodes/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TempCode#findById
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TempCode` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/tempCodes/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TempCode#find
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TempCode` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/tempCodes",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TempCode#findOne
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TempCode` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/tempCodes/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TempCode#updateAll
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/tempCodes/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.TempCode#deleteById
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/tempCodes/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.TempCode#count
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/tempCodes/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TempCode#prototype$updateAttributes
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TempCode` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/tempCodes/:id",
          method: "PUT"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.TempCode#updateOrCreate
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TempCode` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.TempCode#update
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.TempCode#destroyById
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.TempCode#removeById
         * @methodOf lbServices.TempCode
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.TempCode#modelName
    * @propertyOf lbServices.TempCode
    * @description
    * The name of the model represented by this $resource,
    * i.e. `TempCode`.
    */
    R.modelName = "TempCode";


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.PNR
 * @header lbServices.PNR
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `PNR` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "PNR",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/pnrs/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use PNR.trips.findById() instead.
        "prototype$__findById__trips": {
          url: urlBase + "/pnrs/:id/trips/:fk",
          method: "GET"
        },

        // INTERNAL. Use PNR.trips.destroyById() instead.
        "prototype$__destroyById__trips": {
          url: urlBase + "/pnrs/:id/trips/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use PNR.trips.updateById() instead.
        "prototype$__updateById__trips": {
          url: urlBase + "/pnrs/:id/trips/:fk",
          method: "PUT"
        },

        // INTERNAL. Use PNR.trips() instead.
        "prototype$__get__trips": {
          isArray: true,
          url: urlBase + "/pnrs/:id/trips",
          method: "GET"
        },

        // INTERNAL. Use PNR.trips.create() instead.
        "prototype$__create__trips": {
          url: urlBase + "/pnrs/:id/trips",
          method: "POST"
        },

        // INTERNAL. Use PNR.trips.destroyAll() instead.
        "prototype$__delete__trips": {
          url: urlBase + "/pnrs/:id/trips",
          method: "DELETE"
        },

        // INTERNAL. Use PNR.trips.count() instead.
        "prototype$__count__trips": {
          url: urlBase + "/pnrs/:id/trips/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.PNR#create
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PNR` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/pnrs",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.PNR#upsert
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PNR` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/pnrs",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.PNR#exists
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/pnrs/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.PNR#findById
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PNR` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/pnrs/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.PNR#find
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PNR` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/pnrs",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.PNR#findOne
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PNR` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/pnrs/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.PNR#updateAll
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/pnrs/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.PNR#deleteById
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/pnrs/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.PNR#count
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/pnrs/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.PNR#prototype$updateAttributes
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PNR` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/pnrs/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.PNR#getByCustomerId
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `pnrs` – `{array=}` - 
         */
        "getByCustomerId": {
          url: urlBase + "/pnrs/getByCustomerId",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.PNR#insertMany
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `data` – `{array=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "insertMany": {
          url: urlBase + "/pnrs/insertMany",
          method: "POST"
        },

        // INTERNAL. Use Trip.PNR() instead.
        "::get::trip::PNR": {
          url: urlBase + "/trips/:id/PNR",
          method: "GET"
        },

        // INTERNAL. Use TrainRating.PNR() instead.
        "::get::TrainRating::PNR": {
          url: urlBase + "/trainratings/:id/PNR",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.PNR#updateOrCreate
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PNR` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.PNR#update
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.PNR#destroyById
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.PNR#removeById
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.PNR#modelName
    * @propertyOf lbServices.PNR
    * @description
    * The name of the model represented by this $resource,
    * i.e. `PNR`.
    */
    R.modelName = "PNR";

    /**
     * @ngdoc object
     * @name lbServices.PNR.trips
     * @header lbServices.PNR.trips
     * @object
     * @description
     *
     * The object `PNR.trips` groups methods
     * manipulating `Trip` instances related to `PNR`.
     *
     * Call {@link lbServices.PNR#trips PNR.trips()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.PNR#trips
         * @methodOf lbServices.PNR
         *
         * @description
         *
         * Queries trips of PNR.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        R.trips = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::get::PNR::trips"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PNR.trips#count
         * @methodOf lbServices.PNR.trips
         *
         * @description
         *
         * Counts trips of PNR.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.trips.count = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::count::PNR::trips"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PNR.trips#create
         * @methodOf lbServices.PNR.trips
         *
         * @description
         *
         * Creates a new instance in trips of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        R.trips.create = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::create::PNR::trips"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PNR.trips#destroyAll
         * @methodOf lbServices.PNR.trips
         *
         * @description
         *
         * Deletes all trips of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.trips.destroyAll = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::delete::PNR::trips"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PNR.trips#destroyById
         * @methodOf lbServices.PNR.trips
         *
         * @description
         *
         * Delete a related item by id for trips
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for trips
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.trips.destroyById = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::destroyById::PNR::trips"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PNR.trips#findById
         * @methodOf lbServices.PNR.trips
         *
         * @description
         *
         * Find a related item by id for trips
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for trips
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        R.trips.findById = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::findById::PNR::trips"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.PNR.trips#updateById
         * @methodOf lbServices.PNR.trips
         *
         * @description
         *
         * Update a related item by id for trips
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for trips
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        R.trips.updateById = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::updateById::PNR::trips"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.StationCategory
 * @header lbServices.StationCategory
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `StationCategory` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "StationCategory",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/stationCategories/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.StationCategory#create
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationCategory` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/stationCategories",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationCategory#upsert
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationCategory` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/stationCategories",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationCategory#exists
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/stationCategories/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationCategory#findById
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationCategory` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/stationCategories/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationCategory#find
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationCategory` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/stationCategories",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationCategory#findOne
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationCategory` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/stationCategories/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationCategory#updateAll
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/stationCategories/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationCategory#deleteById
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/stationCategories/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationCategory#count
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/stationCategories/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationCategory#prototype$updateAttributes
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationCategory` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/stationCategories/:id",
          method: "PUT"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.StationCategory#updateOrCreate
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationCategory` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.StationCategory#update
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.StationCategory#destroyById
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.StationCategory#removeById
         * @methodOf lbServices.StationCategory
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.StationCategory#modelName
    * @propertyOf lbServices.StationCategory
    * @description
    * The name of the model represented by this $resource,
    * i.e. `StationCategory`.
    */
    R.modelName = "StationCategory";


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Station
 * @header lbServices.Station
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Station` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Station",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/stations/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Station.stationReviews.findById() instead.
        "prototype$__findById__stationReviews": {
          url: urlBase + "/stations/:id/stationReviews/:fk",
          method: "GET"
        },

        // INTERNAL. Use Station.stationReviews.destroyById() instead.
        "prototype$__destroyById__stationReviews": {
          url: urlBase + "/stations/:id/stationReviews/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Station.stationReviews.updateById() instead.
        "prototype$__updateById__stationReviews": {
          url: urlBase + "/stations/:id/stationReviews/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Station.stationReviews() instead.
        "prototype$__get__stationReviews": {
          isArray: true,
          url: urlBase + "/stations/:id/stationReviews",
          method: "GET"
        },

        // INTERNAL. Use Station.stationReviews.create() instead.
        "prototype$__create__stationReviews": {
          url: urlBase + "/stations/:id/stationReviews",
          method: "POST"
        },

        // INTERNAL. Use Station.stationReviews.destroyAll() instead.
        "prototype$__delete__stationReviews": {
          url: urlBase + "/stations/:id/stationReviews",
          method: "DELETE"
        },

        // INTERNAL. Use Station.stationReviews.count() instead.
        "prototype$__count__stationReviews": {
          url: urlBase + "/stations/:id/stationReviews/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Station#create
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Station` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/stations",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Station#upsert
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Station` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/stations",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Station#exists
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/stations/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Station#findById
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Station` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/stations/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Station#find
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Station` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/stations",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Station#findOne
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Station` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/stations/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Station#updateAll
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/stations/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Station#deleteById
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/stations/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Station#count
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/stations/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Station#prototype$updateAttributes
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Station` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/stations/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Station#insertMany
         * @methodOf lbServices.Station
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `data` – `{array=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "insertMany": {
          url: urlBase + "/stations/insertMany",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Station#updateOrCreate
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Station` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Station#update
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Station#destroyById
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Station#removeById
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Station#modelName
    * @propertyOf lbServices.Station
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Station`.
    */
    R.modelName = "Station";

    /**
     * @ngdoc object
     * @name lbServices.Station.stationReviews
     * @header lbServices.Station.stationReviews
     * @object
     * @description
     *
     * The object `Station.stationReviews` groups methods
     * manipulating `StationReview` instances related to `Station`.
     *
     * Call {@link lbServices.Station#stationReviews Station.stationReviews()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Station#stationReviews
         * @methodOf lbServices.Station
         *
         * @description
         *
         * Queries stationReviews of Station.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationReview` object.)
         * </em>
         */
        R.stationReviews = function() {
          var TargetResource = $injector.get("StationReview");
          var action = TargetResource["::get::Station::stationReviews"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Station.stationReviews#count
         * @methodOf lbServices.Station.stationReviews
         *
         * @description
         *
         * Counts stationReviews of Station.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.stationReviews.count = function() {
          var TargetResource = $injector.get("StationReview");
          var action = TargetResource["::count::Station::stationReviews"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Station.stationReviews#create
         * @methodOf lbServices.Station.stationReviews
         *
         * @description
         *
         * Creates a new instance in stationReviews of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationReview` object.)
         * </em>
         */
        R.stationReviews.create = function() {
          var TargetResource = $injector.get("StationReview");
          var action = TargetResource["::create::Station::stationReviews"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Station.stationReviews#destroyAll
         * @methodOf lbServices.Station.stationReviews
         *
         * @description
         *
         * Deletes all stationReviews of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.stationReviews.destroyAll = function() {
          var TargetResource = $injector.get("StationReview");
          var action = TargetResource["::delete::Station::stationReviews"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Station.stationReviews#destroyById
         * @methodOf lbServices.Station.stationReviews
         *
         * @description
         *
         * Delete a related item by id for stationReviews
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for stationReviews
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.stationReviews.destroyById = function() {
          var TargetResource = $injector.get("StationReview");
          var action = TargetResource["::destroyById::Station::stationReviews"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Station.stationReviews#findById
         * @methodOf lbServices.Station.stationReviews
         *
         * @description
         *
         * Find a related item by id for stationReviews
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for stationReviews
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationReview` object.)
         * </em>
         */
        R.stationReviews.findById = function() {
          var TargetResource = $injector.get("StationReview");
          var action = TargetResource["::findById::Station::stationReviews"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Station.stationReviews#updateById
         * @methodOf lbServices.Station.stationReviews
         *
         * @description
         *
         * Update a related item by id for stationReviews
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for stationReviews
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationReview` object.)
         * </em>
         */
        R.stationReviews.updateById = function() {
          var TargetResource = $injector.get("StationReview");
          var action = TargetResource["::updateById::Station::stationReviews"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Train
 * @header lbServices.Train
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Train` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Train",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/trains/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Train#create
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Train` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/trains",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Train#upsert
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Train` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/trains",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Train#exists
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/trains/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Train#findById
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Train` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/trains/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Train#find
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Train` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/trains",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Train#findOne
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Train` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/trains/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Train#updateAll
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/trains/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Train#deleteById
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/trains/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Train#count
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/trains/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Train#prototype$updateAttributes
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Train` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/trains/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Train#insertMany
         * @methodOf lbServices.Train
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `data` – `{array=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "insertMany": {
          url: urlBase + "/trains/insertMany",
          method: "POST"
        },

        // INTERNAL. Use TrainRating.train() instead.
        "::get::TrainRating::train": {
          url: urlBase + "/trainratings/:id/train",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Train#updateOrCreate
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Train` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Train#update
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Train#destroyById
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Train#removeById
         * @methodOf lbServices.Train
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Train#modelName
    * @propertyOf lbServices.Train
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Train`.
    */
    R.modelName = "Train";


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Container
 * @header lbServices.Container
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Container` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Container",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/containers/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Container#getContainers
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "getContainers": {
          isArray: true,
          url: urlBase + "/containers",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#createContainer
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "createContainer": {
          url: urlBase + "/containers",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#destroyContainer
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        "destroyContainer": {
          url: urlBase + "/containers/:container",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#getContainer
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "getContainer": {
          url: urlBase + "/containers/:container",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#getFiles
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "getFiles": {
          isArray: true,
          url: urlBase + "/containers/:container/files",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#getFile
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Container` object.)
         * </em>
         */
        "getFile": {
          url: urlBase + "/containers/:container/files/:file",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#removeFile
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        "removeFile": {
          url: urlBase + "/containers/:container/files/:file",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#upload
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `req` – `{object=}` - 
         *
         *  - `res` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `result` – `{object=}` - 
         */
        "upload": {
          url: urlBase + "/containers/:container/upload",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Container#download
         * @methodOf lbServices.Container
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         *  - `res` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "download": {
          url: urlBase + "/containers/:container/download/:file",
          method: "GET"
        },
      }
    );




    /**
    * @ngdoc property
    * @name lbServices.Container#modelName
    * @propertyOf lbServices.Container
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Container`.
    */
    R.modelName = "Container";


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.File
 * @header lbServices.File
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `File` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "File",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/files/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.File#getContainers
         * @methodOf lbServices.File
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `File` object.)
         * </em>
         */
        "getContainers": {
          isArray: true,
          url: urlBase + "/files",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.File#createContainer
         * @methodOf lbServices.File
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `File` object.)
         * </em>
         */
        "createContainer": {
          url: urlBase + "/files",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.File#destroyContainer
         * @methodOf lbServices.File
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        "destroyContainer": {
          url: urlBase + "/files/:container",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.File#getContainer
         * @methodOf lbServices.File
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `File` object.)
         * </em>
         */
        "getContainer": {
          url: urlBase + "/files/:container",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.File#getFiles
         * @methodOf lbServices.File
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `File` object.)
         * </em>
         */
        "getFiles": {
          isArray: true,
          url: urlBase + "/files/:container/files",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.File#getFile
         * @methodOf lbServices.File
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `File` object.)
         * </em>
         */
        "getFile": {
          url: urlBase + "/files/:container/files/:file",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.File#removeFile
         * @methodOf lbServices.File
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `` – `{undefined=}` - 
         */
        "removeFile": {
          url: urlBase + "/files/:container/files/:file",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.File#upload
         * @methodOf lbServices.File
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `req` – `{object=}` - 
         *
         *  - `res` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `result` – `{object=}` - 
         */
        "upload": {
          url: urlBase + "/files/:container/upload",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.File#download
         * @methodOf lbServices.File
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `container` – `{string=}` - 
         *
         *  - `file` – `{string=}` - 
         *
         *  - `res` – `{object=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "download": {
          url: urlBase + "/files/:container/download/:file",
          method: "GET"
        },
      }
    );




    /**
    * @ngdoc property
    * @name lbServices.File#modelName
    * @propertyOf lbServices.File
    * @description
    * The name of the model represented by this $resource,
    * i.e. `File`.
    */
    R.modelName = "File";


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Trip
 * @header lbServices.Trip
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Trip` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Trip",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/trips/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Trip.PNR() instead.
        "prototype$__get__PNR": {
          url: urlBase + "/trips/:id/PNR",
          method: "GET"
        },

        // INTERNAL. Use Trip.customer() instead.
        "prototype$__get__customer": {
          url: urlBase + "/trips/:id/customer",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Trip#create
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/trips",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Trip#upsert
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/trips",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Trip#exists
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/trips/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Trip#findById
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/trips/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Trip#find
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/trips",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Trip#findOne
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/trips/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Trip#updateAll
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/trips/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Trip#deleteById
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/trips/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Trip#count
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/trips/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Trip#prototype$updateAttributes
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/trips/:id",
          method: "PUT"
        },

        // INTERNAL. Use Customer.trips.findById() instead.
        "::findById::Customer::trips": {
          url: urlBase + "/customers/:id/trips/:fk",
          method: "GET"
        },

        // INTERNAL. Use Customer.trips.destroyById() instead.
        "::destroyById::Customer::trips": {
          url: urlBase + "/customers/:id/trips/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Customer.trips.updateById() instead.
        "::updateById::Customer::trips": {
          url: urlBase + "/customers/:id/trips/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Customer.trips() instead.
        "::get::Customer::trips": {
          isArray: true,
          url: urlBase + "/customers/:id/trips",
          method: "GET"
        },

        // INTERNAL. Use Customer.trips.create() instead.
        "::create::Customer::trips": {
          url: urlBase + "/customers/:id/trips",
          method: "POST"
        },

        // INTERNAL. Use Customer.trips.destroyAll() instead.
        "::delete::Customer::trips": {
          url: urlBase + "/customers/:id/trips",
          method: "DELETE"
        },

        // INTERNAL. Use Customer.trips.count() instead.
        "::count::Customer::trips": {
          url: urlBase + "/customers/:id/trips/count",
          method: "GET"
        },

        // INTERNAL. Use PNR.trips.findById() instead.
        "::findById::PNR::trips": {
          url: urlBase + "/pnrs/:id/trips/:fk",
          method: "GET"
        },

        // INTERNAL. Use PNR.trips.destroyById() instead.
        "::destroyById::PNR::trips": {
          url: urlBase + "/pnrs/:id/trips/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use PNR.trips.updateById() instead.
        "::updateById::PNR::trips": {
          url: urlBase + "/pnrs/:id/trips/:fk",
          method: "PUT"
        },

        // INTERNAL. Use PNR.trips() instead.
        "::get::PNR::trips": {
          isArray: true,
          url: urlBase + "/pnrs/:id/trips",
          method: "GET"
        },

        // INTERNAL. Use PNR.trips.create() instead.
        "::create::PNR::trips": {
          url: urlBase + "/pnrs/:id/trips",
          method: "POST"
        },

        // INTERNAL. Use PNR.trips.destroyAll() instead.
        "::delete::PNR::trips": {
          url: urlBase + "/pnrs/:id/trips",
          method: "DELETE"
        },

        // INTERNAL. Use PNR.trips.count() instead.
        "::count::PNR::trips": {
          url: urlBase + "/pnrs/:id/trips/count",
          method: "GET"
        },

        // INTERNAL. Use Alarm.trip() instead.
        "::get::Alarm::trip": {
          url: urlBase + "/alarms/:id/trip",
          method: "GET"
        },

        // INTERNAL. Use TrainRating.trip() instead.
        "::get::TrainRating::trip": {
          url: urlBase + "/trainratings/:id/trip",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Trip#updateOrCreate
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Trip#update
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Trip#destroyById
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Trip#removeById
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Trip#modelName
    * @propertyOf lbServices.Trip
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Trip`.
    */
    R.modelName = "Trip";


        /**
         * @ngdoc method
         * @name lbServices.Trip#PNR
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Fetches belongsTo relation PNR
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PNR` object.)
         * </em>
         */
        R.PNR = function() {
          var TargetResource = $injector.get("PNR");
          var action = TargetResource["::get::trip::PNR"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Trip#customer
         * @methodOf lbServices.Trip
         *
         * @description
         *
         * Fetches belongsTo relation customer
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Customer` object.)
         * </em>
         */
        R.customer = function() {
          var TargetResource = $injector.get("Customer");
          var action = TargetResource["::get::trip::customer"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Alarm
 * @header lbServices.Alarm
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Alarm` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Alarm",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/alarms/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Alarm.customer() instead.
        "prototype$__get__customer": {
          url: urlBase + "/alarms/:id/customer",
          method: "GET"
        },

        // INTERNAL. Use Alarm.trip() instead.
        "prototype$__get__trip": {
          url: urlBase + "/alarms/:id/trip",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Alarm#create
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Alarm` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/alarms",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Alarm#upsert
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Alarm` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/alarms",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Alarm#exists
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/alarms/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Alarm#findById
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Alarm` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/alarms/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Alarm#find
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Alarm` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/alarms",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Alarm#findOne
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Alarm` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/alarms/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Alarm#updateAll
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/alarms/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Alarm#deleteById
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/alarms/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Alarm#count
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/alarms/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Alarm#prototype$updateAttributes
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Alarm` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/alarms/:id",
          method: "PUT"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Alarm#updateOrCreate
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Alarm` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Alarm#update
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Alarm#destroyById
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Alarm#removeById
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Alarm#modelName
    * @propertyOf lbServices.Alarm
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Alarm`.
    */
    R.modelName = "Alarm";


        /**
         * @ngdoc method
         * @name lbServices.Alarm#customer
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Fetches belongsTo relation customer
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Customer` object.)
         * </em>
         */
        R.customer = function() {
          var TargetResource = $injector.get("Customer");
          var action = TargetResource["::get::Alarm::customer"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Alarm#trip
         * @methodOf lbServices.Alarm
         *
         * @description
         *
         * Fetches belongsTo relation trip
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        R.trip = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::get::Alarm::trip"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.TrainRating
 * @header lbServices.TrainRating
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `TrainRating` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "TrainRating",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/trainratings/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use TrainRating.PNR() instead.
        "prototype$__get__PNR": {
          url: urlBase + "/trainratings/:id/PNR",
          method: "GET"
        },

        // INTERNAL. Use TrainRating.trip() instead.
        "prototype$__get__trip": {
          url: urlBase + "/trainratings/:id/trip",
          method: "GET"
        },

        // INTERNAL. Use TrainRating.train() instead.
        "prototype$__get__train": {
          url: urlBase + "/trainratings/:id/train",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#create
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRating` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/trainratings",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#upsert
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRating` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/trainratings",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#exists
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/trainratings/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#findById
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRating` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/trainratings/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#find
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRating` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/trainratings",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#findOne
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRating` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/trainratings/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#updateAll
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/trainratings/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#deleteById
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/trainratings/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#count
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/trainratings/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#prototype$updateAttributes
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRating` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/trainratings/:id",
          method: "PUT"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.TrainRating#updateOrCreate
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRating` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#update
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#destroyById
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#removeById
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.TrainRating#modelName
    * @propertyOf lbServices.TrainRating
    * @description
    * The name of the model represented by this $resource,
    * i.e. `TrainRating`.
    */
    R.modelName = "TrainRating";


        /**
         * @ngdoc method
         * @name lbServices.TrainRating#PNR
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Fetches belongsTo relation PNR
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `PNR` object.)
         * </em>
         */
        R.PNR = function() {
          var TargetResource = $injector.get("PNR");
          var action = TargetResource["::get::TrainRating::PNR"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#trip
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Fetches belongsTo relation trip
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Trip` object.)
         * </em>
         */
        R.trip = function() {
          var TargetResource = $injector.get("Trip");
          var action = TargetResource["::get::TrainRating::trip"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.TrainRating#train
         * @methodOf lbServices.TrainRating
         *
         * @description
         *
         * Fetches belongsTo relation train
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Train` object.)
         * </em>
         */
        R.train = function() {
          var TargetResource = $injector.get("Train");
          var action = TargetResource["::get::TrainRating::train"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Cab
 * @header lbServices.Cab
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Cab` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Cab",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/cabs/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Cab#create
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cab` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/cabs",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Cab#upsert
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cab` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/cabs",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Cab#exists
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/cabs/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Cab#findById
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cab` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/cabs/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Cab#find
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cab` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/cabs",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Cab#findOne
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cab` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/cabs/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Cab#updateAll
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/cabs/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Cab#deleteById
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/cabs/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Cab#count
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/cabs/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Cab#prototype$updateAttributes
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cab` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/cabs/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Cab#insertMany
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `data` – `{array=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "insertMany": {
          url: urlBase + "/cabs/insertMany",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Cab#updateOrCreate
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Cab` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Cab#update
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Cab#destroyById
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Cab#removeById
         * @methodOf lbServices.Cab
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Cab#modelName
    * @propertyOf lbServices.Cab
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Cab`.
    */
    R.modelName = "Cab";


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Category
 * @header lbServices.Category
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Category` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Category",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/categories/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Category.subcategories.findById() instead.
        "prototype$__findById__subcategories": {
          url: urlBase + "/categories/:id/subcategories/:fk",
          method: "GET"
        },

        // INTERNAL. Use Category.subcategories.destroyById() instead.
        "prototype$__destroyById__subcategories": {
          url: urlBase + "/categories/:id/subcategories/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Category.subcategories.updateById() instead.
        "prototype$__updateById__subcategories": {
          url: urlBase + "/categories/:id/subcategories/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Category.subcategories() instead.
        "prototype$__get__subcategories": {
          isArray: true,
          url: urlBase + "/categories/:id/subcategories",
          method: "GET"
        },

        // INTERNAL. Use Category.subcategories.create() instead.
        "prototype$__create__subcategories": {
          url: urlBase + "/categories/:id/subcategories",
          method: "POST"
        },

        // INTERNAL. Use Category.subcategories.destroyAll() instead.
        "prototype$__delete__subcategories": {
          url: urlBase + "/categories/:id/subcategories",
          method: "DELETE"
        },

        // INTERNAL. Use Category.subcategories.count() instead.
        "prototype$__count__subcategories": {
          url: urlBase + "/categories/:id/subcategories/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Category#create
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Category` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/categories",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Category#upsert
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Category` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/categories",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Category#exists
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/categories/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Category#findById
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Category` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/categories/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Category#find
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Category` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/categories",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Category#findOne
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Category` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/categories/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Category#updateAll
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/categories/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Category#deleteById
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/categories/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Category#count
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/categories/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Category#prototype$updateAttributes
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Category` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/categories/:id",
          method: "PUT"
        },

        // INTERNAL. Use SubCategory.category() instead.
        "::get::SubCategory::category": {
          url: urlBase + "/subcategories/:id/category",
          method: "GET"
        },

        // INTERNAL. Use Item.category() instead.
        "::get::Item::category": {
          url: urlBase + "/items/:id/category",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Category#updateOrCreate
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Category` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Category#update
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Category#destroyById
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Category#removeById
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Category#modelName
    * @propertyOf lbServices.Category
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Category`.
    */
    R.modelName = "Category";

    /**
     * @ngdoc object
     * @name lbServices.Category.subcategories
     * @header lbServices.Category.subcategories
     * @object
     * @description
     *
     * The object `Category.subcategories` groups methods
     * manipulating `SubCategory` instances related to `Category`.
     *
     * Call {@link lbServices.Category#subcategories Category.subcategories()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Category#subcategories
         * @methodOf lbServices.Category
         *
         * @description
         *
         * Queries subcategories of Category.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SubCategory` object.)
         * </em>
         */
        R.subcategories = function() {
          var TargetResource = $injector.get("SubCategory");
          var action = TargetResource["::get::Category::subcategories"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Category.subcategories#count
         * @methodOf lbServices.Category.subcategories
         *
         * @description
         *
         * Counts subcategories of Category.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.subcategories.count = function() {
          var TargetResource = $injector.get("SubCategory");
          var action = TargetResource["::count::Category::subcategories"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Category.subcategories#create
         * @methodOf lbServices.Category.subcategories
         *
         * @description
         *
         * Creates a new instance in subcategories of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SubCategory` object.)
         * </em>
         */
        R.subcategories.create = function() {
          var TargetResource = $injector.get("SubCategory");
          var action = TargetResource["::create::Category::subcategories"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Category.subcategories#destroyAll
         * @methodOf lbServices.Category.subcategories
         *
         * @description
         *
         * Deletes all subcategories of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.subcategories.destroyAll = function() {
          var TargetResource = $injector.get("SubCategory");
          var action = TargetResource["::delete::Category::subcategories"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Category.subcategories#destroyById
         * @methodOf lbServices.Category.subcategories
         *
         * @description
         *
         * Delete a related item by id for subcategories
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for subcategories
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.subcategories.destroyById = function() {
          var TargetResource = $injector.get("SubCategory");
          var action = TargetResource["::destroyById::Category::subcategories"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Category.subcategories#findById
         * @methodOf lbServices.Category.subcategories
         *
         * @description
         *
         * Find a related item by id for subcategories
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for subcategories
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SubCategory` object.)
         * </em>
         */
        R.subcategories.findById = function() {
          var TargetResource = $injector.get("SubCategory");
          var action = TargetResource["::findById::Category::subcategories"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Category.subcategories#updateById
         * @methodOf lbServices.Category.subcategories
         *
         * @description
         *
         * Update a related item by id for subcategories
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for subcategories
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SubCategory` object.)
         * </em>
         */
        R.subcategories.updateById = function() {
          var TargetResource = $injector.get("SubCategory");
          var action = TargetResource["::updateById::Category::subcategories"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.SubCategory
 * @header lbServices.SubCategory
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `SubCategory` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "SubCategory",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/subcategories/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use SubCategory.category() instead.
        "prototype$__get__category": {
          url: urlBase + "/subcategories/:id/category",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.SubCategory#create
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SubCategory` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/subcategories",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.SubCategory#upsert
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SubCategory` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/subcategories",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.SubCategory#exists
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/subcategories/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.SubCategory#findById
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SubCategory` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/subcategories/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.SubCategory#find
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SubCategory` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/subcategories",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.SubCategory#findOne
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SubCategory` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/subcategories/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.SubCategory#updateAll
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/subcategories/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.SubCategory#deleteById
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/subcategories/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.SubCategory#count
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/subcategories/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.SubCategory#prototype$updateAttributes
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SubCategory` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/subcategories/:id",
          method: "PUT"
        },

        // INTERNAL. Use Category.subcategories.findById() instead.
        "::findById::Category::subcategories": {
          url: urlBase + "/categories/:id/subcategories/:fk",
          method: "GET"
        },

        // INTERNAL. Use Category.subcategories.destroyById() instead.
        "::destroyById::Category::subcategories": {
          url: urlBase + "/categories/:id/subcategories/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Category.subcategories.updateById() instead.
        "::updateById::Category::subcategories": {
          url: urlBase + "/categories/:id/subcategories/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Category.subcategories() instead.
        "::get::Category::subcategories": {
          isArray: true,
          url: urlBase + "/categories/:id/subcategories",
          method: "GET"
        },

        // INTERNAL. Use Category.subcategories.create() instead.
        "::create::Category::subcategories": {
          url: urlBase + "/categories/:id/subcategories",
          method: "POST"
        },

        // INTERNAL. Use Category.subcategories.destroyAll() instead.
        "::delete::Category::subcategories": {
          url: urlBase + "/categories/:id/subcategories",
          method: "DELETE"
        },

        // INTERNAL. Use Category.subcategories.count() instead.
        "::count::Category::subcategories": {
          url: urlBase + "/categories/:id/subcategories/count",
          method: "GET"
        },

        // INTERNAL. Use Item.subcategory() instead.
        "::get::Item::subcategory": {
          url: urlBase + "/items/:id/subcategory",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.SubCategory#updateOrCreate
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SubCategory` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.SubCategory#update
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.SubCategory#destroyById
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.SubCategory#removeById
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.SubCategory#modelName
    * @propertyOf lbServices.SubCategory
    * @description
    * The name of the model represented by this $resource,
    * i.e. `SubCategory`.
    */
    R.modelName = "SubCategory";


        /**
         * @ngdoc method
         * @name lbServices.SubCategory#category
         * @methodOf lbServices.SubCategory
         *
         * @description
         *
         * Fetches belongsTo relation category
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Category` object.)
         * </em>
         */
        R.category = function() {
          var TargetResource = $injector.get("Category");
          var action = TargetResource["::get::SubCategory::category"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Restaurant
 * @header lbServices.Restaurant
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Restaurant` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Restaurant",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/restaurants/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Restaurant.restaurantReviews.findById() instead.
        "prototype$__findById__restaurantReviews": {
          url: urlBase + "/restaurants/:id/restaurantReviews/:fk",
          method: "GET"
        },

        // INTERNAL. Use Restaurant.restaurantReviews.destroyById() instead.
        "prototype$__destroyById__restaurantReviews": {
          url: urlBase + "/restaurants/:id/restaurantReviews/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Restaurant.restaurantReviews.updateById() instead.
        "prototype$__updateById__restaurantReviews": {
          url: urlBase + "/restaurants/:id/restaurantReviews/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Restaurant.restaurantReviews() instead.
        "prototype$__get__restaurantReviews": {
          isArray: true,
          url: urlBase + "/restaurants/:id/restaurantReviews",
          method: "GET"
        },

        // INTERNAL. Use Restaurant.restaurantReviews.create() instead.
        "prototype$__create__restaurantReviews": {
          url: urlBase + "/restaurants/:id/restaurantReviews",
          method: "POST"
        },

        // INTERNAL. Use Restaurant.restaurantReviews.destroyAll() instead.
        "prototype$__delete__restaurantReviews": {
          url: urlBase + "/restaurants/:id/restaurantReviews",
          method: "DELETE"
        },

        // INTERNAL. Use Restaurant.restaurantReviews.count() instead.
        "prototype$__count__restaurantReviews": {
          url: urlBase + "/restaurants/:id/restaurantReviews/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#create
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Restaurant` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/restaurants",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#upsert
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Restaurant` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/restaurants",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#exists
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/restaurants/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#findById
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Restaurant` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/restaurants/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#find
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Restaurant` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/restaurants",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#findOne
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Restaurant` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/restaurants/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#updateAll
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/restaurants/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#deleteById
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/restaurants/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#count
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/restaurants/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#prototype$updateAttributes
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Restaurant` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/restaurants/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#insertMany
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `data` – `{array=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "insertMany": {
          url: urlBase + "/restaurants/insertMany",
          method: "POST"
        },

        // INTERNAL. Use Item.restaurant() instead.
        "::get::Item::restaurant": {
          url: urlBase + "/items/:id/restaurant",
          method: "GET"
        },

        // INTERNAL. Use Order.restaurant() instead.
        "::get::Order::restaurant": {
          url: urlBase + "/orders/:id/restaurant",
          method: "GET"
        },

        // INTERNAL. Use RestaurantReview.restaurant() instead.
        "::get::RestaurantReview::restaurant": {
          url: urlBase + "/restaurantReviews/:id/restaurant",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Restaurant#updateOrCreate
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Restaurant` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#update
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#destroyById
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Restaurant#removeById
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Restaurant#modelName
    * @propertyOf lbServices.Restaurant
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Restaurant`.
    */
    R.modelName = "Restaurant";

    /**
     * @ngdoc object
     * @name lbServices.Restaurant.restaurantReviews
     * @header lbServices.Restaurant.restaurantReviews
     * @object
     * @description
     *
     * The object `Restaurant.restaurantReviews` groups methods
     * manipulating `RestaurantReview` instances related to `Restaurant`.
     *
     * Call {@link lbServices.Restaurant#restaurantReviews Restaurant.restaurantReviews()}
     * to query all related instances.
     */


        /**
         * @ngdoc method
         * @name lbServices.Restaurant#restaurantReviews
         * @methodOf lbServices.Restaurant
         *
         * @description
         *
         * Queries restaurantReviews of Restaurant.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RestaurantReview` object.)
         * </em>
         */
        R.restaurantReviews = function() {
          var TargetResource = $injector.get("RestaurantReview");
          var action = TargetResource["::get::Restaurant::restaurantReviews"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Restaurant.restaurantReviews#count
         * @methodOf lbServices.Restaurant.restaurantReviews
         *
         * @description
         *
         * Counts restaurantReviews of Restaurant.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        R.restaurantReviews.count = function() {
          var TargetResource = $injector.get("RestaurantReview");
          var action = TargetResource["::count::Restaurant::restaurantReviews"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Restaurant.restaurantReviews#create
         * @methodOf lbServices.Restaurant.restaurantReviews
         *
         * @description
         *
         * Creates a new instance in restaurantReviews of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RestaurantReview` object.)
         * </em>
         */
        R.restaurantReviews.create = function() {
          var TargetResource = $injector.get("RestaurantReview");
          var action = TargetResource["::create::Restaurant::restaurantReviews"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Restaurant.restaurantReviews#destroyAll
         * @methodOf lbServices.Restaurant.restaurantReviews
         *
         * @description
         *
         * Deletes all restaurantReviews of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.restaurantReviews.destroyAll = function() {
          var TargetResource = $injector.get("RestaurantReview");
          var action = TargetResource["::delete::Restaurant::restaurantReviews"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Restaurant.restaurantReviews#destroyById
         * @methodOf lbServices.Restaurant.restaurantReviews
         *
         * @description
         *
         * Delete a related item by id for restaurantReviews
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for restaurantReviews
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R.restaurantReviews.destroyById = function() {
          var TargetResource = $injector.get("RestaurantReview");
          var action = TargetResource["::destroyById::Restaurant::restaurantReviews"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Restaurant.restaurantReviews#findById
         * @methodOf lbServices.Restaurant.restaurantReviews
         *
         * @description
         *
         * Find a related item by id for restaurantReviews
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for restaurantReviews
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RestaurantReview` object.)
         * </em>
         */
        R.restaurantReviews.findById = function() {
          var TargetResource = $injector.get("RestaurantReview");
          var action = TargetResource["::findById::Restaurant::restaurantReviews"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Restaurant.restaurantReviews#updateById
         * @methodOf lbServices.Restaurant.restaurantReviews
         *
         * @description
         *
         * Update a related item by id for restaurantReviews
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `fk` – `{*}` - Foreign key for restaurantReviews
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RestaurantReview` object.)
         * </em>
         */
        R.restaurantReviews.updateById = function() {
          var TargetResource = $injector.get("RestaurantReview");
          var action = TargetResource["::updateById::Restaurant::restaurantReviews"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Item
 * @header lbServices.Item
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Item` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Item",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/items/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Item.category() instead.
        "prototype$__get__category": {
          url: urlBase + "/items/:id/category",
          method: "GET"
        },

        // INTERNAL. Use Item.subcategory() instead.
        "prototype$__get__subcategory": {
          url: urlBase + "/items/:id/subcategory",
          method: "GET"
        },

        // INTERNAL. Use Item.restaurant() instead.
        "prototype$__get__restaurant": {
          url: urlBase + "/items/:id/restaurant",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Item#create
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Item` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/items",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Item#upsert
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Item` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/items",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Item#exists
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/items/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Item#findById
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Item` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/items/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Item#find
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Item` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/items",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Item#findOne
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Item` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/items/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Item#updateAll
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/items/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Item#deleteById
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/items/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Item#count
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/items/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Item#prototype$updateAttributes
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Item` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/items/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Item#insertMany
         * @methodOf lbServices.Item
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `data` – `{array=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "insertMany": {
          url: urlBase + "/items/insertMany",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Item#updateOrCreate
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Item` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Item#update
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Item#destroyById
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Item#removeById
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Item#modelName
    * @propertyOf lbServices.Item
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Item`.
    */
    R.modelName = "Item";


        /**
         * @ngdoc method
         * @name lbServices.Item#category
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Fetches belongsTo relation category
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Category` object.)
         * </em>
         */
        R.category = function() {
          var TargetResource = $injector.get("Category");
          var action = TargetResource["::get::Item::category"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Item#subcategory
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Fetches belongsTo relation subcategory
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `SubCategory` object.)
         * </em>
         */
        R.subcategory = function() {
          var TargetResource = $injector.get("SubCategory");
          var action = TargetResource["::get::Item::subcategory"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Item#restaurant
         * @methodOf lbServices.Item
         *
         * @description
         *
         * Fetches belongsTo relation restaurant
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Restaurant` object.)
         * </em>
         */
        R.restaurant = function() {
          var TargetResource = $injector.get("Restaurant");
          var action = TargetResource["::get::Item::restaurant"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Hotel
 * @header lbServices.Hotel
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Hotel` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Hotel",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/hotels/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Hotel#create
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Hotel` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/hotels",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Hotel#upsert
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Hotel` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/hotels",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Hotel#exists
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/hotels/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Hotel#findById
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Hotel` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/hotels/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Hotel#find
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Hotel` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/hotels",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Hotel#findOne
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Hotel` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/hotels/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Hotel#updateAll
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/hotels/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Hotel#deleteById
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/hotels/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Hotel#count
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/hotels/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Hotel#prototype$updateAttributes
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Hotel` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/hotels/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Hotel#insertMany
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `data` – `{array=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "insertMany": {
          url: urlBase + "/hotels/insertMany",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Hotel#updateOrCreate
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Hotel` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Hotel#update
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Hotel#destroyById
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Hotel#removeById
         * @methodOf lbServices.Hotel
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Hotel#modelName
    * @propertyOf lbServices.Hotel
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Hotel`.
    */
    R.modelName = "Hotel";


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Order
 * @header lbServices.Order
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Order` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Order",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/orders/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use Order.customer() instead.
        "prototype$__get__customer": {
          url: urlBase + "/orders/:id/customer",
          method: "GET"
        },

        // INTERNAL. Use Order.restaurant() instead.
        "prototype$__get__restaurant": {
          url: urlBase + "/orders/:id/restaurant",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Order#create
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Order` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/orders",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Order#upsert
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Order` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/orders",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Order#exists
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/orders/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Order#findById
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Order` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/orders/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Order#find
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Order` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/orders",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Order#findOne
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Order` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/orders/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Order#updateAll
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/orders/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Order#deleteById
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/orders/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Order#count
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/orders/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Order#prototype$updateAttributes
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Order` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/orders/:id",
          method: "PUT"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Order#updateOrCreate
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Order` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Order#update
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Order#destroyById
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Order#removeById
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.Order#modelName
    * @propertyOf lbServices.Order
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Order`.
    */
    R.modelName = "Order";


        /**
         * @ngdoc method
         * @name lbServices.Order#customer
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Fetches belongsTo relation customer
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Customer` object.)
         * </em>
         */
        R.customer = function() {
          var TargetResource = $injector.get("Customer");
          var action = TargetResource["::get::Order::customer"];
          return action.apply(R, arguments);
        };

        /**
         * @ngdoc method
         * @name lbServices.Order#restaurant
         * @methodOf lbServices.Order
         *
         * @description
         *
         * Fetches belongsTo relation restaurant
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Restaurant` object.)
         * </em>
         */
        R.restaurant = function() {
          var TargetResource = $injector.get("Restaurant");
          var action = TargetResource["::get::Order::restaurant"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.Users
 * @header lbServices.Users
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `Users` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "Users",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/users/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.Users#prototype$__findById__accessTokens
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Find a related item by id for accessTokens
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Users` object.)
         * </em>
         */
        "prototype$__findById__accessTokens": {
          url: urlBase + "/users/:id/accessTokens/:fk",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#prototype$__destroyById__accessTokens
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Delete a related item by id for accessTokens
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "prototype$__destroyById__accessTokens": {
          url: urlBase + "/users/:id/accessTokens/:fk",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#prototype$__updateById__accessTokens
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Update a related item by id for accessTokens
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `fk` – `{*}` - Foreign key for accessTokens
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Users` object.)
         * </em>
         */
        "prototype$__updateById__accessTokens": {
          url: urlBase + "/users/:id/accessTokens/:fk",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#prototype$__get__accessTokens
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Queries accessTokens of Users.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `filter` – `{object=}` - 
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Users` object.)
         * </em>
         */
        "prototype$__get__accessTokens": {
          isArray: true,
          url: urlBase + "/users/:id/accessTokens",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#prototype$__create__accessTokens
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Creates a new instance in accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Users` object.)
         * </em>
         */
        "prototype$__create__accessTokens": {
          url: urlBase + "/users/:id/accessTokens",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#prototype$__delete__accessTokens
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Deletes all accessTokens of this model.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "prototype$__delete__accessTokens": {
          url: urlBase + "/users/:id/accessTokens",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#prototype$__count__accessTokens
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Counts accessTokens of Users.
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "prototype$__count__accessTokens": {
          url: urlBase + "/users/:id/accessTokens/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#create
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Users` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/users",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#upsert
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Users` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/users",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#exists
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/users/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#findById
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Users` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/users/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#find
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Users` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/users",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#findOne
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Users` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/users/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#updateAll
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/users/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#deleteById
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/users/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#count
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/users/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#prototype$updateAttributes
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - User id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Users` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/users/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#login
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Login a user with username/email and password
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `include` – `{string=}` - Related objects to include in the response. See the description of return value for more details.
         *   Default value: `user`.
         *
         *  - `rememberMe` - `boolean` - Whether the authentication credentials
         *     should be remembered in localStorage across app/browser restarts.
         *     Default: `true`.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * The response body contains properties of the AccessToken created on login.
         * Depending on the value of `include` parameter, the body may contain additional properties:
         * 
         *   - `user` - `{User}` - Data of the currently logged in user. (`include=user`)
         * 
         *
         */
        "login": {
          params: {
            include: "user"
          },
          interceptor: {
            response: function(response) {
              var accessToken = response.data;
              LoopBackAuth.setUser(accessToken.id, accessToken.userId, accessToken.user);
              LoopBackAuth.rememberMe = response.config.params.rememberMe !== false;
              LoopBackAuth.save();
              return response.resource;
            }
          },
          url: urlBase + "/users/login",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#logout
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Logout a user with access token
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `access_token` – `{string}` - Do not supply this argument, it is automatically extracted from request headers.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "logout": {
          interceptor: {
            response: function(response) {
              LoopBackAuth.clearUser();
              LoopBackAuth.clearStorage();
              return response.resource;
            }
          },
          url: urlBase + "/users/logout",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#confirm
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Confirm a user registration with email verification token
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `uid` – `{string}` - 
         *
         *  - `token` – `{string}` - 
         *
         *  - `redirect` – `{string}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "confirm": {
          url: urlBase + "/users/confirm",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#resetPassword
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Reset password for a user with email
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "resetPassword": {
          url: urlBase + "/users/reset",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.Users#getCurrent
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Get data of the currently logged user. Fail with HTTP result 401
         * when there is no user logged in.
         *
         * @param {function(Object,Object)=} successCb
         *    Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *    `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         */
        "getCurrent": {
           url: urlBase + "/users" + "/:id",
           method: "GET",
           params: {
             id: function() {
              var id = LoopBackAuth.currentUserId;
              if (id == null) id = '__anonymous__';
              return id;
            },
          },
          interceptor: {
            response: function(response) {
              LoopBackAuth.currentUserData = response.data;
              return response.resource;
            }
          },
          __isGetCurrentUser__ : true
        }
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.Users#updateOrCreate
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Users` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.Users#update
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.Users#destroyById
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Users#removeById
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.Users#getCachedCurrent
         * @methodOf lbServices.Users
         *
         * @description
         *
         * Get data of the currently logged user that was returned by the last
         * call to {@link lbServices.Users#login} or
         * {@link lbServices.Users#getCurrent}. Return null when there
         * is no user logged in or the data of the current user were not fetched
         * yet.
         *
         * @returns {Object} A Users instance.
         */
        R.getCachedCurrent = function() {
          var data = LoopBackAuth.currentUserData;
          return data ? new R(data) : null;
        };

        /**
         * @ngdoc method
         * @name lbServices.Users#isAuthenticated
         * @methodOf lbServices.Users
         *
         * @returns {boolean} True if the current user is authenticated (logged in).
         */
        R.isAuthenticated = function() {
          return this.getCurrentId() != null;
        };

        /**
         * @ngdoc method
         * @name lbServices.Users#getCurrentId
         * @methodOf lbServices.Users
         *
         * @returns {Object} Id of the currently logged-in user or null.
         */
        R.getCurrentId = function() {
          return LoopBackAuth.currentUserId;
        };

    /**
    * @ngdoc property
    * @name lbServices.Users#modelName
    * @propertyOf lbServices.Users
    * @description
    * The name of the model represented by this $resource,
    * i.e. `Users`.
    */
    R.modelName = "Users";


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.RestaurantReview
 * @header lbServices.RestaurantReview
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `RestaurantReview` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "RestaurantReview",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/restaurantReviews/:id",
      { 'id': '@id' },
      {

        // INTERNAL. Use RestaurantReview.restaurant() instead.
        "prototype$__get__restaurant": {
          url: urlBase + "/restaurantReviews/:id/restaurant",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#create
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RestaurantReview` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/restaurantReviews",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#upsert
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RestaurantReview` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/restaurantReviews",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#exists
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/restaurantReviews/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#findById
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RestaurantReview` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/restaurantReviews/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#find
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RestaurantReview` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/restaurantReviews",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#findOne
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RestaurantReview` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/restaurantReviews/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#updateAll
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/restaurantReviews/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#deleteById
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/restaurantReviews/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#count
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/restaurantReviews/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#prototype$updateAttributes
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RestaurantReview` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/restaurantReviews/:id",
          method: "PUT"
        },

        // INTERNAL. Use Restaurant.restaurantReviews.findById() instead.
        "::findById::Restaurant::restaurantReviews": {
          url: urlBase + "/restaurants/:id/restaurantReviews/:fk",
          method: "GET"
        },

        // INTERNAL. Use Restaurant.restaurantReviews.destroyById() instead.
        "::destroyById::Restaurant::restaurantReviews": {
          url: urlBase + "/restaurants/:id/restaurantReviews/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Restaurant.restaurantReviews.updateById() instead.
        "::updateById::Restaurant::restaurantReviews": {
          url: urlBase + "/restaurants/:id/restaurantReviews/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Restaurant.restaurantReviews() instead.
        "::get::Restaurant::restaurantReviews": {
          isArray: true,
          url: urlBase + "/restaurants/:id/restaurantReviews",
          method: "GET"
        },

        // INTERNAL. Use Restaurant.restaurantReviews.create() instead.
        "::create::Restaurant::restaurantReviews": {
          url: urlBase + "/restaurants/:id/restaurantReviews",
          method: "POST"
        },

        // INTERNAL. Use Restaurant.restaurantReviews.destroyAll() instead.
        "::delete::Restaurant::restaurantReviews": {
          url: urlBase + "/restaurants/:id/restaurantReviews",
          method: "DELETE"
        },

        // INTERNAL. Use Restaurant.restaurantReviews.count() instead.
        "::count::Restaurant::restaurantReviews": {
          url: urlBase + "/restaurants/:id/restaurantReviews/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#updateOrCreate
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `RestaurantReview` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#update
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#destroyById
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#removeById
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.RestaurantReview#modelName
    * @propertyOf lbServices.RestaurantReview
    * @description
    * The name of the model represented by this $resource,
    * i.e. `RestaurantReview`.
    */
    R.modelName = "RestaurantReview";


        /**
         * @ngdoc method
         * @name lbServices.RestaurantReview#restaurant
         * @methodOf lbServices.RestaurantReview
         *
         * @description
         *
         * Fetches belongsTo relation restaurant
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         *  - `refresh` – `{boolean=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `Restaurant` object.)
         * </em>
         */
        R.restaurant = function() {
          var TargetResource = $injector.get("Restaurant");
          var action = TargetResource["::get::RestaurantReview::restaurant"];
          return action.apply(R, arguments);
        };

    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.StationReview
 * @header lbServices.StationReview
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `StationReview` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "StationReview",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/stationReviews/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.StationReview#create
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationReview` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/stationReviews",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationReview#upsert
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationReview` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/stationReviews",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationReview#exists
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/stationReviews/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationReview#findById
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationReview` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/stationReviews/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationReview#find
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationReview` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/stationReviews",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationReview#findOne
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationReview` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/stationReviews/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationReview#updateAll
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/stationReviews/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationReview#deleteById
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/stationReviews/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationReview#count
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/stationReviews/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.StationReview#prototype$updateAttributes
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationReview` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/stationReviews/:id",
          method: "PUT"
        },

        // INTERNAL. Use Station.stationReviews.findById() instead.
        "::findById::Station::stationReviews": {
          url: urlBase + "/stations/:id/stationReviews/:fk",
          method: "GET"
        },

        // INTERNAL. Use Station.stationReviews.destroyById() instead.
        "::destroyById::Station::stationReviews": {
          url: urlBase + "/stations/:id/stationReviews/:fk",
          method: "DELETE"
        },

        // INTERNAL. Use Station.stationReviews.updateById() instead.
        "::updateById::Station::stationReviews": {
          url: urlBase + "/stations/:id/stationReviews/:fk",
          method: "PUT"
        },

        // INTERNAL. Use Station.stationReviews() instead.
        "::get::Station::stationReviews": {
          isArray: true,
          url: urlBase + "/stations/:id/stationReviews",
          method: "GET"
        },

        // INTERNAL. Use Station.stationReviews.create() instead.
        "::create::Station::stationReviews": {
          url: urlBase + "/stations/:id/stationReviews",
          method: "POST"
        },

        // INTERNAL. Use Station.stationReviews.destroyAll() instead.
        "::delete::Station::stationReviews": {
          url: urlBase + "/stations/:id/stationReviews",
          method: "DELETE"
        },

        // INTERNAL. Use Station.stationReviews.count() instead.
        "::count::Station::stationReviews": {
          url: urlBase + "/stations/:id/stationReviews/count",
          method: "GET"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.StationReview#updateOrCreate
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `StationReview` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.StationReview#update
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.StationReview#destroyById
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.StationReview#removeById
         * @methodOf lbServices.StationReview
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.StationReview#modelName
    * @propertyOf lbServices.StationReview
    * @description
    * The name of the model represented by this $resource,
    * i.e. `StationReview`.
    */
    R.modelName = "StationReview";


    return R;
  }]);

/**
 * @ngdoc object
 * @name lbServices.TrainRoute
 * @header lbServices.TrainRoute
 * @object
 *
 * @description
 *
 * A $resource object for interacting with the `TrainRoute` model.
 *
 * ## Example
 *
 * See
 * {@link http://docs.angularjs.org/api/ngResource.$resource#example $resource}
 * for an example of using this object.
 *
 */
module.factory(
  "TrainRoute",
  ['LoopBackResource', 'LoopBackAuth', '$injector', function(Resource, LoopBackAuth, $injector) {
    var R = Resource(
      urlBase + "/trainroutes/:id",
      { 'id': '@id' },
      {

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#create
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Create a new instance of the model and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRoute` object.)
         * </em>
         */
        "create": {
          url: urlBase + "/trainroutes",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#upsert
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRoute` object.)
         * </em>
         */
        "upsert": {
          url: urlBase + "/trainroutes",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#exists
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Check whether a model instance exists in the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `exists` – `{boolean=}` - 
         */
        "exists": {
          url: urlBase + "/trainroutes/:id/exists",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#findById
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Find a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRoute` object.)
         * </em>
         */
        "findById": {
          url: urlBase + "/trainroutes/:id",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#find
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Find all instances of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Array.<Object>,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Array.<Object>} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRoute` object.)
         * </em>
         */
        "find": {
          isArray: true,
          url: urlBase + "/trainroutes",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#findOne
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Find first instance of the model matched by filter from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `filter` – `{object=}` - Filter defining fields, where, orderBy, offset, and limit
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRoute` object.)
         * </em>
         */
        "findOne": {
          url: urlBase + "/trainroutes/findOne",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#updateAll
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "updateAll": {
          url: urlBase + "/trainroutes/update",
          method: "POST"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#deleteById
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "deleteById": {
          url: urlBase + "/trainroutes/:id",
          method: "DELETE"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#count
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Count instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * Data properties:
         *
         *  - `count` – `{number=}` - 
         */
        "count": {
          url: urlBase + "/trainroutes/count",
          method: "GET"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#prototype$updateAttributes
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Update attributes for a model instance and persist it into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - PersistedModel id
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRoute` object.)
         * </em>
         */
        "prototype$updateAttributes": {
          url: urlBase + "/trainroutes/:id",
          method: "PUT"
        },

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#insertMany
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * <em>
         * (The remote method definition does not provide any description.)
         * </em>
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         *  - `data` – `{array=}` - 
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        "insertMany": {
          url: urlBase + "/trainroutes/insertMany",
          method: "POST"
        },
      }
    );



        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#updateOrCreate
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Update an existing model instance or insert a new one into the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *   This method does not accept any parameters.
         *   Supply an empty object or omit this argument altogether.
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * <em>
         * (The remote method definition does not provide any description.
         * This usually means the response is a `TrainRoute` object.)
         * </em>
         */
        R["updateOrCreate"] = R["upsert"];

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#update
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Update instances of the model matched by where from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `where` – `{object=}` - Criteria to match model instances
         *
         * @param {Object} postData Request data.
         *
         * This method expects a subset of model properties as request parameters.
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["update"] = R["updateAll"];

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#destroyById
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["destroyById"] = R["deleteById"];

        /**
         * @ngdoc method
         * @name lbServices.TrainRoute#removeById
         * @methodOf lbServices.TrainRoute
         *
         * @description
         *
         * Delete a model instance by id from the data source
         *
         * @param {Object=} parameters Request parameters.
         *
         *  - `id` – `{*}` - Model id
         *
         * @param {function(Object,Object)=} successCb
         *   Success callback with two arguments: `value`, `responseHeaders`.
         *
         * @param {function(Object)=} errorCb Error callback with one argument:
         *   `httpResponse`.
         *
         * @returns {Object} An empty reference that will be
         *   populated with the actual data once the response is returned
         *   from the server.
         *
         * This method returns no data.
         */
        R["removeById"] = R["deleteById"];


    /**
    * @ngdoc property
    * @name lbServices.TrainRoute#modelName
    * @propertyOf lbServices.TrainRoute
    * @description
    * The name of the model represented by this $resource,
    * i.e. `TrainRoute`.
    */
    R.modelName = "TrainRoute";


    return R;
  }]);


module
  .factory('LoopBackAuth', function() {
    var props = ['accessTokenId', 'currentUserId'];
    var propsPrefix = '$LoopBack$';

    function LoopBackAuth() {
      var self = this;
      props.forEach(function(name) {
        self[name] = load(name);
      });
      this.rememberMe = undefined;
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.save = function() {
      var self = this;
      var storage = this.rememberMe ? localStorage : sessionStorage;
      props.forEach(function(name) {
        save(storage, name, self[name]);
      });
    };

    LoopBackAuth.prototype.setUser = function(accessTokenId, userId, userData) {
      this.accessTokenId = accessTokenId;
      this.currentUserId = userId;
      this.currentUserData = userData;
    }

    LoopBackAuth.prototype.clearUser = function() {
      this.accessTokenId = null;
      this.currentUserId = null;
      this.currentUserData = null;
    }

    LoopBackAuth.prototype.clearStorage = function() {
      props.forEach(function(name) {
        save(sessionStorage, name, null);
        save(localStorage, name, null);
      });
    };

    return new LoopBackAuth();

    // Note: LocalStorage converts the value to string
    // We are using empty string as a marker for null/undefined values.
    function save(storage, name, value) {
      var key = propsPrefix + name;
      if (value == null) value = '';
      storage[key] = value;
    }

    function load(name) {
      var key = propsPrefix + name;
      return localStorage[key] || sessionStorage[key] || null;
    }
  })
  .config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('LoopBackAuthRequestInterceptor');
  }])
  .factory('LoopBackAuthRequestInterceptor', [ '$q', 'LoopBackAuth',
    function($q, LoopBackAuth) {
      return {
        'request': function(config) {

          // filter out non urlBase requests
          if (config.url.substr(0, urlBase.length) !== urlBase) {
            return config;
          }

          if (LoopBackAuth.accessTokenId) {
            config.headers[authHeader] = LoopBackAuth.accessTokenId;
          } else if (config.__isGetCurrentUser__) {
            // Return a stub 401 error for User.getCurrent() when
            // there is no user logged in
            var res = {
              body: { error: { status: 401 } },
              status: 401,
              config: config,
              headers: function() { return undefined; }
            };
            return $q.reject(res);
          }
          return config || $q.when(config);
        }
      }
    }])

  /**
   * @ngdoc object
   * @name lbServices.LoopBackResourceProvider
   * @header lbServices.LoopBackResourceProvider
   * @description
   * Use `LoopBackResourceProvider` to change the global configuration
   * settings used by all models. Note that the provider is available
   * to Configuration Blocks only, see
   * {@link https://docs.angularjs.org/guide/module#module-loading-dependencies Module Loading & Dependencies}
   * for more details.
   *
   * ## Example
   *
   * ```js
   * angular.module('app')
   *  .config(function(LoopBackResourceProvider) {
   *     LoopBackResourceProvider.setAuthHeader('X-Access-Token');
   *  });
   * ```
   */
  .provider('LoopBackResource', function LoopBackResourceProvider() {
    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#setAuthHeader
     * @methodOf lbServices.LoopBackResourceProvider
     * @param {string} header The header name to use, e.g. `X-Access-Token`
     * @description
     * Configure the REST transport to use a different header for sending
     * the authentication token. It is sent in the `Authorization` header
     * by default.
     */
    this.setAuthHeader = function(header) {
      authHeader = header;
    };

    /**
     * @ngdoc method
     * @name lbServices.LoopBackResourceProvider#setUrlBase
     * @methodOf lbServices.LoopBackResourceProvider
     * @param {string} url The URL to use, e.g. `/api` or `//example.com/api`.
     * @description
     * Change the URL of the REST API server. By default, the URL provided
     * to the code generator (`lb-ng` or `grunt-loopback-sdk-angular`) is used.
     */
    this.setUrlBase = function(url) {
      urlBase = url;
    };

    this.$get = ['$resource', function($resource) {
      return function(url, params, actions) {
        var resource = $resource(url, params, actions);

        // Angular always calls POST on $save()
        // This hack is based on
        // http://kirkbushell.me/angular-js-using-ng-resource-in-a-more-restful-manner/
        resource.prototype.$save = function(success, error) {
          // Fortunately, LoopBack provides a convenient `upsert` method
          // that exactly fits our needs.
          var result = resource.upsert.call(this, {}, this, success, error);
          return result.$promise || result;
        };
        return resource;
      };
    }];
  });

})(window, window.angular);

'use strict';

angular.module('qeApp')
    .factory('notify', notify);

function notify() {
    var timeout = 2000;

    function error(msg, duration) {
        $.notify(msg, {
            status: 'danger',
            timeout: duration || timeout
        });
    }

    function success(msg, duration) {
        $.notify(msg, {
            status: 'success',
            timeout: duration || timeout
        });
    }

    function validationError() {
        error('Save failed. Please check your data');
    }

    return {
        error: error,
        validationError: validationError,
        success: success
    };
}

'use strict';

angular.module('qeApp')
    .factory('uploader', upload);

upload.$inject = ['FileUploader', '$http'];

function upload(FileUploader, $http) {
    var containerId = '';
    var filesUrl = '';
    var modelName = '';

    function create(id, model) {
        modelName = model;
        containerId = id;
        filesUrl = '/api/containers/' + containerId + '/files';
        return new FileUploader({
            url: '/api/containers/' + containerId + '/upload',
            formData: [
                {model: modelName}
            ]
        });
    }

    function remove(id) {
        return $http.delete(filesUrl + '/' + encodeURIComponent(id) + '?model=' + modelName);
    }

    function loadFilesList() {
        return $http.get(filesUrl);
    }

    function getContainer() {
        return $http.get('/api/containers/' + containerId);
    }

    function createContainer() {
        return $http.post('/api/containers', {
            name: containerId
        });
    }

    return {
        create: create,
        remove: remove,
        loadFilesList: loadFilesList,
        getContainer: getContainer,
        createContainer: createContainer
    }
}
