angular.module('qeApp')
  .controller('QEFolderController', folder);

folder.$inject = ['$scope', 'mails', '$stateParams', ];

function folder($scope, mails, $stateParams) {
  $scope.folder = $stateParams.folder;
  mails.all().then(function(mails) {
    $scope.mails = mails;
  });
}
