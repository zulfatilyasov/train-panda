// To run this code, edit file
// index.html or index.jade and change
// html data-ng-app attribute from
// angle to qeApp
// -----------------------------------

'use strict';
angular.module('qeApp', ['angle', 'lbServices', 'ngAnimate'])
    .run(runQuoteApp);

runQuoteApp.$inject = ['$log', '$rootScope', '$state', 'LoopBackAuth'];

function runQuoteApp($log, $rootScope, $state, LoopBackAuth) {
    var navigatingToLogin = false;
    console.log('quote engine app running');
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if (!LoopBackAuth.accessTokenId && !navigatingToLogin) {
            //console.log('navigating login');
            navigatingToLogin = true;
            event.preventDefault();
            $state.go('page.login');
        }
    });
}
