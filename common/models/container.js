"use strict";
module.exports = function (Container) {
    Container.afterRemote('**.removeFile', function (ctx, unused, next) {
        if (!ctx.req.query.model) {
            next();
            return;
        }

        var modelName = ctx.req.query.model;
        if (!modelName) {
            next();
            return;
        }

        var model = Container.app.models[modelName];
        var modelId = ctx.req.params.container;
        var fileName = ctx.req.params.file;

        if (!model || !modelId) {
            next();
            return;
        }

        var collection = model.getDataSource().connector.collection(modelName);
        if (!collection) {
            next();
            return;
        }

        var ObjectID = Container.app.datasources.mongodb.ObjectID;
        var id = ObjectID.createFromHexString(modelId);
        collection.update({_id: id}, {$pull: {'images': {'name': fileName}}}, function (err, result) {
            next();
        });
    });

    Container.afterRemote('**.upload', function (ctx, data, next) {
        var model = data.result.fields.model;
        var file = data.result.files.file;

        if (!model || !model.length || !file || !file.length) {
            next();
            return;
        }

        var modelName = model[0];
        var ModelId = file[0].container;
        var fileName = file[0].name;

        var Model = Container.app.models[modelName];

        if (!Model || !ModelId || !fileName) {
            next();
            return;
        }

        var collection = Model.getDataSource().connector.collection(modelName);
        if (!collection) {
            next();
            return;
        }

        var fileObject = {
            name: fileName,
            url: '/api/containers/' + ModelId + '/download/' + fileName
        };

        var ObjectID = Container.app.datasources.mongodb.ObjectID;
        var id = ObjectID.createFromHexString(ModelId);
        collection.update({_id: id}, {$addToSet: {'images': fileObject}}, function (err, result) {
            next();
        });
    });
};
