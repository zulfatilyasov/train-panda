'use strict';
angular.module('qeApp')
    .controller('CitiesController', cities);

cities.$inject = ['City', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function cities(City, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;

    $rootScope.loading = true;
    vm.cities = City.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.cities-datatable');
        });
    });

    csvParser.attachInputWatcher('.upload-select', City);

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    vm.delete = function (cityId) {
        City.deleteById({
            id: cityId
        }).$promise
            .then(function () {
                dataTables.removeRow(cityId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}
