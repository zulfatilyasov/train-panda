'use strict';
angular.module('qeApp')
    .controller('CategoriesFormController', category);

category.$inject = ['Category', 'notify', '$stateParams', '$location'];

function category(Category, notify, $stateParams, $location) {
    var vm = this;
    vm.category = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';

    if ($stateParams.id) {
        vm.category = Category.findById({
            id: $stateParams.id
        });
    }
    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success(' Category saved!');
        };
        if (!vm.category.id) {
            Category.create(vm.category, function () {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.category.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }

}
