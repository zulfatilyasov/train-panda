'use strict';
angular.module('qeApp')
    .controller('TripsController', trips);

trips.$inject = ['Trip', 'datatables', '$timeout', '$state', '$rootScope'];

function trips(Trip, dataTables, $timeout, $state, $rootScope) {
    var vm = this;

    var filter = {
        'filter[include]': ['PNR', 'customer']
    };

    $rootScope.loading = true;
    Trip.query(filter, function (trips) {
        vm.trips = trips;
        $timeout(function () {
            dataTables.initDataTable('.trips-datatable');
        })
    });


    vm.delete = function (tripId) {
        Trip.deleteById({
            id: tripId
        }).$promise
            .then(function () {
                dataTables.removeRow(tripId);
            });
    };

    vm.showAlarms = function (tripId) {
        $state.go('app.trip-alarms', {tripId: tripId});
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}
