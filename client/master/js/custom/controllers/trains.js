'use strict';
angular.module('qeApp')
    .controller('TrainsController', trains);

trains.$inject = ['Train', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function trains(Train, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;
    $rootScope.loading = true;
    vm.trains = Train.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.trains-datatable');
        });
    });

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    csvParser.attachInputWatcher('.upload-select', Train);

    vm.delete = function (trainId) {
        Train.deleteById({
            id: trainId
        }).$promise
            .then(function () {
                dataTables.removeRow(trainId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}
