'use strict';
angular.module('qeApp')
    .controller('TrainRatingsController', trainRatings);

trainRatings.$inject = ['TrainRating', 'datatables', '$timeout', '$state', '$rootScope'];

function trainRatings(TrainRating, dataTables, $timeout, $state, $rootScope) {
    var vm = this;

    var filter = {
        'filter[include]': ['PNR', 'train', 'trip']
    };

    $rootScope.loading = true;
    TrainRating.query(filter, function (trainRatings) {
        vm.trainRatings = trainRatings;
        $timeout(function () {
            dataTables.initDataTable('.trainRatings-datatable');
        })
    });


    vm.delete = function (trainRatingId) {
        TrainRating.deleteById({
            id: trainRatingId
        }).$promise
            .then(function () {
                dataTables.removeRow(trainRatingId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}
