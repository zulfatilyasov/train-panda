'use strict';
angular.module('qeApp')
    .controller('TripFormController', trip);

trip.$inject = ['Trip', 'PNR', 'Customer', 'notify', '$stateParams', '$location', '$state'];

function trip(Trip, PNR, Customer, notify, $stateParams, $location, $state) {
    var vm = this;
    vm.trip = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        var filter = {
            'filter[include]': ['PNR', 'customer'],
            'filter[where][id]': $stateParams.id
        };
        Trip.query(filter, function(trips) {
            vm.trip = trips[0];
        });
    }

    var pnrs = PNR.find({
        filter: {
            fields: {
                id: true,
                pnr: true
            }
        }
    }, function () {
        vm.pnrs = pnrs;
    });

    var customers = Customer.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.customers = customers;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('Trip saved!');
        };
        if (!vm.trip.id) {
            vm.trip = Trip.create(vm.trip, function () {
                saveSuccess(true);
                $state.go('app.trips-edit', {id: vm.trip.id})
            }, saveFailed);
        } else {
            vm.trip.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
