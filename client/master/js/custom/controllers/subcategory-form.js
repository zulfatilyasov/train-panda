'use strict';
angular.module('qeApp')
    .controller('SubCategoriesFormController', subCategory);

subCategory.$inject = ['SubCategory','Category' ,'notify', '$stateParams', '$location'];

function subCategory(SubCategory, Category, notify, $stateParams, $location) {
    var vm = this;
    vm.subCategory = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';

    if ($stateParams.id) {
        vm.subCategory = SubCategory.findById({
            id: $stateParams.id
        });
    }

    var categories = Category.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.categories = categories;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success(' SubCategory saved!');
        };
        if (!vm.subCategory.id) {
            SubCategory.create(vm.subCategory, function () {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.subCategory.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }

}
