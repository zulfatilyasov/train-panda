'use strict';

angular.module('qeApp')
    .factory('notify', notify);

function notify() {
    var timeout = 2000;

    function error(msg, duration) {
        $.notify(msg, {
            status: 'danger',
            timeout: duration || timeout
        });
    }

    function success(msg, duration) {
        $.notify(msg, {
            status: 'success',
            timeout: duration || timeout
        });
    }

    function validationError() {
        error('Save failed. Please check your data');
    }

    return {
        error: error,
        validationError: validationError,
        success: success
    };
}
