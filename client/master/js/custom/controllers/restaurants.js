'use strict';
angular.module('qeApp')
    .controller('RestaurantsController', restaurants);

restaurants.$inject = ['Restaurant', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function restaurants(Restaurant, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;

    $rootScope.loading = true;
    vm.restaurants = Restaurant.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.restaurants-datatable');
        })
    });

    csvParser.attachInputWatcher('.upload-select', Restaurant);

    vm.delete = function (restaurantId) {
        Restaurant.deleteById({
            id: restaurantId
        }).$promise
            .then(function () {
                dataTables.removeRow(restaurantId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}
