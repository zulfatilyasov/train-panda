'use strict';
angular.module('qeApp')
    .controller('OrdersFormController', order);

order.$inject = ['Order', 'Restaurant', 'Station', 'Customer', 'notify', '$stateParams', '$location', '$filter'];

function order(Order, Restaurant, Station, Customer, notify, $stateParams, $location, $filter) {
    var vm = this;
    vm.order = {};
    vm.action = 'upsert';

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        var filter = {
            'filter[include]': ['restaurant', 'customer'],
            'filter[where][id]': $stateParams.id
        };
        if ($stateParams.id) {
            Order.query(filter, function(trips) {
                vm.order = trips[0];
                changeDates();
            });
        }
    }

    function changeDates() {
        if (vm.order.date) {
            vm.order.date = formatDate(vm.order.date);
        }
    }

    function formatDate(date) {
        return $filter('date')(date, 'MM/dd/yyyy H:mm');
    }

    var restaurants = Restaurant.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.restaurants = restaurants;
    });

    var customers = Customer.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.customers = customers;
    });

    var stations = Station.find({
        filter: {
            fields: {
                id: true,
                code:true,
                name: true
            }
        }
    }, function () {
        vm.stations = stations;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('order saved!');
        };
        if (!vm.order.id) {
            Order.create(vm.order, function () {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.order.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
