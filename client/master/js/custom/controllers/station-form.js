'use strict';
angular.module('qeApp')
    .controller('StationFormController', station);

station.$inject = ['Station', 'City', 'State', 'StationCategory', 'notify', '$stateParams', '$location', 'uploader'];

function station(Station, City, State, StationCategory, notify, $stateParams, $location, uploader) {
    var vm = this;
    vm.station = {};
    vm.action = 'upsert';
    vm.halfLength = 0;

    if ($location.path().indexOf('view') >= 0)
        vm.action = 'view';

    if ($stateParams.id) {
        vm.station = Station.findById({
            id: $stateParams.id
        }, function () {
            if (vm.station.category)
                vm.categoryId = vm.station.category.id;
            if (vm.station.cityId)
                vm.station.city = City.findById({id: vm.station.cityId});
            if (vm.station.stateId)
                vm.station.state = State.findById({id: vm.station.stateId});
        });

        vm.containerId = $stateParams.id;
        vm.uploader = uploader.create($stateParams.id, 'Station');
        vm.uploader.onCompleteAll = function () {
            loadFiles();
        };

        uploader.getContainer()
            .success(function (container) {
                if (container) {
                    loadFiles();
                }
            })
            .error(function (data) {
                if (data.error.status === 404) {
                    uploader.createContainer();
                }
            });
    }

    function loadFiles() {
        uploader.loadFilesList()
            .success(function (data) {
                vm.files = data;
                vm.halfLength = Math.ceil(vm.files.length / 2);
            });
    }

    vm.deleteFile = function (index, id) {
        uploader.remove(id)
            .success(function () {
                vm.files.splice(index, 1);
            });
    };

    var categories = StationCategory.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.categories = categories;
    });

    var cities = City.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.cities = cities;
    });

    var states = State.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.states = states;
    });

    vm.stateChanged = function () {
        City.findByStateId({
            stateId: vm.station.stateId
        }, function (data) {
            debugger;
            vm.cities = data.cities;
        });
    };

    function beforeSave() {
        if (vm.categoryId) {
            var categories = vm.categories.filter(function (c) {
                return c.id === vm.categoryId;
            });
            if (categories.length)
                vm.station.category = categories[0];
        }
    }

    vm.save = function () {
        vm.saving = true;
        beforeSave();
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('Station saved!');
        };
        if (!vm.station.id) {
            Station.create(vm.station, function () {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.station.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
