'use strict';
angular.module('qeApp')
    .controller('OrdersController', orders);

orders.$inject = ['Order', 'datatables', '$timeout', '$state', 'csvParser', '$rootScope'];

function orders(Order, dataTables, $timeout, $state, csvParser, $rootScope) {
    var vm = this;

    var filter = {
        'filter[include]': ['customer', 'restaurant']
    };

    $rootScope.loading = true;
    Order.query(filter, function (orders) {
        vm.orders = orders;
        $timeout(function () {
            dataTables.initDataTable('.orders-datatable');
        })
    });

    csvParser.attachInputWatcher('.upload-select', Order);

    $rootScope.$on('uploadStart', function () {
        vm.uploading = true;
    });

    $rootScope.$on('uploadFinish', function () {
        vm.uploading = false;
    });

    vm.delete = function (orderId) {
        Order.deleteById({
            id: orderId
        }).$promise
            .then(function () {
                dataTables.removeRow(orderId);
            });
    };

    vm.showAlarms = function (orderId) {
        $state.go('app.order-alarms', {orderId: orderId});
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}
