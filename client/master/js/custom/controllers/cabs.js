'use strict';
angular.module('qeApp')
    .controller('CabsController', cabs);

cabs.$inject = ['Cab', 'datatables', '$timeout', 'csvParser', '$rootScope'];

function cabs(Cab, dataTables, $timeout, csvParser, $rootScope) {
    var vm = this;

    $rootScope.loading = true;
    vm.cabs = Cab.find(function () {
        $timeout(function () {
            dataTables.initDataTable('.cabs-datatable');
        })
    });

    csvParser.attachInputWatcher('.upload-select', Cab);

    vm.delete = function (cabId) {
        Cab.deleteById({
            id: cabId
        }).$promise
            .then(function () {
                dataTables.removeRow(cabId);
            });
    };

    vm.search = function () {
        dataTables.searchTable(vm.searchQuery);
    };
}
