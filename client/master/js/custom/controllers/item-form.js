'use strict';
angular.module('qeApp')
    .controller('ItemsFormController', item);

item.$inject = ['Item', 'Restaurant', 'Category', 'SubCategory', 'notify', '$stateParams', '$location'];

function item(Item, Restaurant, Category, SubCategory, notify, $stateParams, $location) {
    var vm = this;
    vm.item = {};
    vm.action = 'upsert';
    vm.restaurantId = null;

    if ($location.path().indexOf('view') >= 0) {
        vm.action = 'view';
    }

    if ($stateParams.id) {
        var filter = {
            'filter[include]': ['category', 'subcategory', 'restaurant'],
            'filter[where][id]': $stateParams.id
        };
        if ($stateParams.id) {
            Item.query(filter, function(trips) {
                vm.item = trips[0];
            });
        }
    }

    var restaurants = Restaurant.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.restaurants = restaurants;
    });

    var categories = Category.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.categories = categories;
    });

    var subCategories = SubCategory.find({
        filter: {
            fields: {
                id: true,
                name: true
            }
        }
    }, function () {
        vm.subCategories = subCategories;
    });

    vm.save = function () {
        vm.saving = true;
        var saveSuccess = function () {
            vm.saving = false;
            notify.success('item saved!');
        };
        if (!vm.item.id) {
            Item.create(vm.item, function () {
                saveSuccess();
            }, saveFailed);
        } else {
            vm.item.$save(function () {
                saveSuccess();
            }, saveFailed);
        }
    };

    function saveFailed(resp) {
        vm.saving = false;
        if (resp && resp.data && resp.data.error)
            console.log(resp.data.error);
        if (resp && resp.data && resp.data.error)
            notify.error(resp.data.error);
    }
}
