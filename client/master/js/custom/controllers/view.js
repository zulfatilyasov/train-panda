angular.module('qeApp')
  .controller('QEViewController', ['$scope', 'mails', '$stateParams', function($scope, mails, $stateParams) {
    mails.get($stateParams.mid).then(function(mail) {
      $scope.mail = mail;
    });
  }]);
